
#ifndef _PRINTER_H
#define _PRINTER_H

////////////////////
// Printer:
// this class prints out indentions and
// strings

using namespace std;

class Printer
{
    int indention;
    std::ostream* out;
public:
    //Printer()
    //{
    //  Printer(&cout);
    //}
    Printer(std::ostream* output) : indention(0), out(output) {}
    ~Printer() { out->flush(); }
    void Indent()
    {
        indention++;
    }
    void Unindent()
    {
        indention--;
    }
    void Print(const char* string)
    {
        for (int i = 0; i < indention; i++)
            (*out) << "  ";
        (*out) << string;
    }
    void PrintSpace() {
        for (int i = 0; i < indention; i++) (*out) << "  ";
    }
    void Print(const char* string, const char* param)
    {
        char final[256];
        snprintf(final, 256, string, param);
        Print(final);
    }
    void Print(const char* string, const int param)
    {
        char final[256];
        snprintf(final, 256, string, param);
        Print(final);
    }
    void Print(const char* string, const char* param, const char* param2)
    {
        char final[256];
        snprintf(final, 256, string, param, param2);
        Print(final);
    }
    void Print(const char* string, const int param, const int param2)
    {
        char final[256];
        snprintf(final, 256, string, param, param2);
        Print(final);
    }
    void Print(const char* string, const char* param, const int param2)
    {
        char final[256];
        snprintf(final, 256, string, param, param2);
        Print(final);
    }
    void Print(const char* string, const int param, const char* param2)
    {
        char final[256];
        snprintf(final, 256, string, param, param2);
        Print(final);
    }
};

#endif // _PRINTER_H

