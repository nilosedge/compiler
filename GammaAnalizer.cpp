// GammaAnalizer.cpp:  Final Semantic Anaisys Pass
// This Visitor preforms all the remaining semantic checks.

#include "GammaAnalizer.h"
#include "StateInfo.h"

// Will: All semantic checks at the program level already finished.
void GammaAnalizer::Visit(ProgramNode* Program)
{
	Program->classes->Accept(this);
}

// Will: Only visit the methods since all symantic errors with fields are 
// dealt with in Beta
void GammaAnalizer::Visit(ClassNode* Class)
{
	if(Class->body->methods)
		Class->body->methods->Accept(this);
}

// Will: Only visit the body since parameters are handled by Beta
void GammaAnalizer::Visit(MethodNode* Method)
{
	// No check here either since the parser should
	// have complained.
	Method->body->Accept(this);
}

// Stefan: Just sets the type for the method call
//	If the method is called on a foreign method call
//	then we need to get the type from innerAccess
void GammaAnalizer::Visit(MethodCallNode* MethodCall)
{
	MethodCall->ReturnType = VisitMethodCall(MethodCall);
}

// Stefan: Just sets the type for the method call
//	If the method is called on a foreign method call
//	then we need to get the type from innerAccess
void GammaAnalizer::Visit(MethodCallStatementNode* MethodCall)
{
	MethodCall->ReturnType = VisitMethodCall(MethodCall);
}

// Stefan: See Comments inside the method
void GammaAnalizer::Visit(VariableAccessFactorNode* VariableAccess)
{
	VariableAccess->variable->Accept(this);

	VariableInfo* variable = 0;
	
	// If our variable access is in the form of 'a.x' and x is not
	// an array then ReturnType will be the type of x.
	// Elsewise it will be the type of objects stored in array x.
	if (VariableAccess->variable->innerAccess)
	{
		VariableAccess->variable->ReturnType->Fields.Lookup(
			VariableAccess->variable->innerAccess->name->Value,
			&variable);

		if (variable)
		{	
			ForeignMemberArrayAccessNode* forMemArray
				= dynamic_cast<ForeignMemberArrayAccessNode*>
					(VariableAccess->variable->innerAccess);
			// forMemArray should be null if the dynamic_cast failed
			// this will happen when the innerAcces is not pointing to an Array
			if (forMemArray)
			{
				VariableAccess->ReturnType
					= State.ResolveArrayAccess(variable->Type, VariableAccess->RowNumber);
				return;
			}
		}
	}
	// If our variable access is in the form of 'a' and a is simply a 
	// variable then ReturnType will be the type of a
	// Elsewise it will be the type of objects stored in array a.
	else
	{
		if (VariableAccess->variable->name)
		{
			variable = State.ResolveVariable(VariableAccess->variable->name->Value);
		}
		else
		{
			VariableAccess->ReturnType = State.CurrentClass;
			return;
		}

		MemberArrayAccessNode* memArray
			= dynamic_cast<MemberArrayAccessNode*>(VariableAccess->variable);
		if (variable && memArray)
		{
			VariableAccess->ReturnType
				= State.ResolveArrayAccess(variable->Type, VariableAccess->RowNumber);
			return;
		}
	}

	// If we somehow didn't find the type print out a sane error
	if (!variable)
	{
		char message[180];
		if (VariableAccess->variable->innerAccess)
			snprintf(message, 180, "Undeclared field \'%s\' (Assuming type of \'int\')",
				VariableAccess->variable->innerAccess->name->Value);
		else
			snprintf(message, 180, "Undeclared variable \'%s\' (Assuming type of \'int\')",
				VariableAccess->variable->name->Value);
		State.Errors->PrintError(message, VariableAccess->RowNumber);
		VariableAccess->ReturnType = State.ResolveType("int");
	}
	else
		VariableAccess->ReturnType = variable->Type;
	
}

// Stefan: Here we just pass the type up
void GammaAnalizer::Visit(ArgumentNode* Argument)
{
	Argument->value->Accept(this);
	Argument->ReturnType = Argument->value->ReturnType;
}

// Stefan:  Just sets the type for the this node to that of the
//	current class
void GammaAnalizer::Visit(ThisAccessNode* This)
{
	This->ReturnType = State.CurrentClass;
}

// Stefan: This doesn't do anything because at this point
//	we don't know if it is an method or a field
void GammaAnalizer::Visit(ForeignMemberAccessNode* MemberAccess)
{
	//MemberAccess->ReturnType = State.WorkingClass;
}

// Stefan: if we don't have an innerAccess then we don't
//	know what we are other wise we know we are field or
//	a variable
void GammaAnalizer::Visit(MemberAccessNode* MemberAccess)
{
	if (!MemberAccess->innerAccess)
		return;

	MemberAccess->innerAccess->Accept(this);

	VariableInfo* variable = State.ResolveVariable(MemberAccess->name->Value);
	if (variable)
		MemberAccess->ReturnType = variable->Type;
	else
	{
        char message[180];
        snprintf(message, 180, "Undeclared variable \'%s\' (Assuming type \'int\')",
        	MemberAccess->name->Value);
		State.Errors->PrintError(message, MemberAccess->RowNumber);
		MemberAccess->ReturnType = State.ResolveType("int");
	}
}

// Stefan: We want to do the same as a regular foreignmemberaccessnode
//	but also check the index
void GammaAnalizer::Visit(ForeignMemberArrayAccessNode* MemberAccess)
{
	Visit((ForeignMemberAccessNode*)MemberAccess);
	
	MemberAccess->index->Accept(this);

	AssertType(MemberAccess->index, State.ResolveType("int"));
}

// Stefan: We want to do the same as a regular memberaccessnode
//	but also check the index
void GammaAnalizer::Visit(MemberArrayAccessNode* MemberAccess)
{
	Visit((MemberAccessNode*)MemberAccess);

	MemberAccess->index->Accept(this);

	AssertType(MemberAccess->index, State.ResolveType("int"));

	if (MemberAccess->ReturnType)
	{
		if (MemberAccess->ReturnType->SecondaryType)
		{
			MemberAccess->ReturnType = MemberAccess->ReturnType->SecondaryType;
		}
		else
		{
			char message[180];
			snprintf(message, 180, "Array access on non arrray \'%s\'",
				MemberAccess->name->Value);
			State.Errors->PrintError(message, MemberAccess->RowNumber);
		}
	}
}

//Will: Were only interested in the statement's of the body
void GammaAnalizer::Visit(MethodBodyNode* MethodBody)
{
	if(MethodBody->statements)
		MethodBody->statements->Accept(this);
}

// Stefan: See Comments inside the method
void GammaAnalizer::Visit(AssignmentStatementNode* Assignment)
{
	Assignment->member->Accept(this);
	Assignment->value->Accept(this);

	// Don't all assignments to this directly, only in the form of this.x 
	if (!Assignment->member->name && !Assignment->member->innerAccess)
	{
		State.Errors->PrintError("\'this\' is read-only", Assignment->RowNumber);
		return;
	}

	VariableInfo* variable = 0;
	
	// If our assignment is in the form of 'a.x' and x is not
	// an array then ReturnType will be the type of x.
	// Elsewise it will be the type of objects stored in array x.
	if (Assignment->member->innerAccess)
	{
		Assignment->member->ReturnType->Fields.Lookup(
			Assignment->member->innerAccess->name->Value,
			&variable);

		if (variable)
		{
			Assignment->ReturnType = variable->Type;

			// Don't allow assignment to a.x.length
			if (strcmp("length", Assignment->member->innerAccess->name->Value) == 0
				&& Assignment->member->ReturnType->SecondaryType)
			{
				State.Errors->PrintError("\'length\' is read-only", Assignment->RowNumber);
			}

			ForeignMemberArrayAccessNode* forMemArray
				= dynamic_cast<ForeignMemberArrayAccessNode*>
					(Assignment->member->innerAccess);
			if (forMemArray)
			{
				Assignment->ReturnType
					= State.ResolveArrayAccess(variable->Type, Assignment->RowNumber);
			}
		}
	}
	// If our assignment is in the form of 'a' and a is simply a 
	// variable then ReturnType will be the type of a
	// Elsewise it will be the type of objects stored in array a.
	else
	{
		variable = State.ResolveVariable(Assignment->member->name->Value);
		if (variable)
			Assignment->ReturnType = variable->Type;

		MemberArrayAccessNode* memArray
			= dynamic_cast<MemberArrayAccessNode*>(Assignment->member);
		if (variable && memArray)
		{
			Assignment->ReturnType
				= State.ResolveArrayAccess(variable->Type, Assignment->RowNumber);
		}
	}

	// If we somehow didn't find the type print out a sane error
	if (!variable)
	{
		char message[180];
		if (Assignment->member->innerAccess)
			snprintf(message, 180, "Undeclared field \'%s\' on type \'%s\' (Assuming type of \'int\')",
				Assignment->member->innerAccess->name->Value,
				Assignment->member->ReturnType->Name);
		else
			snprintf(message, 180, "Undeclared variable \'%s\' (Assuming type of \'int\')",
				Assignment->member->name->Value);
		State.Errors->PrintError(message, Assignment->RowNumber);
		Assignment->ReturnType = State.ResolveType("int");
	}

	if (!CompareTypes(Assignment->ReturnType, Assignment->value->ReturnType, true))
	{
		char message[180];
		snprintf(message, 180, "Type \'%s\' is incompatible with \'%s\'",
			Assignment->ReturnType->Name,
			Assignment->value->ReturnType->Name);
		State.Errors->PrintError(message, Assignment->RowNumber);
		return;
	}

}

// Stefan: Here we verify that the print statement
//	is indeed taking all ints
void GammaAnalizer::Visit(PrintStatementNode* Print)
{
	Print->arguments->Accept(this);

	Nodes::NodeCollection::iterator argument_it = Print->arguments->nodes.begin();
	TypeInfo* intType = State.ResolveType("int");

	int count = 1;
	while (argument_it != Print->arguments->nodes.end())
	{
		if (!CompareTypes(intType, (*argument_it)->ReturnType, false))
		{
			char message[180];
			snprintf(message, 180, "Parameters %d of method \'%s\', expected type \'%s\' found \'%s\'",
				count, "print",
				intType->Name,
				(*argument_it)->ReturnType->Name);
			State.Errors->PrintError(message, (*argument_it)->RowNumber);
		}
		count++;
		argument_it++;
	}
}

// Stefan: check children and assert that the condition
//	is of type int
void GammaAnalizer::Visit(IfStatementNode* If)
{
	If->booleanExpression->Accept(this);
	If->trueCase->Accept(this);
	if (If->falseCase)
		If->falseCase->Accept(this);

	AssertType(If->booleanExpression, State.ResolveType("int"));
}

// Stefan: check children and assert that the condition
//	is of type int
void GammaAnalizer::Visit(WhileStatementNode* While)
{
	While->value->Accept(this);
	While->loop->Accept(this);

	AssertType(While->value, State.ResolveType("int"));
}

// Will: Set the ReturnType of the returnStatement to be 
// the Returntyp of the expression (returnValue).
// Throw an error if it doesn't match the method's declaration
void GammaAnalizer::Visit(ReturnStatementNode* Return)
{
	if(Return->returnValue) {
		Return->returnValue->Accept(this);
		Return->ReturnType = Return->returnValue->ReturnType;
	} else {
		Return->ReturnType = State.ResolveType("void");
	}
	
	if(!CompareTypes(State.CurrentMethod->ReturnType, Return->ReturnType, true)) {
		char message[180];
		snprintf(message, 180, "Return type does not match method declaration,\n\t\texpected type \'%s\' found \'%s\'", 
			State.CurrentMethod->ReturnType->Name, Return->ReturnType->Name);
		State.Errors->PrintError(message, Return->RowNumber, true);
	}

}

// Stefan: we don't need to do anything here :)
void GammaAnalizer::Visit(NoOpStatementNode* NoOp)
{
	
}

// Will: Use the Generic Magic to do all the work
void GammaAnalizer::Visit(IsEqualNode* IsEqual)
{
	VisitBinaryRelationalExpressionNode(IsEqual, true);
}

// Will: Use the Generic Magic to do all the work
void GammaAnalizer::Visit(IsNotEqualNode* IsNotEqual)
{
	VisitBinaryRelationalExpressionNode(IsNotEqual, true);
}

// Will: Use the Generic Magic to do all the work
void GammaAnalizer::Visit(IsLessThanNode* IsLessThan)
{
	VisitBinaryRelationalExpressionNode(IsLessThan);
}

// Will: Use the Generic Magic to do all the work
void GammaAnalizer::Visit(IsGreaterThanNode* IsGreaterThan)
{
	VisitBinaryRelationalExpressionNode(IsGreaterThan);
}

// Will: Use the Generic Magic to do all the work
void GammaAnalizer::Visit(IsLessThanOrEqualNode* IsLessThanOrEqual)
{
	VisitBinaryRelationalExpressionNode(IsLessThanOrEqual);
}

// Will: Use the Generic Magic to do all the work
void GammaAnalizer::Visit(IsGreaterThanOrEqualNode* IsGreaterThanOrEqual)
{
	VisitBinaryRelationalExpressionNode(IsGreaterThanOrEqual);
}

// Will: Get the type fromt he object were creating
// if were trying to construct something that doesn't exist 
// simply put void in there for the interim.
void GammaAnalizer::Visit(ConstructorCallNode* ConstructorCall)
{
	TypeInfo* new_type = State.ResolveType(ConstructorCall->type);
	if(new_type) {
		ConstructorCall->ReturnType = new_type;
	} else {
		ConstructorCall->ReturnType = State.ResolveType("void");
	}
}

// Will: Make sure that our array construction length is an int value.
void GammaAnalizer::Visit(ArrayConstructorCallNode* ArrayConstructorCall)
{
	ArrayConstructorCall->type->Accept(this);
	ArrayConstructorCall->count->Accept(this);

	ArrayConstructorCall->ReturnType = State.ResolveArrayType(ArrayConstructorCall->type);

	TypeInfo* exp_info = ArrayConstructorCall->count->ReturnType;
	TypeInfo* int_info = State.ResolveType("int");

	if(exp_info != int_info) {
		char message[180];
		snprintf(message, 180, "Invalid array size, found \'%s\' required \'int\'.", 
			ArrayConstructorCall->count->ReturnType->Name);
		State.Errors->PrintError(message, ArrayConstructorCall->count->RowNumber, true);
	}
}

// Will: Use the Generic Magic to do all the work
void GammaAnalizer::Visit(AdditionNode* Addition)
{
	VisitBinarySubExpressionNode(Addition);
}

// Will: Use the Generic Magic to do all the work
void GammaAnalizer::Visit(SubtractionNode* Subtraction)
{
	VisitBinarySubExpressionNode(Subtraction);
}

// Will: Use the Generic Magic to do all the work
void GammaAnalizer::Visit(LogicalOrNode* LogicalOr)
{
	VisitBinarySubExpressionNode(LogicalOr);
}

// Will: Use the Generic Magic to do all the work
void GammaAnalizer::Visit(MultiplyNode* Multiply)
{
	VisitBinaryTermNode(Multiply);
}

// Will: Use the Generic Magic to do all the work
void GammaAnalizer::Visit(DivideNode* Divide)
{
	VisitBinaryTermNode(Divide);
}

// Will: Use the Generic Magic to do all the work
void GammaAnalizer::Visit(ModulusNode* Modulus)
{
	VisitBinaryTermNode(Modulus);
}

// Will: Use the Generic Magic to do all the work
void GammaAnalizer::Visit(LogicalAndNode* LogicalAnd)
{
	VisitBinaryTermNode(LogicalAnd);
}

// Will: Simply return int type since that's it will evaluate to.
void GammaAnalizer::Visit(ReadNode* Read)
{
	Read->ReturnType = State.ResolveType("int");
}

// Will: Null == Null
void GammaAnalizer::Visit(NullNode* Null)
{
	Null->ReturnType = State.ResolveType("null");
}

//Will: A number is an int...
void GammaAnalizer::Visit(NumberNode* Number)
{
	Number->ReturnType = State.ResolveType("int");
}

// Stefan: we check the child types, then we make sure we 
//	are using ints
void GammaAnalizer::Visit(UnaryMinusNode* UnaryMinus)
{
	UnaryMinus->value->Accept(this);	
	UnaryMinus->value->Accept(this);	
	UnaryMinus->ReturnType = State.ResolveType("int");

	if (UnaryMinus->ReturnType != UnaryMinus->value->ReturnType)
	{
		char message[180];
		snprintf(message, 180, "Unary minus operation only valid on type \'int\', found \'%s\' instead",
			 UnaryMinus->value->ReturnType->Name);
		State.Errors->PrintError(message, UnaryMinus->RowNumber);
	}
}

// Stefan: we check the child types, then we make sure we 
//	are using ints
void GammaAnalizer::Visit(LogicalNotNode* LogicalNot)
{
	LogicalNot->value->Accept(this);	
	LogicalNot->ReturnType = State.ResolveType("int");

	if (LogicalNot->ReturnType != LogicalNot->value->ReturnType)
	{
		char message[180];
		snprintf(message, 180, "Logical Not operation only valid on type \'int\', found \'%s\' instead",
			 LogicalNot->value->ReturnType->Name);
		State.Errors->PrintError(message, LogicalNot->RowNumber);
	}
}

// Stefan: I am nost sure it this gets called
//	but if it does we just need to resolve the type
//	and pass it up to the paretns
void GammaAnalizer::Visit(TypeNode* Type)
{
	Type->ReturnType = State.ResolveType(Type);
}

// Stefan: I am nost sure it this gets called
//	but if it does we just need to resolve the type
//	and pass it up to the paretns
void GammaAnalizer::Visit(ArrayTypeNode* Array)
{
	Array->innerType->Accept(this);
	Array->ReturnType = State.ResolveType(Array);
}

// Will: Fill the ReturnType Pointers of each Field
void GammaAnalizer::Visit(FieldNode* Field)
{
	TypeInfo* typ_ptr;
	State.Program->Classes.Lookup(Field->type->typeName->Value, &typ_ptr);
	Field->ReturnType = typ_ptr;
}

// Will: Fill the ReturnType Pointers of each Parameter
void GammaAnalizer::Visit(ParameterNode* Parameter)
{
	TypeInfo* typ_ptr;
	State.Program->Classes.Lookup(Parameter->type->typeName->Value, &typ_ptr);
	Parameter->ReturnType = typ_ptr;
}

// Will: Fill the ReturnType Pointers of each Variable
void GammaAnalizer::Visit(VariableNode* Variable)
{
	TypeInfo* typ_ptr;
	State.Program->Classes.Lookup(Variable->type->typeName->Value, &typ_ptr);
	Variable->ReturnType = typ_ptr;
}

/*************************************************************
 *                      Helper Functions                     *
 *************************************************************/

// Stefan: The AssertType method verifies that a node results in the requested type
//  There are two version of this method, one takes the name of a type
//  And th other takes a pointer to a typeinfo object
void GammaAnalizer::AssertType(TreeNode* node, const char* typeName)
{
	AssertType(node, State.ResolveType(typeName));
}

// Stefan: The AssertType method verifies that a node results in the requested type
//  There are two version of this method, one takes the name of a type
//  And th other takes a pointer to a typeinfo object
void GammaAnalizer::AssertType(TreeNode* node, TypeInfo* type)
{
	if (node->ReturnType != type)
	{
		char message[180];
		snprintf(message, 180, "Type \'%s\' is incompatible with type \'%s\'",
			node->ReturnType->Name, type->Name);
		State.Errors->PrintError(message, node->RowNumber);
	}
}

// Stefan: Nice little function to check the equality of nodes in binary
// expressions and the like.
bool GammaAnalizer::CompareTypes(TypeInfo* left, TypeInfo* right, bool rightCanBeNull)
{
	if (!left || !right)
		return true;
		
	if (left == right)
		return true;

	if (strcmp(left->Name, right->Name) == 0)
		return true;
	
	if (left->IsReferenceType && rightCanBeNull)
	{
		TypeInfo* null = State.ResolveType("null");

		return right == null;
	}

	return false;
}

// Will: Helper Function to validate all decendants of BinaryTermNode
// (MultiplyNode DivideNode ModulusNode LogicalAndNode)
// Basically makes sure that both have a ReturnType of 'int'
void GammaAnalizer::VisitBinaryTermNode(BinaryTermNode* BinaryTerm) {
	BinaryTerm->left->Accept(this);
	BinaryTerm->right->Accept(this);

	TypeInfo* int_type = State.ResolveType("int");

	if (BinaryTerm->left->ReturnType 
		&& BinaryTerm->left->ReturnType != int_type) {
		char message[180];
		snprintf(message, 180, "Type \'%s\' is incompatible with \'%s\' operator",
			BinaryTerm->left->ReturnType->Name, BinaryTerm->nodeName);
		State.Errors->PrintError(message, BinaryTerm->RowNumber);
	} 

	if (BinaryTerm->right->ReturnType
		&& BinaryTerm->right->ReturnType != int_type) {
		char message[180];
		snprintf(message, 180, "Type \'%s\' is incompatible with \'%s\' operator",
			BinaryTerm->right->ReturnType->Name, BinaryTerm->nodeName);
		State.Errors->PrintError(message, BinaryTerm->RowNumber);
	}

	BinaryTerm->ReturnType = int_type;
}

// Will: Helper fuction to validate all decendants of BinarySubExpressionNode
// (AdditionNode SubtractionNode LogicalOrNode)
// Basically makes sure that both have a ReturnType of 'int'
void GammaAnalizer::VisitBinarySubExpressionNode(BinarySubExpressionNode* BinarySubExpression) {
	BinarySubExpression->left->Accept(this);
	BinarySubExpression->right->Accept(this);

	TypeInfo* int_type = State.ResolveType("int");

	if (BinarySubExpression->left->ReturnType
		&& BinarySubExpression->left->ReturnType != int_type) {
		char message[180];
		snprintf(message, 180, "Type \'%s\' is incompatible with \'%s\' operator",
			BinarySubExpression->left->ReturnType->Name, BinarySubExpression->nodeName);
		State.Errors->PrintError(message, BinarySubExpression->RowNumber);
	} 

	if (BinarySubExpression->right->ReturnType
		&& BinarySubExpression->right->ReturnType != int_type) {
		char message[180];
		snprintf(message, 180, "Type \'%s\' is incompatible with \'%s\' operator",
			BinarySubExpression->right->ReturnType->Name, BinarySubExpression->nodeName);
		State.Errors->PrintError(message, BinarySubExpression->RowNumber);
	}

	BinarySubExpression->ReturnType = int_type;
}

// Will: Helper fuction to validate all decendants of BinaryRelationalExpressionNode
// (IsEqualNode IsNotEqualNode IsLessThanNode IsGreaterThanNode IsLessThanOrEqualNode IsGreaterThanOrEqualNode)
// Basically makes sure that both have a ReturnType of 'int'
void GammaAnalizer::VisitBinaryRelationalExpressionNode(BinaryRelationalExpressionNode* BinaryRelationalExpression, bool allowNull) {
	BinaryRelationalExpression->left->Accept(this);
	BinaryRelationalExpression->right->Accept(this);

	TypeInfo* int_type = State.ResolveType("int");

	if (allowNull)
	{
		if (!CompareTypes(BinaryRelationalExpression->left->ReturnType, BinaryRelationalExpression->right->ReturnType, true))
		{
			char message[180];
			snprintf(message, 180, "Type \'%s\' is incompatible with \'%s\'",
				BinaryRelationalExpression->left->ReturnType->Name,
				BinaryRelationalExpression->right->ReturnType->Name);
			State.Errors->PrintError(message, BinaryRelationalExpression->RowNumber);
		}
	}
	else
	{
		if (BinaryRelationalExpression->left->ReturnType
			&& BinaryRelationalExpression->left->ReturnType != int_type) {
			char message[180];
			snprintf(message, 180, "Type \'%s\' is incompatible with \'%s\' operator",
				BinaryRelationalExpression->left->ReturnType->Name, BinaryRelationalExpression->nodeName);
			State.Errors->PrintError(message, BinaryRelationalExpression->RowNumber);
		} 

		if (BinaryRelationalExpression->right->ReturnType
			&& BinaryRelationalExpression->right->ReturnType != int_type) {
			char message[180];
			snprintf(message, 180, "Type \'%s\' is incompatible with \'%s\' operator",
				BinaryRelationalExpression->right->ReturnType->Name, BinaryRelationalExpression->nodeName);
			State.Errors->PrintError(message, BinaryRelationalExpression->RowNumber);
		}
	}

	BinaryRelationalExpression->ReturnType = int_type;
}

// Stefan: This function gets the type of a memberaccessNode
//	not the type but the actual result tuype`
TypeInfo* GammaAnalizer::GetMemberAccessType(MemberAccessNode* member)
{
	if (member->innerAccess)
	{
		return member->innerAccess->ReturnType;
	}
	else
	{
		return member->ReturnType;
	}
	
}

// Stefan: Just sets the type for the method call
//	If the method is called on a foreign method call
//	then we need to get the type from innerAccess
TypeInfo*  GammaAnalizer::VisitMethodCall(MethodCall* methodCall)
{
	methodCall->member->Accept(this);
	methodCall->arguments->Accept(this);

	MethodInfo* method = 0;
	TypeInfo* type = 0;
	const char* methodName;
	if (methodCall->member->innerAccess)
	{
		type = methodCall->member->ReturnType;
		methodName = methodCall->member->innerAccess->name->Value;
	}
	else
	{
		type = State.CurrentClass;
		if (methodCall->member->name)
			methodName = methodCall->member->name->Value;
		else
		{
			char message[180];
			snprintf(message, 180, "\'this\' is not a method");
			State.Errors->PrintError(message, methodCall->member->RowNumber);
			return State.ResolveType("int");
		}
	}

	type->Methods.Lookup(methodName, &method);
	
	if (!method)
	{
		char message[180];
		snprintf(message, 180, "Undeclared method \'%s\' (Assuming Return type of \'int\')", methodName);
		State.Errors->PrintError(message, methodCall->member->RowNumber);
		return State.ResolveType("int");
	}

	// Get iterators for the parameters and argument collecitons
	VariableVector::iterator parameter_it = method->ParameterList.begin();
	Nodes::NodeCollection::iterator argument_it = methodCall->arguments->nodes.begin();

	// loop through the parameters and arguments and verify
	//	they are typed correctly
	int count = 1;
	while (parameter_it != method->ParameterList.end()
		&& argument_it != methodCall->arguments->nodes.end())
	{
		if (!CompareTypes((*parameter_it)->Type, (*argument_it)->ReturnType, true))
		{
			char message[180];
			snprintf(message, 180, "Parameter %d of method \'%s\', expected type \'%s\' found \'%s\'",
				count, method->Name,
				(*parameter_it)->Type->Name,
				(*argument_it)->ReturnType->Name);
			State.Errors->PrintError(message, (*argument_it)->RowNumber);
		}
		count++;
		parameter_it++;
		argument_it++;
	}

	// Check to make sure the number of parameters matches
	if (method->ParameterList.size()
		!= methodCall->arguments->nodes.size())
	{
		char countMessage[180];
		snprintf(countMessage, 180, "Method \'%s\' takes %d parameters, found %d instead",
			method->Name, method->ParameterList.size(),
			methodCall->arguments->nodes.size());
		State.Errors->PrintError(countMessage, methodCall->member->RowNumber);
	}
	
	return method->ReturnType;
}

