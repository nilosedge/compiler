

#include "ErrorManager.h"

void ErrorManager::TooManyErrors()
{
	(*output) << "--------------------------------" << std::endl
		<< " Too Many Errors" << std::endl;
}

bool ErrorManager::IncrementErrorCount()
{
	if (maxErrors == ++errors)
		TooManyErrors();
	return errors < maxErrors;
}

int ErrorManager::GetErrorCount()
{
	return errors;
}

void ErrorManager::PrintError(const char* message)
{
	if (IncrementErrorCount())
		(*output) << " Error : " << message << std::endl;
}

void ErrorManager::PrintError(const char* message, int line, bool printLines)
{
	if (line < 1)
	{
		PrintError(message);
		return;
	}
	
	if (IncrementErrorCount())
	{
		(*output) << " Error on " << line << " : " << message << std::endl;
		//printLines = false;
		if (printLines)
			(*output) << " " << (line-1) << " " << buffer->GetLine(line-2)
				  	<< " " << (line)   << " " << buffer->GetLine(line-1)
				  	<< " " << (line+1) << " " << buffer->GetLine(line);
	}
}

