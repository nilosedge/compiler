#include "CodeGen.h"
#include "Printer.h"


////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
// The following functions work with our visitor////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

/* Function that we do not need to implement */
void CodeGen::Visit(NoOpStatementNode* NoOp) { }
void CodeGen::Visit(FieldNode* Field) { }
void CodeGen::Visit(TypeNode* Type) { }
void CodeGen::Visit(ArrayTypeNode* Array) { }


// This function is used to increment the global
// counter so that we do not have the same loop
// names
void CodeGen::IncOffSet() {OffSet += 4;}

//Generates a unique number for use with MIPS labels
int CodeGen::GetLabelCount() { return LabelCounter++; }
void CodeGen::EmitProcCall(char* procname) { printer.Print("jal %s\t # Jump to %s\n", procname, procname); }

/* Runs this function when a parameter is evaluated in the program
	setting the ofsets in the symbol table
*/
void CodeGen::Visit(ParameterNode* Parameter) {
	State.WorkingVariable->Offset = OffSet;
	OffSet += 4;
}

/* Runs this function when a variable evaluation is encountered in the program  */
void CodeGen::Visit(VariableNode* Variable) {
	Variable->Offset = OffSet;
	OffSet += 4;
}



/* Top of the tree that prints the header anc calls the global main*/
void CodeGen::Visit(ProgramNode* Program) {

	printer.Print("#;<program>\n");
	EmitHeaderCode();
	printer.Indent();
	// Creates the first instance of the class and 
	// grabs some space from the heap.
	printer.Print("li $t0 %i\t # Puts the amount of space we need into $t0\n", (State.Program->MainClass->Size/4));
	EmitPush("$t0");
	EmitAloc();

	MethodInfo* mainMethod = NULL;
	State.Program->MainClass->Methods.Lookup("main", &mainMethod);
	
	EmitProcCall(mainMethod->UniqueName);
	printer.Print("la $v0 10\n"); // This is exit code
	printer.Print("syscall\n");

	printer.Indent();
	if(Program->classes) Program->classes->Accept(this);

	printer.Unindent();
	printer.Print("#;</program>\n");	
	EmitPrintCode();

	EmitExceptionCode();
}


/* Runs when it encounters classes*/
void CodeGen::Visit(ClassNode* Class)
{
	printer.Print("#;<class name=\"%s\">\n", Class->name->Value);
	printer.Indent();
	OffSet = 0;
	if (Class->body->fields) Class->body->fields->Accept(this);      /* Here it branches to fields when found*/
	if (Class->body->methods) Class->body->methods->Accept(this);	/* Here it branches to methods when found */    
	printer.Unindent();
	printer.Print("#;</class>\n");
}




/* Runs this function when it encounters a method declaration in the program */
// This is the 
void CodeGen::Visit(MethodNode* Method)
{
	//int LocalOffSet = OffSet;
	printer.Print("#;<method name=\"%s\">\n", Method->name->Value);
	printer.Indent();
	printer.Print("%s:\n", State.CurrentMethod->UniqueName);
	
	if(Method->parameters && Method->body) {
		int numParms, numLocals;
		numParms = Method->parameters->GetLength(); 
		State.CurrentMethod->numParams = numParms;
		if(Method->body->variables) numLocals = Method->body->variables->GetLength();
		else numLocals = 0;
		printer.Print("move $s1, $fp\t\t#Save fp into t1\n");
		printer.Print("addi $fp, $sp,%i\t\t#Position fp at the top of the first argument being passed in\n", (numParms + 1)*4);
		//printer.Print("addi $sp, $sp,%i \t\t#Position sp at the end of the locals\n", (numLocals)*-4);
		for(int i = 0; i < numLocals; i++) {
			EmitPush("$zero");
		}

		// Very important that the Offset is set to zero
		// before visiting the parameters.
		OffSet = 0;
		Method->parameters->Accept(this);

		// This next line is A MUST do not remove
		OffSet += 4; // Inc's the offset past the "THIS" reference
		Method->body->Accept(this);
	}

	printer.Print("%s_end:\n", State.CurrentMethod->UniqueName);

	// This next section of code is for the stack frame clean up
	// does the nessesary things to pop the stack frame.
	EmitPop("$ra");
	EmitPop("$t0");
	printer.Print("move $sp $fp\n");
	printer.Print("move $fp $t0\n");
	printer.Print("jr $ra\n");
	printer.Unindent();
	printer.Print("#;</method>\n");
}


/* This function evaluates what is inside a certain method
// for example
	void Foo(){
			This is the body of a method that would actually be evaluated
	}
*/
void CodeGen::Visit(MethodBodyNode* MethodBody)
{
	printer.Print("#;<MethodBody>\n");
	printer.Indent();
	if(MethodBody->variables) MethodBody->variables->Accept(this);	/* Here it braches to variable when found*/
	EmitPush("$s1"); // Stores the FP onto the stack
	EmitPush("$ra"); // Stores the RA onto the stack
	if(MethodBody->statements) MethodBody->statements->Accept(this);	/* Here it branches to statements when found in the program */
	printer.Unindent();	
	printer.Print("#;</MethodBody>\n");
}



/* This is our assignment statement function 
// For example:
// 						int <x> = <0>;
// Here, we would give <x> to <t1> and <0> to <t0>. We then assign the value of t0 to t1
//
*/
void CodeGen::Visit(AssignmentStatementNode* Assignment)
{
	printer.Print("#;<AssignmentStatementNode>\n");	
	printer.Indent();
	if(Assignment->member) Assignment->member->Accept(this);	/* H*/
	if(Assignment->value) Assignment->value->Accept(this);
	EmitPop("$t1"); 
	EmitPop("$t0");
	printer.Print("beq $t0 $zero Null_Pointer_Exception\t # The is the null pointer exception\n");
	printer.Print("sw $t1 0($t0)\t # Store the value of $t1 at address location $t0\n");
	printer.Unindent();	
	printer.Print("#;</AssignmentStatementNode>\n");
}


/* This is our printing function for the statements, taking in count the number of arguments passed to it*/
void CodeGen::Visit(PrintStatementNode* Print) {
	printer.Print("#;<PrintStatementNode>\n");
	printer.Indent();
	if(Print->arguments) {
		Print->arguments->Accept(this);
		if(Print->arguments->GetLength() > 0) {	// Enters this procedure length of arguments passed is greater than zero
			printer.Print("li $t0 %i\t # Loads the number of args into $t0.\n", Print->arguments->GetLength());
			EmitPush("$t0");
			EmitProcCall("print");
		} else {	// If length of arguments is less than zero it only prints a new line character
			printer.Print("la $a0 N\t# Loads the address of the newline char.\n");
			printer.Print("li $v0 4\t# Tells syscall that it wants to print an int.\n");
			printer.Print("syscall\t# Do the system call.\n");
		}
	}
	
	printer.Unindent();	
	printer.Print("#;</PrintStatementNode>\n");
}

/* This function is visited when an If statement is encountered in our program */
void CodeGen::Visit(IfStatementNode* If)
{
	int labelNumber;
	labelNumber=GetLabelCount();
	printer.Print("#;<IfStatementNode>\n");
	printer.Indent();
	if(If->booleanExpression) If->booleanExpression->Accept(this);
	EmitPop("$t0");
	printer.Print("beq $t0 $zero else%i\t#if $t0 is false jump to else%i\n",labelNumber, labelNumber);
	if(If->trueCase) If->trueCase->Accept(this);
	printer.Print("j endelse%i\t\t#Skip to the end of the false statement\n",labelNumber);
	printer.Print("else%i:\n",labelNumber);	
	if(If->falseCase) If->falseCase->Accept(this);
	printer.Print("endelse%i:\t\t#End of If-Then-Else\n",labelNumber);
	printer.Unindent();	
	printer.Print("#;</IfStatementNode>\n");
}


/* This function is executed if a While statement is found in the program */
void CodeGen::Visit(WhileStatementNode* While) {
	int labelNum;
	labelNum=GetLabelCount();
	printer.Print("#;<WhileStatementNode>\n");
	printer.Indent();
	printer.Print("while%i:\n",labelNum);
	if(While->value) While->value->Accept(this);
	EmitPop("$t0");
	printer.Print("beq $t0 $zero endwhile%i\t#If t0 is equal to 0, then jump to endwhile%i\n",labelNum, labelNum);
	if(While->loop) While->loop->Accept(this);
	printer.Print("j while%i\n",labelNum);
	printer.Print("endwhile%i:\n",labelNum);
	printer.Unindent();	
	printer.Print("#;</WhileStatementNode>\n");
}

/* This funciton is executed when a Return Statement is found in our program declaration is a specific function*/
void CodeGen::Visit(ReturnStatementNode* Return) {
	printer.Print("#;<ReturnStatementNode>\n");	
	printer.Indent();
	if(Return->returnValue) {
		Return->returnValue->Accept(this);
		EmitPop("$v0");
	}
	printer.Print("j %s_end\n", State.CurrentMethod->UniqueName);
	printer.Unindent();	
	printer.Print("#;</ReturnStatementNode>\n");
}

/* This function is executes when a method call statement is also found in our program */
void CodeGen::Visit(MethodCallStatementNode* MethodCall) {
	printer.Print("#;<MethodCallSatementNode>\n");	
	printer.Indent();
	if(MethodCall->arguments) { MethodCall->arguments->Accept(this); }
	if(MethodCall->member) { MethodCall->member->Accept(this); }
	if(MethodCall->member->name) {
		MethodInfo* method = 0;
		if (State.CurrentClass->Methods.Lookup(MethodCall->member->name->Value, &method)) {
			EmitProcCall(method->UniqueName);
		} 
	}
	printer.Unindent();	
	printer.Print("#;</MethodCallStatementNode>\n");
}

void CodeGen::Visit(MethodCallNode* MethodCall) {
	printer.Print("#;<MethodCallNode>\n");	
	printer.Indent();
	if(MethodCall->arguments) { MethodCall->arguments->Accept(this); }
	if(MethodCall->member) { MethodCall->member->Accept(this); }

	if(MethodCall->member->name) {
		MethodInfo* method = 0;
		if (State.CurrentClass->Methods.Lookup(MethodCall->member->name->Value, &method)) {
			EmitProcCall(method->UniqueName);
		}
	}
	// This is the return statement this is what returns the value back
	// onto the stack.
	EmitPush("$v0");
	printer.Unindent();	
	printer.Print("#;</MethodCallNode>\n");
}


/* This function is run when ever an IsEqual (==) relation operator is found in our program*/
void CodeGen::Visit(IsEqualNode* IsEqual)
{
	printer.Print("#;<IsEqualNode>\n");	
	printer.Indent();
	EmitBinRelExp("seq $t0 $t0 $t1","Set $t0 to 1 if $t0 is equal to $t1",IsEqual);
	printer.Unindent();	
	printer.Print("#;</IsEqualNode>\n");
}

/* This function is executed when a not equal (!=) relation operator is found*/
void CodeGen::Visit(IsNotEqualNode* IsNotEqual)
{
	printer.Print("#;<IsNotEqualNode>\n");	
	printer.Indent();
	EmitBinRelExp("sne $t0 $t0 $t1","Set $t0 to 1 if $t0 is not equal to $t1",IsNotEqual);
	printer.Unindent();	
	printer.Print("#;</IsNotEqualNode>\n");
}

/* This function is executed when a less than (<) relation operator is found  */
void CodeGen::Visit(IsLessThanNode* IsLessThan)
{
	printer.Print("#;<IsLessThanNode>\n");
	printer.Indent();
	EmitBinRelExp("slt $t0 $t0 $t1","Set $t0 to 1 if $t0 is less than $t1",IsLessThan);
	printer.Unindent();	
	printer.Print("#;</IsLessThanNode>\n");
}

/* This function is executed whenever a greater (>) relation operator is found */
void CodeGen::Visit(IsGreaterThanNode* IsGreaterThan)
{
	printer.Print("#;<IsGreaterThanNode>\n");	
	printer.Indent();
	EmitBinRelExp("sgt $t0 $t0 $t1","Set $t0 to 1 if $t0 is greater than $t1",IsGreaterThan);
	printer.Unindent();	
	printer.Print("#;</IsGreaterThanNode>\n");
}

/* This funciton is executed whenever a less than or equal (<=) relation operator is found */
void CodeGen::Visit(IsLessThanOrEqualNode* IsLessThanOrEqual)
{
	printer.Print("#;<IsLessThanOrEqualNode>\n");
	printer.Indent();
	EmitBinRelExp("sle $t0 $t0 $t1","Set $t0 to 1 if $t0 is less than or equal to $t1",IsLessThanOrEqual);
	printer.Unindent();	
	printer.Print("#;</IsLessThanOrEqualNode>\n");
}

/* This function is executed whenever a greater than or equal (>=) relation operator is found */
void CodeGen::Visit(IsGreaterThanOrEqualNode* IsGreaterThanOrEqual)
{
	printer.Print("#;<IsGreaterThanOrEqualNode>\n");
	printer.Indent();
	EmitBinRelExp("sge $t0 $t0 $t1","Set $t0 to 1 if $t0 is greater than or equal to $t1",IsGreaterThanOrEqual);
	printer.Unindent();	
	printer.Print("#;</IsGreaterThanOrEqualNode>\n");
}

/* This function allocates an instance for a new <class> ( e.g. x = new <Foo>; )*/
void CodeGen::Visit(ConstructorCallNode* ConstructorCall)
{
	printer.Print("#;<ConstructorCallNode>\n");
	printer.Indent();
	TypeInfo *typeinfo = State.ResolveType(ConstructorCall->type);
	if(typeinfo) { 
		printer.Print("li $t0 %i\t # Puts the amount of space we need into $t0\n", (typeinfo->Size/4));
		EmitPush("$t0");
		EmitAloc(); 
	}
	printer.Unindent();	
	printer.Print("#;</ConstructorCallNode>\n");
}

/* */
void CodeGen::Visit(ArrayConstructorCallNode* ArrayConsturctorCall)
{
	printer.Print("#;<ArrayConstructorCallNode>\n");
	printer.Indent();
	if(ArrayConsturctorCall->count) ArrayConsturctorCall->count->Accept(this);
	EmitAloc(true);
	printer.Unindent();
	printer.Print("#;</ArrayConstructorCallNode>\n");	
}

/* This function is called for the addition of two numbers, it adds t1 and t0, and stores the result into t0*/
void CodeGen::Visit(AdditionNode* Addition)
{
	printer.Print("#;<AdditionNode>\n");	
	printer.Indent();
	EmitBinarySubExp("add $t0 $t0 $t1", "Add two integers and store in t0\n", Addition);
	printer.Unindent();	
	printer.Print("#;</AdditionNode>\n");
}


/* This function is called for the subtraction of two integer numbers, it puts the difference of the integers from t0 and t1 into t0 */
void CodeGen::Visit(SubtractionNode* Subtraction)
{
	printer.Print("#;<SubtractionNode>\n");
	printer.Indent();
	EmitBinarySubExp("sub $t0 $t0 $t1", "Subtract two integers and store in t0\n",Subtraction);
	printer.Unindent();	
	printer.Print("#;</SubtractionNode>\n");
}

/* This function is called to put the logical OR of the integers from t0 and t1 to t0 */
void CodeGen::Visit(LogicalOrNode* LogicalOr)
{
	printer.Print("#;<LogicalOrNode>\n");
	printer.Indent();
	int LabelNum = GetLabelCount();
	LogicalOr->left->Accept(this);
	EmitPop("$t0");
	printer.Print("bne $t0 $zero end_binary_or_with_one%i\n", LabelNum);
	LogicalOr->right->Accept(this);
	EmitPop("$t0");
	printer.Print("bne $t0 $zero end_binary_or_with_one%i\n", LabelNum);
	EmitPush("$t0");
	printer.Print("j end_binary_or%i\n", LabelNum);
	printer.Print("end_binary_or_with_one%i:\n", LabelNum);
	printer.Print("li $t0 1\n");
	EmitPush("$t0");
	printer.Print("end_binary_or%i:\n", LabelNum);
	printer.Unindent();	
	printer.Print("#;</LogicalOrNode>\n");

}

/* This function is called to multiply t0 and t1 and stores the value into t0  */
void CodeGen::Visit(MultiplyNode* Multiply)
{
	printer.Print("#;<MultiplyNode>\n");
	printer.Indent();
	EmitBinaryTermCode("mul $t0 $t0 $t1", "Multiply $t0 and $t1 put the answer in $t0\n", Multiply);
	printer.Unindent();	
	printer.Print("#;</MultiplyNode>\n");
}

/* This function is called to divide two numbers, t0 and t1*/
void CodeGen::Visit(DivideNode* Divide)
{
	printer.Print("#;<DivideNode>\n");
	printer.Indent();
	EmitBinaryTermCode("div $t0 $t1", "Divide $t0 by $t1 and put the answer in lo and hi\nmflo $t0\t#Move the conntents of the lo reg into $t0\n", Divide);
	printer.Unindent();	
	printer.Print("#;</DivideNode>\n");
}

/* This function is called to obtain the modulus of two numbers (7 % 5), so it takes the remainder from the register hi   */
void CodeGen::Visit(ModulusNode* Modulus)
{
	printer.Print("#;<ModulusNode>\n");
	printer.Indent();
	EmitBinaryTermCode("div $t0 $t1", "Divide $t0 by $t1 and put the answer in lo and hi\nmfhi $t0\t#Move the conntents of the hi reg into $t0\n", Modulus);
	printer.Unindent();	
	printer.Print("#;</ModulusNode>\n");
}

/* This function is called to evaluate a logical and of two integers from register t0 and t1 into t0 */
void CodeGen::Visit(LogicalAndNode* LogicalAnd)
{
	printer.Print("#;<LogicalAndNode>\n");
	printer.Indent();
	int LabelNum = GetLabelCount();
	LogicalAnd->left->Accept(this);
	EmitPop("$t0");
	printer.Print("beq $t0 $zero end_binary_and_with_zero%i\n", LabelNum);
	LogicalAnd->right->Accept(this);
	EmitPop("$t0");
	printer.Print("beq $t0 $zero end_binary_and_with_zero%i\n", LabelNum);
	printer.Print("li $t0 1\n");
	EmitPush("$t0");
	printer.Print("j end_binary_and%i\n", LabelNum);
	printer.Print("end_binary_and_with_zero%i:\n", LabelNum);
	EmitPush("$zero");
	printer.Print("end_binary_and%i:\n", LabelNum);
	printer.Unindent();	
	printer.Print("#;</LogicalAndNode>\n");
}

/* This function is called to execute the read from the keyboard */
void CodeGen::Visit(ReadNode* Read)
{
	printer.Print("#;<ReadNode>\n");
	printer.Indent();
	printer.Print("li $v0 5\t# This is the value to put in $v0 to read an int from the keyboard.\n");
	printer.Print("syscall\t #Do the system call.\n");
	EmitPush("$v0");
	printer.Unindent();	
	printer.Print("#;</ReadNode>\n");
}

void CodeGen::Visit(NullNode* Null)
{
	printer.Print("#;<NullNode>\n");
	printer.Indent();
	EmitPush("$zero");  //NULL is the same thing as zero
	printer.Unindent();	
	printer.Print("#;</NullNode>\n");
}

/* This function is executed when loading the address of a variable  */
void CodeGen::Visit(VariableAccessFactorNode* VariableAccess)
{
	printer.Print("#;<VariableAccessFactorNode>\n");
	printer.Indent();
	if(VariableAccess->variable) { 
		VariableAccess->variable->Accept(this);
		EmitPop("$t0");
		printer.Print("beq $t0 $zero Null_Pointer_Exception\t # The is the null pointer exception\n");
		printer.Print("lw $t1 0($t0)\t # Loads the value of into t1\n"); // VariableAccess->variable->name->Value);
		EmitPush("$t1");
	}
	printer.Unindent();	
	printer.Print("#;</VariableAccessFactorNode>\n");
}

/* This function is executed to load the value of a number into a register t0*/
void CodeGen::Visit(NumberNode* Number)
{
	printer.Print("#;<NumberNode value=\"%s\">\n", Number->value->Value);
	printer.Indent();
	printer.Print("li $t0 %s\t#Load %s into $t0\n", Number->value->Value, Number->value->Value);
	EmitPush("$t0");
	printer.Unindent();
	printer.Print("#;</NumberNode>\n");	
}

void CodeGen::Visit(UnaryMinusNode* UnaryMinus)
{
	printer.Print("#;<UnaryMinusNode>\n");	
	printer.Indent();
	if(UnaryMinus->value) UnaryMinus->value->Accept(this);	
	EmitPop("$t0");
	printer.Print("sub $t0, $zero $t0\n");
	EmitPush("$t0");
	printer.Unindent();	
	printer.Print("#;</UnaryMinusNode>\n");
}

void CodeGen::Visit(LogicalNotNode* LogicalNot)
{
	printer.Print("#;<LogicalNotNode>\n");
	printer.Indent();
	int LabelNum = GetLabelCount();
	if(LogicalNot->value) LogicalNot->value->Accept(this);
	EmitPop("$t0");
	printer.Print("beq $t0 $zero end_not_with_one%i\n", LabelNum);
	EmitPush("$zero");
	printer.Print("j end_not%i\n", LabelNum);
	printer.Print("end_not_with_one%i:\n", LabelNum);
	printer.Print("li $t0 1\n");
	EmitPush("$t0");
	printer.Print("end_not%i:\n", LabelNum);
	printer.Unindent();	
	printer.Print("#;</LogicalNotNode>\n");
}

void CodeGen::Visit(ArgumentNode* Argument)
{
	printer.Print("#;<ArgumentNode>\n");	
	printer.Indent();
	if(Argument->value) Argument->value->Accept(this);
	printer.Unindent();	
	printer.Print("#;</ArgumentNode>\n");
}

void CodeGen::Visit(ThisAccessNode* This)
{
	printer.Print("#;<ThisAccessNode>\n");	
	printer.Indent();

	State.WorkingClass = State.CurrentClass;

	printer.Print("addi $t0 $fp -%i\t # Gets the offset in the frame for the reference for THIS\n", (State.CurrentMethod->numParams * 4));
	EmitPush("$t0");

	if(This->innerAccess) { This->innerAccess->Accept(this); }

	printer.Unindent();	
	printer.Print("#;</ThisAccessNode>\n");
}

void CodeGen::Visit(ForeignMemberAccessNode* MemberAccess)
{
	printer.Print("#;<ForeignMemberAccessNode name=\"%s\">\n",MemberAccess->name->Value);	
	printer.Indent();

	VariableInfo *var = 0;
	MethodInfo *method = 0;

	printer.Print(" # This is the working class: %s\n", State.WorkingClass->Name);

	if(State.WorkingClass->Fields.Lookup(MemberAccess->name->Value, &var)) {
		EmitPop("$t0");
		printer.Print("lw $t0 0($t0)\t # Load the address of the class to t0\n"); 
		printer.Print("addi $t0 $t0 %i\t # Add the offset for %s to the address in $t0\n", var->Offset, MemberAccess->name->Value);
		EmitPush("$t0");
	} else if(State.WorkingClass->Methods.Lookup(MemberAccess->name->Value, &method)) {
		EmitPop("$t0");
		printer.Print("lw $t0 0($t0)\t # Load the address of the class to t0\n"); 
		EmitPush("$t0");
		EmitProcCall(method->UniqueName);
	} else if(State.WorkingClass->SecondaryType) {
		if(State.WorkingClass->SecondaryType->Fields.Lookup(MemberAccess->name->Value, &var)) {
			EmitPop("$t0");
			printer.Print("lw $t0 0($t0)\t # Load the address of the class to t0\n"); 
			printer.Print("addi $t0 $t0 %i\t # Add the offset for %s to the address in $t0\n", var->Offset, MemberAccess->name->Value);
			EmitPush("$t0");
		} else if(State.WorkingClass->SecondaryType->Methods.Lookup(MemberAccess->name->Value, &method)) {
			EmitPop("$t0");
			printer.Print("lw $t0 0($t0)\t # Load the address of the class to t0\n"); 
			EmitPush("$t0");
			EmitProcCall(method->UniqueName);
		}
	}

	printer.Unindent();	
	printer.Print("#;</ForeignMemberAccessNode>\n");
}

void CodeGen::Visit(ForeignMemberArrayAccessNode* MemberAccess)
{
	printer.Print("#;<ForeignMemberArrayAccessNode>\n");
	printer.Indent();

	printer.Print("# THIS CODE DIED-------------------------------------\n");
/*
	VariableInfo *var = 0;
	MethodInfo *method = 0;
	TypeInfo *working;

	if(State.WorkingClass->SecondaryType) working = State.WorkingClass->SecondaryType;
	else working = State.WorkingClass;


	if(working->Fields.Lookup(MemberAccess->name->Value, &var)) {
		EmitPop("$t0");
		printer.Print("lw $t0 0($t0)\t # Load the address of the class to t0\n"); 
		printer.Print("addi $t0 $t0 %i\t # Add the offset for %s to the address in $t0\n", var->Offset, MemberAccess->name->Value);
		EmitPush("$t0");
	} else if(working->Methods.Lookup(MemberAccess->name->Value, &method)) {
		EmitPop("$t0");
		printer.Print("lw $t0 0($t0)\t # Load the address of the class to t0\n"); 
		EmitPush("$t0");
		EmitProcCall(method->UniqueName);
	}


	//if(MemberAccess->index) MemberAccess->index->Accept(this);
*/

	printer.Unindent();	
	printer.Print("#;</ForeignMemberArrayAccessNode>\n");
}

void CodeGen::Visit(MemberAccessNode* MemberAccess)
{
	printer.Print("#;<MemberAccessNode name=\"%s\">\n",MemberAccess->name->Value);
	printer.Indent();
	VariableInfo *var = 0;

	if(State.CurrentMethod->Variables.Lookup(MemberAccess->name->Value, &var)) {
		printer.Print("addi $t0 $fp -%i\t # Loads the address of %s into $t0\n", var->Offset, MemberAccess->name->Value);
	} else if(State.CurrentMethod->Parameters.Lookup(MemberAccess->name->Value, &var)) {
		printer.Print("addi $t0 $fp -%i\t # Loads the address of %s into $t0\n", var->Offset, MemberAccess->name->Value);
	} else if(State.CurrentClass->Fields.Lookup(MemberAccess->name->Value, &var)) {
		printer.Print("addi $t0 $fp -%i\t # Gets the offset in the frame for the reference for THIS\n", (State.CurrentMethod->numParams * 4));
		printer.Print("lw $t0 0($t0) \t # Get the THIS reference into t0.\n");
		printer.Print("addi $t0 $t0 %i\t # Adds the offset in the class to the base of THIS\n", var->Offset);
	} else {
		printer.Print("addi $t0 $fp -%i\t # Gets the offset in the frame for the reference for THIS\n", (State.CurrentMethod->numParams * 4));
		printer.Print("lw $t0 0($t0) \t # Get the THIS reference into t0.\n");
	}
	EmitPush("$t0");

	if(var && var->Type) State.WorkingClass = var->Type;

	if(MemberAccess->innerAccess) { MemberAccess->innerAccess->Accept(this); }

	printer.Unindent();	
	printer.Print("#;</MemberAccessNode>\n");
}

void CodeGen::Visit(MemberArrayAccessNode* MemberAccess)
{
	printer.Print("#;<MemberArrayAccessNode name=\"%s\">\n",MemberAccess->name->Value);
	printer.Indent();

	VariableInfo *var = 0;

	if(State.CurrentMethod->Variables.Lookup(MemberAccess->name->Value, &var)) {
		printer.Print("addi $s1 $fp -%i\t # Loads the address of %s into $s1\n", var->Offset, MemberAccess->name->Value);
	} else if(State.CurrentMethod->Parameters.Lookup(MemberAccess->name->Value, &var)) {
		printer.Print("addi $s1 $fp -%i\t # Loads the address of %s into $s1\n", var->Offset, MemberAccess->name->Value);
	} else if(State.CurrentClass->Fields.Lookup(MemberAccess->name->Value, &var)) {
		printer.Print("addi $t0 $fp -%i\t # Gets the offset in the frame for the reference for THIS\n", (State.CurrentMethod->numParams * 4));
		printer.Print("lw $t0 0($t0) \t # Get the THIS reference into t0.\n");
		printer.Print("addi $s1 $t0 %i\t # Adds the offset in the class to the base of THIS\n", var->Offset);
	} else {
		// This code should never get run so if this gets called something really bad happened.
		printer.Print(" # FAILED-----------------------------------------------------\n");
	}

	EmitPush("$s1");

	if(MemberAccess->index) {
		MemberAccess->index->Accept(this);
		EmitPop("$t1");
		printer.Print("addi $t1 $t1 1\n");
		EmitPop("$s1");

		printer.Print("blt $t1 $zero Out_Of_Bounds_Exception\t # This is the out of bound exception\n");
		printer.Print("lw $s1 0($s1)\t # Load the address of the array\n");
		printer.Print("beq $s1 $zero Null_Pointer_Exception\t # The is the null pointer exception\n");
		printer.Print("lw $s0 0($s1)\t # Load in the size of the array\n");
		printer.Print("bgt $t1 $s0 Out_Of_Bounds_Exception\t # this is the out of bound exception\n");
		printer.Print("li $t2 4\n");
		printer.Print("mult $t1 $t2\n");
		printer.Print("mflo $t1\n");
		printer.Print("add $t1 $s1 $t1\t # Loads the value at the offset into the address of t1\n");
		EmitPush("$t1");
	}

	if(var && var->Type) State.WorkingClass = var->Type;

	printer.Print(" # This is the type name %s\n", var->Type->Name);

	if(MemberAccess->innerAccess) MemberAccess->innerAccess->Accept(this);
	//EmitPop("$t0");
	//printer.Print("lw $t0 0($t0)\n");
	//EmitPush("$t0");

	printer.Unindent();	
	printer.Print("#;</MemberArrayAccessNode>\n");
}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
//Other functions not dealing with the visitor.
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////




// These two function are used to print the code for pushing
// and poping off the stack.
void CodeGen::EmitPop(char* reg) {
	printer.Indent();
	printer.Print("addi $sp $sp 4\t#  This is the code that       \n");
	printer.Print("lw %s 0($sp)\t#    pops %s off the stack.      \n",reg,reg);
	printer.Unindent();
}
void CodeGen::EmitPush(char* reg) {
	printer.Indent();
	printer.Print("sw %s 0($sp)\t#    This is the code to push %s \n",reg,reg);
	printer.Print("addi $sp $sp -4\t# onto the stack.             \n");
	printer.Unindent();
}



// THe next three function are used to print Binary code basicly
// they all visit the left then the right and print what ever code
// needs to be generated by it.
////////////////////////////////////

	void CodeGen::EmitBinaryTermCode(char* command, char* comment, BinaryTermNode* bse) {
		bse->left->Accept(this);
		bse->right->Accept(this);
		// This is the run time divide by zero exception.
		EmitPop("$t1");
		EmitPop("$t0");

		if( !strcmp(command,"div $t0 $t1") ) {
			printer.Print("beq $t1 $zero Divide_By_Zero_Exception\t#Jump to Exception if try to Divide by Zero\n");
		} 
		printer.Print("%s\t#%s",command, comment);
		EmitPush("$t0");
	}
	void CodeGen::EmitBinarySubExp(char* command, char* comment, BinarySubExpressionNode* bse) {
		bse->left->Accept(this);
		bse->right->Accept(this);
		EmitPop("$t1");
		EmitPop("$t0");
		printer.Print("%s\t#%s",command, comment);
		EmitPush("$t0");
	}
	void CodeGen::EmitBinRelExp(char* command, char* comment, BinaryRelationalExpressionNode* bre) {
		bre->left->Accept(this);
		bre->right->Accept(this);
		EmitPop("$t1");
		EmitPop("$t0");
		printer.Print("%s\t#%s\n",command,comment);
		EmitPush("$t0");
	}
/////////////////////////////////




// It generates the code for the decaf built-in print function
// This code prints the print function in decaf basicly the number
// of args is on the stack and then that many args are behind it
// on the stack.
void CodeGen::EmitPrintCode() {
	printer.Indent();
	printer.Print("\n\n#\n# PRINT FUNCTION\n#\n");
	printer.Print("print:\t\t# The label to the print function.\n");
	printer.Print("addi $sp $sp 4\n");
	printer.Print("lw $t0 0($sp)\t\t# Grab the number of args from the stack.\n");
	printer.Print("li $t1, 4\t\t# Load the number 4 into $t1\n");
	printer.Print("mult $t0 $t1\t\t# Multiply the args times 4\n");
	printer.Print("mflo $t2\t\t# Grab the number of bytes that the args use.\n");
	printer.Print("add $s0 $sp $t2\t\t# Set $s0 to point to the address of the first arg.\n");
	printer.Print("print_loop:\t\t# This is the printer loop.\n");
	printer.Print("beq $s0 $sp print_end\t\t# If there is not param's left jump to the end.\n");
	EmitPrint("$s0");
	printer.Print("addi $s0 $s0 -4\t\t# Decrement the counter.\n");
	printer.Print("j print_loop\t\t# Jump to the begining of this loop.\n");
	printer.Print("print_end:\t\t# Print end\n");
	printer.Print("lw $t0 0($sp)\n");
	printer.Print("li $t1, 4\n");
	printer.Print("mult $t0 $t1\n");
	printer.Print("mflo $t0\n");
	printer.Print("addi $t0 $t0 4\n");
	printer.Print("add $sp $sp $t0\n");
	printer.Print("addi $sp $sp -4\n");
	printer.Print("jr $ra\t\t# Jump to the return address.\n");
	printer.Unindent();	
}

/* It prints whatever is in the register that it receives */
// This is used to print the value that is in the address of reg
void CodeGen::EmitPrint(char* reg) {
	printer.Indent();
	printer.Print("lw $a0 0(%s)\t#This moves the value in the addess of %s into $s0 \n", reg, reg);
	printer.Print("li $v0 1\t#                                                      \n");
	printer.Print("syscall\t#                    Then it prints a space             \n");
	printer.Print("la $a0 SP\t#                  does the syscall and is            \n");
	printer.Print("li $v0 4\t#                   done.                              \n");
	printer.Print("syscall\t#                                                       \n");
	printer.Unindent();	
}


//PrintReg("$s1");
// This function was used just to insert print code for a certain register
// this function never actully gets called in the compiler but just used
// for debuging pruposes.
void CodeGen::PrintReg(char* reg) {
	printer.Indent();
	printer.Print("move $a0 %s\t#   Puts the value in %s into register $a0.\n", reg, reg);
	printer.Print("li $v0 1\t#                                             \n");
	printer.Print("syscall\t#       Prints the register that it            \n");
	printer.Print("la $a0 SP\t#     loads the address of the SP            \n");
	printer.Print("li $v0 4\t#      string which is a space and            \n");
	printer.Print("syscall\t#       then prints it.                        \n");
	printer.Unindent();	
}

// It allocates an amount of memory
// And puts the base address on to the stack
void CodeGen::EmitAloc(bool SetSize) {
	printer.Indent();	
	int labelNum;
	labelNum=GetLabelCount(); // Gets the number of the next loop
	EmitPop("$s0");
	printer.Print("move $s2 $s0\t#      This is the chuck of code that      \n");
	printer.Print("li $t1 4\t#          alocates the amount of memory       \n");
	printer.Print("mul $s1 $s0 $t1\t#   that was pushed onto the stack      \n");
	printer.Print("addi $a0 $s1 4\t#                                        \n");
	printer.Print("li $v0 9\t#                                              \n");
	printer.Print("syscall\t#           Does the call to get the memory     \n");
	printer.Print("sw $s2 0($v0)\t#                                         \n");
	printer.Print("move $s5 $v0\t                                           \n");
	printer.Print("add $t0 $s1 $s5\t#              Now we go through the    \n");
	printer.Print("start_init_loop%i:\t#           chunk of memory to set   \n", labelNum);
	printer.Print("beq $s5 $t0 end_init_loop%i\t#  all the values to 0      \n", labelNum);
	printer.Print("sw $zero 0($t0)\t#                                       \n");
	printer.Print("addi $t0 $t0 -4\t#                                       \n");
	printer.Print("j start_init_loop%i\t#                                   \n", labelNum);
	printer.Print("end_init_loop%i:\t#                                      \n", labelNum);
	printer.Print("sw $zero 0($s5)\t#                                       \n");
	if(SetSize) { printer.Print("sw $s0 0($s5)\t # This is only done if SetSize is true.\n"); }
	EmitPush("$s5");
}

//Prints the header information at the top of the SPIM file.
// This is also used to setup the data segments of the program.
void CodeGen::EmitHeaderCode() {
	printer.Print("\n\n#\n# PROGRAM HEADER\n#\n");
	printer.Print(".data\n");
	printer.Print("SP:\t.asciiz \" \"\n");
	printer.Print("N:\t.asciiz \"\\n\"\n");
	printer.Print("OutOfBounds:\t.asciiz \"EXCEPTION: Array access out of bounds\\n\"\n");
	printer.Print("DivideByZero:\t.asciiz \"EXCEPTION: Divide by Zero\\n\"\n");
	printer.Print("NullPointer:\t.asciiz \"EXCEPTION: Null pointer\\n\"\n");
	printer.Print(".text\n");
	printer.Print(".globl main\n");
	printer.Print("main:\n");  
}


// This Function is used just to emit some exception code at the end of
// the program where the program will just to if a run time exception is
// Encountered.
void CodeGen::EmitExceptionCode() {
	printer.Print("\n\n#\n# EXCEPTION HANDLING\n#\n");
	printer.Print("Out_Of_Bounds_Exception:\n");
	EmitPrintDefinedString("OutOfBounds");	
	printer.Print("j ___end_exception\n");
	printer.Print("Divide_By_Zero_Exception:\n");
	EmitPrintDefinedString("DivideByZero");
	printer.Print("j ___end_exception\n");
	printer.Print("Null_Pointer_Exception:\n");
	EmitPrintDefinedString("NullPointer");
	printer.Print("j ___end_exception\n");
	//Halt the program
	printer.Print("___end_exception:\n");
	printer.Print("la $v0 10\n");
	printer.Print("syscall\n");
}

//Function accepts a string and generates the code for printing.  The string must
//already be defined in the header information.  Pass the string's name as used in
//the header.
void CodeGen::EmitPrintDefinedString(char* stringName) {
	printer.Print("la $a0 %s\t\t# Move address of %s into register $a0.\n", stringName, stringName);
	printer.Print("li $v0 4\t\t# Load in the print_string value.\n");
	printer.Print("syscall\t\t# Do the syscall.\n");
}
	
