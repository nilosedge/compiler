//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//  This is the Print tester.  This utilizes the visitation scheme to visit each of the nodes
//  and produce the Abstract Syntax Tree.  No code is generated.  The nodes are simply printed
//  in XML format.
//  This traversers the whole tree.
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

#include "PrintTest.h"
#include "Printer.h"

void PrintTest::Visit(ProgramNode* Program)
{
	printer.Print("<program>\n");
	printer.Indent();
	if(Program->classes) Program->classes->Accept(this);
	printer.Unindent();
	printer.Print("</program>\n");
}

void PrintTest::Visit(ClassNode* Class)
{
	printer.Print("<class name=\"%s\">\n", Class->name->Value);
	printer.Indent();
	if (Class->body->fields) Class->body->fields->Accept(this);
	if (Class->body->methods) Class->body->methods->Accept(this);
	printer.Unindent();
	printer.Print("</class>\n");
}

void PrintTest::Visit(FieldNode* Field)
{
	printer.Print("<field name=\"%s\">\n", Field->name->Value);
	printer.Indent();
	if(Field->type) Field->type->Accept(this);
	printer.Unindent();
	printer.Print("</field>\n");
}

void PrintTest::Visit(MethodNode* Method)
{
	printer.Print("<method name=\"%s\">\n", Method->name->Value);
	printer.Indent();
	if(Method->type) Method->type->Accept(this);
	if(Method->parameters) Method->parameters->Accept(this);
	if(Method->body) Method->body->Accept(this);
	printer.Unindent();
	printer.Print("</method>\n");
}

void PrintTest::Visit(ParameterNode* Parameter)
{
	printer.Print("<Parameter name=\"%s\">\n",Parameter->name->Value);
	printer.Indent();
	if(Parameter->type) Parameter->type->Accept(this);
	printer.Unindent();
	printer.Print("</Parameter>\n");	
}

void PrintTest::Visit(MethodBodyNode* MethodBody)
{
	printer.Print("<MethodBody>\n");
	printer.Indent();
	if(MethodBody->variables) MethodBody->variables->Accept(this);
	if(MethodBody->statements) MethodBody->statements->Accept(this);
	printer.Unindent();	
	printer.Print("</MethodBody>\n");
}

void PrintTest::Visit(VariableNode* Variable)
{
	printer.Print("<VariableNode name=\"%s\">\n",Variable->name->Value);
	printer.Indent();
	if(Variable->type) Variable->type->Accept(this);
	printer.Unindent();	
	printer.Print("</VariableNode>\n");
}

void PrintTest::Visit(AssignmentStatementNode* Assignment)
{
	printer.Print("<AssignmentStatementNode>\n");	
	printer.Indent();
	if(Assignment->member) Assignment->member->Accept(this);
	if(Assignment->value) Assignment->value->Accept(this);
	printer.Unindent();	
	printer.Print("</AssignmentStatementNode>\n");
}

void PrintTest::Visit(PrintStatementNode* Print)
{
	printer.Print("<PrintStatementNode>\n");
	printer.Indent();
	if(Print->arguments) Print->arguments->Accept(this);
	printer.Unindent();	
	printer.Print("</PrintStatementNode>\n");
}

void PrintTest::Visit(IfStatementNode* If)
{
	printer.Print("<IfStatementNode>\n");
	printer.Indent();
	if(If->booleanExpression) If->booleanExpression->Accept(this);
	if(If->trueCase) If->trueCase->Accept(this);
	if(If->falseCase) If->falseCase->Accept(this);
	printer.Unindent();	
	printer.Print("</IfStatementNode>\n");
}

void PrintTest::Visit(WhileStatementNode* While)
{
	printer.Print("<WhileStatementNode>\n");
	printer.Indent();
	if(While->value) While->value->Accept(this);
	if(While->loop) While->loop->Accept(this);
	printer.Unindent();	
	printer.Print("</WhileStatementNode>\n");
}

void PrintTest::Visit(ReturnStatementNode* Return)
{
	printer.Print("<ReturnStatementNode>\n");	
	printer.Indent();
	if(Return->returnValue) Return->returnValue->Accept(this);
	printer.Unindent();	
	printer.Print("</ReturnStatementNode>\n");
}

void PrintTest::Visit(MethodCallStatementNode* MethodCall)
{
	printer.Print("<MethodCallSatementNode>\n");	
	printer.Indent();
	if(MethodCall->member) MethodCall->member->Accept(this);
	if(MethodCall->arguments) MethodCall->arguments->Accept(this);
	printer.Unindent();	
	printer.Print("</MethodCallStatementNode>\n");
}

	//virtual void Visit(MethodCallNode* MethodCall) = 0;
void PrintTest::Visit(MethodCallNode* MethodCall)
{
	printer.Print("<MethodCallNode>\n");	
	printer.Indent();
	if(MethodCall->member) MethodCall->member->Accept(this);
	if(MethodCall->arguments) MethodCall->arguments->Accept(this);
	printer.Unindent();	
	printer.Print("</MethodCallNode>\n");
}

void PrintTest::Visit(NoOpStatementNode* NoOp)
{
	printer.Print("<NoOpStatementNode>\n");
	printer.Indent();
	printer.Unindent();	
	printer.Print("</NoOpStatementNode>\n");
}

void PrintTest::Visit(IsEqualNode* IsEqual)
{
	printer.Print("<IsEqualNode>\n");	
	printer.Indent();
	if(IsEqual->left) IsEqual->left->Accept(this);
	if(IsEqual->right) IsEqual->right->Accept(this);
	printer.Unindent();	
	printer.Print("</IsEqualNode>\n");
}

void PrintTest::Visit(IsNotEqualNode* IsNotEqual)
{
	printer.Print("<IsNotEqualNode>\n");	
	printer.Indent();
	if(IsNotEqual->left) IsNotEqual->left->Accept(this);
	if(IsNotEqual->right) IsNotEqual->right->Accept(this);
	printer.Unindent();	
	printer.Print("</IsNotEqualNode>\n");
}

void PrintTest::Visit(IsLessThanNode* IsLessThan)
{
	printer.Print("<IsLessThanNode>\n");
	printer.Indent();
	if(IsLessThan->left) IsLessThan->left->Accept(this);
	if(IsLessThan->right) IsLessThan->right->Accept(this);
	printer.Unindent();	
	printer.Print("</IsLessThanNode>\n");
}

void PrintTest::Visit(IsGreaterThanNode* IsGreaterThan)
{
	printer.Print("<IsGreaterThanNode>\n");	
	printer.Indent();
	if(IsGreaterThan->left) IsGreaterThan->left->Accept(this);
	if(IsGreaterThan->right) IsGreaterThan->right->Accept(this);
	printer.Unindent();	
	printer.Print("</IsGreaterThanNode>\n");
}

void PrintTest::Visit(IsLessThanOrEqualNode* IsLessThanOrEqual)
{
	printer.Print("<IsLessThanOrEqualNode>\n");
	printer.Indent();
	if(IsLessThanOrEqual->left) IsLessThanOrEqual->left->Accept(this);
	if(IsLessThanOrEqual->right) IsLessThanOrEqual->right->Accept(this);
	printer.Unindent();	
	printer.Print("</IsLessThanOrEqualNode>\n");
}

void PrintTest::Visit(IsGreaterThanOrEqualNode* IsGreaterThanOrEqual)
{
	printer.Print("<IsGreaterThanOrEqualNode>\n");
	printer.Indent();
	IsGreaterThanOrEqual->left->Accept(this);
	IsGreaterThanOrEqual->right->Accept(this);
	printer.Unindent();	
	printer.Print("</IsGreaterThanOrEqualNode>\n");
}

void PrintTest::Visit(ConstructorCallNode* ConstructorCall)
{
	printer.Print("<ConstructorCallNode>\n");
	printer.Indent();
	if(ConstructorCall->type) ConstructorCall->type->Accept(this);
	printer.Unindent();	
	printer.Print("</ConstructorCallNode>\n");
}

void PrintTest::Visit(ArrayConstructorCallNode* ArrayConsturctorCall)
{
	printer.Print("<ArrayConstructorCallNode>\n");
	printer.Indent();
	if(ArrayConsturctorCall->type) ArrayConsturctorCall->type->Accept(this);
	if(ArrayConsturctorCall->count) ArrayConsturctorCall->count->Accept(this);
	printer.Unindent();
	printer.Print("</ArrayConstructorCallNode>\n");	
}

void PrintTest::Visit(AdditionNode* Addition)
{
	printer.Print("<AdditionNode>\n");	
	printer.Indent();
	if(Addition->left) Addition->left->Accept(this);
	if(Addition->right) Addition->right->Accept(this);
	printer.Unindent();	
	printer.Print("</AdditionNode>\n");
}

void PrintTest::Visit(SubtractionNode* Subtraction)
{
	printer.Print("<SubtractionNode>\n");
	printer.Indent();
	if(Subtraction->left) Subtraction->left->Accept(this);
	if(Subtraction->right) Subtraction->right->Accept(this);
	printer.Unindent();	
	printer.Print("</SubtractionNode>\n");
}

void PrintTest::Visit(LogicalOrNode* LogicalOr)
{
	printer.Print("<LogicalOrNode>\n");
	printer.Indent();
	if(LogicalOr->left) LogicalOr->left->Accept(this);
	if(LogicalOr->right) LogicalOr->right->Accept(this);
	printer.Unindent();	
	printer.Print("</LogicalOrNode>\n");
}

void PrintTest::Visit(MultiplyNode* Multiply)
{
	printer.Print("<MultiplyNode>\n");
	printer.Indent();
	if(Multiply->left) Multiply->left->Accept(this);
	if(Multiply->right) Multiply->right->Accept(this);
	printer.Unindent();	
	printer.Print("</MultiplyNode>\n");
}

void PrintTest::Visit(DivideNode* Divide)
{
	printer.Print("<DivideNode>\n");
	printer.Indent();
	if(Divide->left) Divide->left->Accept(this);	
	if(Divide->right) Divide->right->Accept(this);	
	printer.Unindent();	
	printer.Print("</DivideNode>\n");
}

void PrintTest::Visit(ModulusNode* Modulus)
{
	printer.Print("<ModulusNode>\n");
	printer.Indent();
	if(Modulus->left) Modulus->left->Accept(this);
	if(Modulus->right) Modulus->right->Accept(this);
	printer.Unindent();	
	printer.Print("</ModulusNode>\n");
}

void PrintTest::Visit(LogicalAndNode* LogicalAnd)
{
	printer.Print("<LogicalAndNode>\n");
	printer.Indent();
	if(LogicalAnd->left) LogicalAnd->left->Accept(this);
	if(LogicalAnd->right) LogicalAnd->right->Accept(this);
	printer.Unindent();	
	printer.Print("</LogicalAndNOde>\n");
}

void PrintTest::Visit(ReadNode* Read)
{
	printer.Print("<ReadNode>\n");
	printer.Indent();
	printer.Unindent();	
	printer.Print("</ReadNode>\n");
}

void PrintTest::Visit(NullNode* Null)
{
	printer.Print("<NullNode>\n");
	printer.Indent();
	
	printer.Unindent();	
	printer.Print("</NullNode>\n");
}

void PrintTest::Visit(VariableAccessFactorNode* VariableAccess)
{
	printer.Print("<VariableAccessFactorNode>\n");
	printer.Indent();
	if(VariableAccess->variable) VariableAccess->variable->Accept(this);
	printer.Unindent();	
	printer.Print("</VariableAccessFactorNode>\n");
}

void PrintTest::Visit(NumberNode* Number)
{
	printer.Print("<NumberNode value=\"%s\">\n", Number->value->Value);
	printer.Indent();
	printer.Unindent();
	printer.Print("</NumberNode>\n");	
}

void PrintTest::Visit(UnaryMinusNode* UnaryMinus)
{
	printer.Print("<UnaryMinusNode>\n");	
	printer.Indent();
	if(UnaryMinus->value) UnaryMinus->value->Accept(this);	
	printer.Unindent();	
	printer.Print("</UnaryMinusNode>\n");
}

void PrintTest::Visit(LogicalNotNode* LogicalNot)
{
	printer.Print("<LogicalNotNode>\n");
	printer.Indent();
	
	printer.Unindent();	
	printer.Print("</LogicalNotNode>\n");
}

void PrintTest::Visit(ArgumentNode* Argument)
{
	printer.Print("<ArgumentNode>\n");	
	printer.Indent();
	if(Argument->value) Argument->value->Accept(this);
	printer.Unindent();	
	printer.Print("</ArgumentNode>\n");
}

void PrintTest::Visit(ThisAccessNode* This)
{
	printer.Print("<ThisAccessNode>\n");	
	printer.Indent();
	
	printer.Unindent();	
	printer.Print("</ThisAccessNode>\n");
}

void PrintTest::Visit(ForeignMemberAccessNode* MemberAccess)
{
	printer.Print("<ForeignMemberAccessNode name=\"%s\">\n",MemberAccess->name->Value);	
	printer.Indent();

	printer.Unindent();	
	printer.Print("</ForeignMemberAccessNode>\n");
}

void PrintTest::Visit(MemberAccessNode* MemberAccess)
{
	printer.Print("<MemberAccessNode name=\"%s\">\n",MemberAccess->name->Value);
	printer.Indent();
	if(MemberAccess->innerAccess) MemberAccess->innerAccess->Accept(this);
	printer.Unindent();	
	printer.Print("</MemberAccessNode>\n");
}

void PrintTest::Visit(ForeignMemberArrayAccessNode* MemberAccess)
{
	printer.Print("<ForeignMemberArrayAccessNode>\n");
	printer.Indent();
	if(MemberAccess->index) MemberAccess->index->Accept(this);
	printer.Unindent();	
	printer.Print("</ForeignMemberArrayAccessNode>\n");
}

void PrintTest::Visit(MemberArrayAccessNode* MemberAccess)
{
	printer.Print("<MemberArrayAccessNode name=\"%s\">\n",MemberAccess->name->Value);
	printer.Indent();
	if(MemberAccess->innerAccess) MemberAccess->innerAccess->Accept(this);
	if(MemberAccess->index) MemberAccess->index->Accept(this);
	printer.Unindent();	
	printer.Print("</MemberArrayAccessNode>\n");
}

void PrintTest::Visit(TypeNode* Type)
{
	printer.Print("<TypeNode type=\"%i\">\n", Type->typeID);
	printer.Indent();
	
	printer.Unindent();	
	printer.Print("</TypeNode>\n");
}

void PrintTest::Visit(ArrayTypeNode* Array)
{
	printer.Print("<ArrayTypeNode>");	
	printer.Indent();
	if(Array->innerType) Array->innerType->Accept(this);
	printer.Unindent();	
	printer.Print("</ArrayTypeNode>");
}



