// AlphaAnalizer.cpp:
// Populate the Global Type symbol table.
// After this pass all duplicate class declaration should be caught.

#include "AlphaAnalizer.h"


// Statically load some generic types into the global type table
void AlphaAnalizer::Visit(ProgramNode* Program)
{
	Program->classes->Accept(this);

	TypeInfo* void_type = new TypeInfo();
	void_type->Name = "void";
	void_type->IsReferenceType = false;
	State.Program->Classes.Add("void", void_type); 

	TypeInfo* int_type = new TypeInfo();
	int_type->Name = "int";
	int_type->IsReferenceType = false;
	State.Program->Classes.Add("int", int_type); 

	// Stefan - I added this null type to handle
	//   null pointers I am not sure if we will
	//   find this useful, but for now we have it
	TypeInfo* null_type = new TypeInfo();
	null_type->Name = "null";
	State.Program->Classes.Add("null", null_type); 
}

// Add each class as a unique type to the global type table.
void AlphaAnalizer::Visit(ClassNode* Class)
{
	State.CurrentClass->Name = Class->name->Value;
	TypeInfo*  FirstDec = NULL;

	if(State.Program->Classes.Lookup(State.CurrentClass->Name, &FirstDec)) {
		char message[160];
		snprintf(message, 160, "Duplicate class declaration \'%*s\'",  strlen(State.CurrentClass->Name),
				 State.CurrentClass->Name);
		State.Errors->PrintError(message, Class->RowNumber, false);
	} else {
		State.Program->Classes.Add(State.CurrentClass->Name, State.CurrentClass);
	}
}

// The rest of these are just here so we fulfill the interface. 
// We don't actually use any of them.
void AlphaAnalizer::Visit(FieldNode* Field)
{
	
}

void AlphaAnalizer::Visit(MethodNode* Method)
{
	
}

void AlphaAnalizer::Visit(MethodCallNode* MethodCall)
{
	
}

void AlphaAnalizer::Visit(ParameterNode* Parameter)
{
	
}

void AlphaAnalizer::Visit(MethodBodyNode* MethodBody)
{
	
}

void AlphaAnalizer::Visit(VariableNode* Variable)
{
	
}

void AlphaAnalizer::Visit(AssignmentStatementNode* Assignment)
{
	
}

void AlphaAnalizer::Visit(PrintStatementNode* Print)
{
	
}

void AlphaAnalizer::Visit(IfStatementNode* If)
{
	
}

void AlphaAnalizer::Visit(WhileStatementNode* While)
{
	
}

void AlphaAnalizer::Visit(ReturnStatementNode* Return)
{
	
}

void AlphaAnalizer::Visit(MethodCallStatementNode* MethodCall)
{
	
}

void AlphaAnalizer::Visit(NoOpStatementNode* NoOp)
{
	
}

void AlphaAnalizer::Visit(IsEqualNode* IsEqual)
{
	
}

void AlphaAnalizer::Visit(IsNotEqualNode* IsNotEqual)
{
	
}

void AlphaAnalizer::Visit(IsLessThanNode* IsLessThan)
{
	
}

void AlphaAnalizer::Visit(IsGreaterThanNode* IsGreaterThan)
{
	
}

void AlphaAnalizer::Visit(IsLessThanOrEqualNode* IsLessThanOrEqual)
{
	
}

void AlphaAnalizer::Visit(IsGreaterThanOrEqualNode* IsGreaterThanOrEqual)
{
	
}

void AlphaAnalizer::Visit(ConstructorCallNode* ConstructorCall)
{
	
}

void AlphaAnalizer::Visit(ArrayConstructorCallNode* ArrayConsturctorCall)
{
	
}

void AlphaAnalizer::Visit(AdditionNode* Addition)
{
	
}

void AlphaAnalizer::Visit(SubtractionNode* Subtraction)
{
	
}

void AlphaAnalizer::Visit(LogicalOrNode* LogicalOr)
{
	
}

void AlphaAnalizer::Visit(MultiplyNode* Multiply)
{
	
}

void AlphaAnalizer::Visit(DivideNode* Divide)
{
	
}

void AlphaAnalizer::Visit(ModulusNode* Modulus)
{
	
}

void AlphaAnalizer::Visit(LogicalAndNode* LogicalAnd)
{
	
}

void AlphaAnalizer::Visit(ReadNode* Read)
{
	
}

void AlphaAnalizer::Visit(NullNode* Null)
{
	
}

void AlphaAnalizer::Visit(VariableAccessFactorNode* VariableAccess)
{
	
}

void AlphaAnalizer::Visit(NumberNode* Number)
{
	
}

void AlphaAnalizer::Visit(UnaryMinusNode* UnaryMinus)
{
	
}

void AlphaAnalizer::Visit(LogicalNotNode* LogicalNot)
{
	
}

void AlphaAnalizer::Visit(ArgumentNode* Argument)
{
	
}

void AlphaAnalizer::Visit(ThisAccessNode* This)
{
	
}

void AlphaAnalizer::Visit(ForeignMemberAccessNode* MemberAccess)
{
	
}

void AlphaAnalizer::Visit(MemberAccessNode* MemberAccess)
{
	
}

void AlphaAnalizer::Visit(ForeignMemberArrayAccessNode* MemberAccess)
{
	
}

void AlphaAnalizer::Visit(MemberArrayAccessNode* MemberAccess)
{
	
}

void AlphaAnalizer::Visit(TypeNode* Type)
{
	
}

void AlphaAnalizer::Visit(ArrayTypeNode* Array)
{
	
}

