
#include "Buffer.h"
#include <string.h>
#include <iostream>
#include <fstream>

std::vector<char*> temp;

	///
	// Initialize the buffer
	Buffer::Buffer()
	{
		currentString = 0;
		backupCurrentString = 0;
		readCount = 0;
	}

	///
	// Free memory used by the buffer
	Buffer::~Buffer()
	{
		Queue::iterator it;
		for (it = source.begin(); it != source.end(); it++)
			delete *it;
	}
	
	///
	// reads some data from the buffer
	int Buffer::Read(char* buf, int maxSize)
	{
		if (!currentString || *currentString == 0)
			GetNextString();

		if (!currentString)
		{
			buf[0] = 0;
			buf[1] = 0;
			return 0;
		}

		int len = strlen(currentString);
		int chars = (len < maxSize) ? len : (maxSize);

		strncpy(buf, currentString, chars);
		
		currentString += chars;

		return chars;
	}

	///
	// starts reading the next line in the buffer
	void Buffer::GetNextString()
	{
		//delete backupCurrentString;
		if (readCount < source.size())
		{
			backupCurrentString = currentString = source[readCount];
			readCount++;
		}
		else
			backupCurrentString = currentString = 0;
	}

    ///
    // Enbuffer the data from stdin
    void Buffer::GetStdIn()
    {
        char buffer[256];

        bool eof = std::cin.eof();
        while (!eof)
        {
            std::cin.getline(buffer, 250);
            char* string = new char[strlen(buffer) + 5];

            strcpy(string, buffer);

            eof = std::cin.eof();
            //if (!eof)
                strcat(string, "\n");

            source.push_back(string);
        }
    }

	///
	// Enbuffer the data from a file
	void Buffer::GetFile(const char* fileName)
	{
		char buffer[256];
		std::ifstream in(fileName);

		bool eof = in.eof();
		while (!eof)
		{
			in.getline(buffer, 250);
			char* string = new char[strlen(buffer) + 5];

			strcpy(string, buffer);
		
			eof = in.eof();
			//if (!eof)
				strcat(string, "\n");
				
			source.push_back(string);
		}
	}

	///
	// returns the pointer to the current line
	const char* Buffer::GetLine()
	{
		return backupCurrentString;
	}

	///
	// returns a pointer to the specified line
	//   if the requested line doesn't exist, then "\n"
	//   is returned
	const char* Buffer::GetLine(int line)
	{
		if (((unsigned int)line < source.size()) && (0 <= line))
			return source[line];
		else
			return " -- No Line Data -- \n";
	}

