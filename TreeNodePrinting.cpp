
#include "TreeNode.h"

////////
//  Every TreeNode based class must be able to print itself 
//    These are the methods that provide the functionality 
//	  for this functionality

void TypeNode::BeginPrintNode(Printer* printer)
{
	const char* name;

	switch (typeID)
	{
		case New:
			name = typeName->Value;
			break;
			
		case Void:
			name = "void";
			break;

		case Int:
			name = "int";
			break;

		case Array:
			name = "Array";
			break;
	}
	printer->Print("<%s name=\"%s\">\n", nodeName, name);
}


void ArrayTypeNode::PrintChildren(Printer* printer)
{
	innerType->Print(printer);
}

void NameWithTypeNode::BeginPrintNode(Printer* printer)
{
	printer->Print("<%s name=\"%s\">\n", nodeName, name->Value);
}

void NameWithTypeNode::PrintChildren(Printer* printer)
{
	type->Print(printer);
}

void NameWithReturnTypeNode::BeginPrintNode(Printer* printer)
{
	printer->Print("<%s name=\"%s\">\n", nodeName, name->Value);
}

void NameWithReturnTypeNode::PrintChildren(Printer* printer)
{
	type->Print(printer);
}

void ForeignMemberAccessNode::BeginPrintNode(Printer* printer)
{
	printer->Print("<%s name=\"%s\">\n", nodeName, name->Value);
}

void MemberAccessNode::BeginPrintNode(Printer* printer)
{
	if (name)
		printer->Print("<%s name=\"%s\">\n", nodeName, name->Value);
	else
		TreeNode::BeginPrintNode(printer);
}

void ForeignMemberArrayAccessNode::PrintChildren(Printer* printer)
{
	index->Print(printer);
}

void MemberArrayAccessNode::PrintChildren(Printer* printer)
{
	index->Print(printer);
}

void ArgumentNode::PrintChildren(Printer* printer)
{
	value->Print(printer);
}

void UnaryOperationNode::PrintChildren(Printer* printer)
{
	value->Print(printer);
}

void ParenExpressionNode::PrintChildren(Printer* printer)
{
	value->Print(printer);
}

void VariableAccessFactorNode::PrintChildren(Printer* printer)
{
	variable->Print(printer);
}

void NumberNode::BeginPrintNode(Printer* printer)
{
	printer->Print("<%s value=\"%s\">\n", nodeName, value->Value);
}

void BinaryTermNode::PrintChildren(Printer* printer)
{
	left->Print(printer);
	right->Print(printer);
}

void BinarySubExpressionNode::PrintChildren(Printer* printer)
{
	left->Print(printer);
	right->Print(printer);
}

void BinaryRelationalExpressionNode::PrintChildren(Printer* printer)
{
	left->Print(printer);
	right->Print(printer);
}

void ConstructorCallNode::PrintChildren(Printer* printer)
{
	type->Print(printer);
}

void ArrayConstructorCallNode::PrintChildren(Printer* printer)
{
	type->Print(printer);
	count->Print(printer);
}

void StatementNodes::BeginPrintNode(Printer* printer)
{
	printer->Unindent();
}

void StatementNodes::EndPrintNode(Printer* printer)
{
	printer->Indent();
}

void StatementNodes::PrintChildren(Printer* printer)
{
	PrintNodes(printer);
}

void StatementBlockNode::PrintChildren(Printer* printer)
{
	block->Print(printer);
}

void AssignmentStatementNode::PrintChildren(Printer* printer)
{
	member->Print(printer);
	value->Print(printer);
}

void PrintStatementNode::PrintChildren(Printer* printer)
{
	arguments->Print(printer);
}

void IfStatementNode::PrintChildren(Printer* printer)
{
	printer->Print("<Condition>\n");
	printer->Indent();
	booleanExpression->Print(printer);
	printer->Unindent();
	printer->Print("</Condition>\n");
	printer->Print("<TrueCase>\n");
	printer->Indent();
	trueCase->Print(printer);
	printer->Unindent();
	printer->Print("</TrueCase>\n");
	if (falseCase)
	{
		printer->Print("<FalseCase>\n");
		printer->Indent();
		falseCase->Print(printer);
		printer->Unindent();
		printer->Print("</FalseCase>\n");
	}
}

void WhileStatementNode::PrintChildren(Printer* printer)
{
	value->Print(printer);
	loop->Print(printer);
}

void ReturnStatementNode::PrintChildren(Printer* printer)
{
	if (returnValue)
		returnValue->Print(printer);
}

void VariableNode::BeginPrintNode(Printer* printer)
{
	printer->Print("<%s name=\"%s\">\n", nodeName, name->Value);
}

void VariableNode::PrintChildren(Printer* printer)
{
	type->Print(printer);
}

void VariableNodes::PrintChildren(Printer* printer)
{
	PrintNodes(printer);
}

void MethodBodyNode::PrintChildren(Printer* printer)
{
	if (variables)
		variables->Print(printer);
	if (statements)
		statements->Print(printer);
}

void ParameterNode::BeginPrintNode(Printer* printer)
{
	printer->Print("<%s name=\"%s\">\n", nodeName, name->Value);
}

void ParameterNode::PrintChildren(Printer* printer)
{
	type->Print(printer);
}

void ParameterNodes::PrintChildren(Printer* printer)
{
	PrintNodes(printer);
}

void FieldNode::BeginPrintNode(Printer* printer)
{
	printer->Print("<%s name=\"%s\">\n", nodeName, name->Value);
}

void FieldNode::PrintChildren(Printer* printer)
{
	type->Print(printer);
}

void FieldNodes::PrintChildren(Printer* printer)
{
	PrintNodes(printer);
}

void MethodNode::BeginPrintNode(Printer* printer)
{
	printer->Print("<%s name=\"%s\">\n", nodeName, name->Value);
}

void MethodNode::PrintChildren(Printer* printer)
{
	type->Print(printer);
	parameters->Print(printer);
	body->Print(printer);
}

void MethodNodes::PrintChildren(Printer* printer)
{
	PrintNodes(printer);
}

void ClassBodyNode::PrintChildren(Printer* printer)
{
	if (fields)
		fields->Print(printer);
	if (methods)
		methods->Print(printer);
}

void ClassNode::BeginPrintNode(Printer* printer)
{
	printer->Print("<%s name=\"%s\">\n", nodeName, name->Value);
}

void ClassNode::PrintChildren(Printer* printer)
{
	body->Print(printer);
}

void ClassNodes::PrintChildren(Printer* printer)
{
	PrintNodes(printer);
}

void ProgramNode::PrintChildren(Printer* printer)
{
	classes->Print(printer);
}


void ArgumentNodes::PrintChildren(Printer* printer)
{
	PrintNodes(printer);
}

void MethodCallNode::PrintChildren(Printer* printer)
{
	PrintMethodCallChildren(printer);
}

void MethodCallStatementNode::PrintChildren(Printer* printer)
{
	PrintMethodCallChildren(printer);
}

    void MethodCall::PrintMethodCallChildren(Printer* printer)
    {
        member->Print(printer);
        arguments->Print(printer);
    }


