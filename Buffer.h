

#include <vector>

/////////
// The Buffer class
//   this class buffers the data
//   and then allows the data to be
//   read and allows the data to be
//   retrieved as lines.
class Buffer
{
protected:
	typedef std::vector<char*> Queue;
	// the lines of data
	Queue source;
	// a pointer current character
	char* currentString;
	// the current Line
	char* backupCurrentString;
	// the number of lines read
	unsigned int readCount;
	// move to the next string
	void GetNextString();
public:
	// Creates the buffer
	Buffer();
	// destoys the buffer
	~Buffer();
	// reads from the buffer
	int Read(char* buf, int maxSize);
	// loads the buffer with data from stdin
	void GetStdIn();
	// loads the buffer with data from a file
	void GetFile(const char* filename);
	// gets the current line
	const char* GetLine();
	// gets a line
	const char* GetLine(int line);
};
