%{

#include <stdio.h>
#include "lexer.h"
#include "ErrorManager.h"

///////////////////;/////
//	LexerState
// this is the class that holds and manages the state
// for the lexer
class LexerState
{
protected:
	// the following two lines are the current
	// line and char counts for the lexer
	int newCharCount;
	int newLineCount;

	ErrorManager* errorManager;
	
	// these to variables control whether the lexer
	// prtints lexical information, and whether it
	// prints error messages
	bool printLex;
	bool printErrors;

public:
	Buffer* buffer;
	
	// the following two lines are the previous
	// line and char counts for the lexer, since
	// the parser uses one symbol of look ahead,
	// this is especially important
	int CharCount;
	int LineCount;

	// initializes the lexer
	LexerState()
	{
		LineCount = newLineCount = 1;
		CharCount = newCharCount = 1;
		
		printLex = false;
		printErrors = true;
	}

	// UpdateCharCount updates the character count
	//	Parameters:
	//		chars is the number of characters the lexer has seen
	void UpdateCharCount(int chars)
	{
		CharCount = newCharCount;
		LineCount = newLineCount;
		newCharCount += chars;
	}

	// tells the line counter that a new line has been seen
	void NewLine()
	{
		CharCount = newCharCount;
		LineCount = newLineCount;
		newLineCount += 1;
		newCharCount = 1;
	}

	// Token recognition handler declarations
	//	See the function definitions for more information.
	int KeywordHandler(int token, const char* message);
	int NumberHandler();
	int IdentifierHandler();
	
	// handles when the lexer has seen some form of invalid input
	void InvalidCharacterHandler();

	// used by token handlers to print token info
	void PrintTokenInfo(const char* description, int token, const char* extra);
	// This method handles all error printing
	//  TODO: this method and the error count metthods should be put
	//        into a seperate class, because they are used everywhere
	//        not used in the lexer
	void PrintError(const char* message);

	void Initialize(ErrorManager* errorManager, Buffer* buffer);
	
} state;

// redirects input requests to the buffer
#undef YY_INPUT
#define YY_INPUT(buf, result, max) result = state.buffer->Read(buf, 1)

%}

 /* The regular expression definitions each of these corresponds
 	to a valid type of input. These definitions make the rule
	selection easier to read and the definitions are ordered almost
	the same as they appear in lexer.h*/
WhiteSpace	[ \t]
NewLine		[\n]
Comment		"//"[^\n]*\n

Class		"class"
Else		"else"
If			"if"
Int			"int"
New			"new"
Null		"null"
Print		"print"
Read		"read"
Return		"return"
This		"this"
Void		"void"
While		"while"

LeftArray	"["
RightArray	"]"
LeftCurly	"{"
RightCurly	"}"
LeftParen	"("
RightParen	")"

IsNotEqual	"!="
IsEqual		"=="
IsLessThanOrEqual		"<="
IsGreaterThanOrEqual	">="
IsLessThan		"<"
IsGreaterThan	">"

LogicalAnd	"&&"
LogicalOr	"||"
LogicalNot	"!"

OpPlus		"+"
OpMinus		"-"
OpMultiply	"*"
OpDivide	"/"
OpModulus	"%"

SemiColon	";"
Comma		","

SetEqual	"="

MemberOf	"."

Number		(0x)?[0-9]+
Identifier	[a-zA-Z][_a-zA-Z0-9]*

AnyChar		(.)

%%
 /*  The Rule section of the file, contians all the lexer rules
 	each rule calles the function that handles that kind of input
	this way the rule section is not cluttered with C code when
	the main point of the rule section is to define the rules.*/
 
 /* the quit command, this command is only used for testing */
 /*"quit"			{*/
 /*					return 0;*/
 /*				}*/
 

 /* Remove Whitespace */
{WhiteSpace}+	{	state.UpdateCharCount(yyleng); }

 /* Remove NewLines and Comments */
{NewLine}|{Comment}	{	state.NewLine(); }

 /* Handle Keywords */ 
{Class}			{	return state.KeywordHandler(_class,	"Class"); }
{Else}			{	return state.KeywordHandler(_else,	"Else"); }
{If}			{	return state.KeywordHandler(_if,	"If"); }
{Int}			{	return state.KeywordHandler(_int,	"Int"); }
{New}			{	return state.KeywordHandler(_new,	"New"); }
{Null}			{	return state.KeywordHandler(_null,	"Null"); }
{Print}			{	return state.KeywordHandler(_print,	"Print"); }
{Read}			{	return state.KeywordHandler(_readfun,	"Read"); }
{Return}		{	return state.KeywordHandler(_return,	"Return"); }
{This}			{	return state.KeywordHandler(_this,	"This"); }
{Void}			{	return state.KeywordHandler(_void,	"Void"); }
{While}			{	return state.KeywordHandler(_while,	"While"); }

 /* Handle other reserved charcters */
{LeftArray}		{	return state.KeywordHandler(LeftArray,	"LeftArray"); }
{RightArray}	{	return state.KeywordHandler(RightArray,	"RightArray"); }
{LeftCurly}		{	return state.KeywordHandler(LeftCurly,	"LeftCurly"); }
{RightCurly}	{	return state.KeywordHandler(RightCurly,	"RightCurly"); }
{LeftParen}		{	return state.KeywordHandler(LeftParen,	"LeftParen"); }
{RightParen}	{	return state.KeywordHandler(RightParen,	"RightParen"); }

{IsNotEqual}	{	return state.KeywordHandler(IsNotEqual,	"IsNotEqual"); }
{IsEqual}		{	return state.KeywordHandler(IsEqual,	"IsEqual"); }
{IsLessThan}		{	return state.KeywordHandler(IsLessThan,	"IsLessThan"); }
{IsGreaterThan}		{	return state.KeywordHandler(IsGreaterThan,	"IsGreaterThan"); }
{IsLessThanOrEqual}		{	return state.KeywordHandler(IsLessThanOrEqual,	"IsLessThanOrEqual"); }
{IsGreaterThanOrEqual}	{	return state.KeywordHandler(IsGreaterThanOrEqual,	"IsGreaterThanOrEqual"); }

{LogicalAnd}	{	return state.KeywordHandler(LogicalAnd,	"LogicalAnd"); }
{LogicalOr}		{	return state.KeywordHandler(LogicalOr,	"LogicalOr"); }
{LogicalNot}	{	return state.KeywordHandler(LogicalNot,	"LogicalNot"); }

{OpPlus}		{	return state.KeywordHandler(OpPlus,		"OpPlus"); }
{OpMinus}		{	return state.KeywordHandler(OpMinus,	"OpMinus"); }
{OpMultiply}	{	return state.KeywordHandler(OpMultiply,	"OpMultiply"); }
{OpDivide}		{	return state.KeywordHandler(OpDivide,	"OpDivide"); }
{OpModulus}		{	return state.KeywordHandler(OpModulus,	"OpModulus"); }

{SemiColon}		{	return state.KeywordHandler(SemiColon,	"SemiColon"); }
{Comma}			{	return state.KeywordHandler(Comma,		"Comma"); }

{SetEqual}		{	return state.KeywordHandler(SetEqual,	"SetEqual"); }

{MemberOf}		{	return state.KeywordHandler(MemberOf,	"MemberOf"); }

 /* Handle Numbers */
{Number}		{	return state.NumberHandler(); }

 /* Handle Identifiers */
{Identifier}	{	return state.IdentifierHandler(); }

 /* Error on any other character */
{AnyChar}		{	state.InvalidCharacterHandler(); }

%%
//};

// Processes a keyword recognition
//	Parameters:
//		token is the id of the recognized token
//		message is the message to be displayed to the user
//	Returns the Keyword Token value
int LexerState::KeywordHandler(int token, const char* message)
{
	UpdateCharCount(yyleng); 
	yylval.token = new Token(token, CharCount, LineCount);
	PrintTokenInfo(message, Number, "");
	return token;
}

// Processes a Number recognition
//	Returns the Number Token value
int LexerState::NumberHandler()
{
	UpdateCharCount(yyleng); 
	yylval.number = new NumberToken(yytext, Number, CharCount, LineCount);
	PrintTokenInfo("Number Value:", Number, yytext);
	return Number;
}

// Processes an Identifieir recognition
//	Returns the Identifier Token value
int LexerState::IdentifierHandler()
{
	UpdateCharCount(yyleng); 
	yylval.identifier = new IdentifierToken(yytext, Identifier, CharCount, LineCount);
	PrintTokenInfo("Identifier Value:", Identifier, yytext);
	return Identifier;
}

// Processes an Invalid Character recognition
void LexerState::InvalidCharacterHandler()
{
	UpdateCharCount(yyleng); 
	char temp[128];
	snprintf(temp, 128, "(%2d,%2d) - Ignoring Invalid Character: %s\n", CharCount, LineCount, yytext);
	PrintError(temp);
}
	
// prints out token informaiton
void LexerState::PrintTokenInfo(const char* description, int token, const char* extra)
{
	if (printLex)
		printf("(%2d,%2d) - %8d - %s %s\n", CharCount, LineCount, token, description, extra);
}
	
// Prints an error message with the aproximate
//  line the error appears on, and the line and
//  the line before and after the line
void LexerState::PrintError(const char* message)
{
	errorManager->PrintError(message, LineCount-1);
}

// This function is here so we don't have to 
//	link in the lex library
int yywrap()
{
	return 1;
}

// lex and yacc error handler
void yyerror(const char* err)
{
	state.PrintError(err);
}

void LexerState::Initialize(ErrorManager* errorManager, Buffer* buffer)
{
	this->errorManager = errorManager;
	this->buffer = buffer;
}

// tells the buffer to read all data in from stdin
void InitializeLexer(ErrorManager* errorManager, Buffer* buffer)
{
	state.Initialize(errorManager, buffer);
}
