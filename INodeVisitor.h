
#ifndef _INODE_VISITOR
#define _INODE_VISITOR

#include "TreeNode.h"
#include "StateInfo.h"
#include "ErrorManager.h"

// Predeclare TypeNode so we can refer to it
class TypeNode;


// VisitorState is the Symbolic information
// manager. It contains each class and the
// information for each class, along with
// working symbol information
// See VisitorUtility.cpp for information about each method
class VisitorState
{
		// All protected information is for use with the State's
		// stack ability, when the state is pushed a new Raw
		// State is created as a copy of the current state.
		// when the state is poped the previous state is restored
protected:

	class RawVisitorState
	{
	public:
		ProgramInfo* Program;
		TypeInfo* CurrentClass;
		MethodInfo* CurrentMethod;
	
		TypeInfo* WorkingClass;
		MethodInfo* WorkingMethod;
		VariableInfo* WorkingVariable;
	};

	std::vector<RawVisitorState*> stateStack;
	
	RawVisitorState* Clone();
	void Unclone(RawVisitorState* state);
public:
	VisitorState();
	virtual ~VisitorState();

	// The Program info
	ProgramInfo* Program;
	// The current class we are visiting
	TypeInfo* CurrentClass;
	// The current method we are visitiing
	MethodInfo* CurrentMethod;

	
	// A temporary Class pointer
	TypeInfo* WorkingClass;
	// a temporary Method pointer
	MethodInfo* WorkingMethod;
	// a temporary Variable pointer
	VariableInfo* WorkingVariable;

	// The Error Manager for use by the State and
	// the visitor that uses the state.
	ErrorManager* Errors;

	TypeInfo* ResolveType(const TypeNode* typeNode);
	TypeInfo* ResolveType(const char* typeName);
	TypeInfo* ResolveArrayAccess(const TypeInfo* arrayType, int);
	TypeInfo* ResolveArrayType(const TypeNode* typeNode);
	VariableInfo* ResolveVariable(const char* typeName);
	void PushState();
	void PopState();
};


// The INodeVisitor Abstract class provides the functionality
// for the visitor design pattern to work. the XML printer, 
// all three analizers and the codegen phase all implement
// this class.
struct INodeVisitor
{
	virtual ~INodeVisitor() {};
	virtual void Visit(ProgramNode* Program) = 0;
	virtual void Visit(ClassNode* Class) = 0;
	virtual void Visit(FieldNode* Field) = 0;
	virtual void Visit(MethodNode* Method) = 0;
	virtual void Visit(ParameterNode* Parameter) = 0;
	virtual void Visit(MethodBodyNode* MethodBody) = 0;
	virtual void Visit(VariableNode* Variable) = 0;
	virtual void Visit(AssignmentStatementNode* Assignment) = 0;
	virtual void Visit(PrintStatementNode* Print) = 0;
	virtual void Visit(IfStatementNode* If) = 0;
	virtual void Visit(WhileStatementNode* While) = 0;
	virtual void Visit(ReturnStatementNode* Return) = 0;
	virtual void Visit(MethodCallStatementNode* MethodCall) = 0;
	virtual void Visit(MethodCallNode* MethodCall) = 0;
	virtual void Visit(NoOpStatementNode* NoOp) = 0;
	virtual void Visit(IsEqualNode* IsEqual) = 0;
	virtual void Visit(IsNotEqualNode* IsNotEqual) = 0;
	virtual void Visit(IsLessThanNode* IsLessThan) = 0;
	virtual void Visit(IsGreaterThanNode* IsGreaterThan) = 0;
	virtual void Visit(IsLessThanOrEqualNode* IsLessThanOrEqual) = 0;
	virtual void Visit(IsGreaterThanOrEqualNode* IsGreaterThanOrEqual) = 0;
	virtual void Visit(ConstructorCallNode* ConstructorCall) = 0;
	virtual void Visit(ArrayConstructorCallNode* ArrayConsturctorCall) = 0;
	virtual void Visit(AdditionNode* Addition) = 0;
	virtual void Visit(SubtractionNode* Subtraction) = 0;
	virtual void Visit(LogicalOrNode* LogicalOr) = 0;
	virtual void Visit(MultiplyNode* Multiply) = 0;
	virtual void Visit(DivideNode* Divide) = 0;
	virtual void Visit(ModulusNode* Modulus) = 0;
	virtual void Visit(LogicalAndNode* LogicalAnd) = 0;
	virtual void Visit(ReadNode* Read) = 0;
	virtual void Visit(NullNode* Null) = 0;
	virtual void Visit(VariableAccessFactorNode* VariableAccess) = 0;
	virtual void Visit(NumberNode* Number) = 0;
	virtual void Visit(UnaryMinusNode* UnaryMinus) = 0;
	virtual void Visit(LogicalNotNode* LogicalNot) = 0;
	virtual void Visit(ArgumentNode* Argument) = 0;
	virtual void Visit(ThisAccessNode* This) = 0;
	virtual void Visit(ForeignMemberAccessNode* MemberAccess) = 0;
	virtual void Visit(MemberAccessNode* MemberAccess) = 0;
	virtual void Visit(ForeignMemberArrayAccessNode* MemberAccess) = 0;
	virtual void Visit(MemberArrayAccessNode* MemberAccess) = 0;
	virtual void Visit(TypeNode* Type) = 0;
	virtual void Visit(ArrayTypeNode* Array) = 0;


	// The Visitor's State information
	VisitorState State;
};


#endif // _INODE_VISITOR
