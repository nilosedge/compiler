
#ifndef _ERROR_MANAGER_H
#define _ERROR_MANAGER_H

#include "Buffer.h"
#include <iostream>

class ErrorManager
{
protected:
	int errors;
	int maxErrors;
	std::ostream* output;
	Buffer* buffer;

	void TooManyErrors();
	bool IncrementErrorCount();
public:
	ErrorManager(std::ostream* output, Buffer* buffer, int maxErrors)
		: errors(0), maxErrors(maxErrors), output(output), buffer(buffer)
	{
	}
	int GetErrorCount();
	void PrintError(const char* message);
	void PrintError(const char* message, int line, bool printLines = true);
};


#endif
