

#if !defined(BINARYTREE_H)
#define BINARYTREE_H

#include "Hasher.h"


namespace Hashing
{
		// This function is provided so we 
		// can have a default do nothing
		// memory Freer
	template<class T>
	inline void ValueDeleter(T)
	{
	}

	// The BinaryTree class is a simple binary tree that
	// takes four parameters
	// 	Value : the type that the Tree holds as it's data
	// 	Key : the type that will be used for the keys
	// 	Hasher : Hasher is a class we the static methods
	// 		that the BinaryTree class uses for allocation
	// 		and deallocation of Keys. See StringHasher
	// 	Deleter : a function pointer used to dealocate
	// 		Values
	template
		<
			class Value = int,
			class Key = String,
			class Hasher = StringHasher,
			void (*Deleter)(Value) = ValueDeleter<Value>
		>
	class BinaryTree
	{
		typedef BinaryTree<Value,Key,Hasher,Deleter> TreeNode;
		Key key;
		Value value;
		TreeNode* left;
		TreeNode* right;

	public:

		// Creates a new Binary Tree Node
		inline BinaryTree(Key key, Value value)
		{
			this->key = Hasher::Clone(key);
			this->value = value;
			this->left = NULL;
			this->right = NULL;
		}

		// Deletes the Node and frees any memory necesary
		~BinaryTree()
		{
			Hasher::Delete(key);
			Deleter(value);
			if (left)
				delete left;
			if (right)
				delete right;
		}

		// This isn't ever used
		void* GetHandle()
		{
			return (void*) this;
		}


		// Inserts a new key value pair into the tree
		inline void Add(Key key, Value value)
		{
			int compare = Hasher::Compare(this->key, key);

			if (compare == 0)
			{
				printf("*** Error *** Identifier already in the symbol table\n");
				return;
			}

			if (compare < 0)
			{
				if (left)
					left->Add(key, value);
				else
					left = new TreeNode(key, value);
			}
			else
			{
				if (right)
					right->Add(key, value);
				else
					right = new TreeNode(key, value);
			}
		}

		// It turns out that this method is never used and so it was
		// never written
		inline bool Remove(void* handle)
		{
			return false;
		}

		// It turns out that this method is never used and so it was
		// never written
		inline bool Remove(Key key)
		{
			return false;
		}



		// Looks up the key in the tree and if the key is in the tree
		// it will give the value in the pValue variable. this emthod
		// returns true or false depending on whether the key was in
		// the tree
		inline bool Lookup(Key key, Value* pValue)
		{
			int compare = Hasher::Compare(this->key, key);
			if (compare == 0)
			{
				if (pValue)
					*pValue = value;
			    return true;
			}

			if (compare < 0 && left)
				return left->Lookup(key, pValue);

			if (right)
				return right->Lookup(key, pValue);

			return false;
		}


		// Prints the Key for the tree.
		inline void PrintKeys()
		{
			std::cout << this->key << std::endl;
			if (left)
				left->PrintKeys();
			if (right)
				right->PrintKeys();
		}

	};

}


#endif // HASHTABLE_H
