
#ifndef LEXER_H
#define LEXER_H

#include "TreeNode.h"
#include "parser.cpp.h"

//	The Token enumeration
//	it contains a value for ever token in Decaf
/*
enum Token
{
	_class = 100,
	_else,
	_if,
	_int,
	_new,
	_null,
	_print,
	_readfun,
	_return,
	_this,
	_void,
	_while,

	LeftArray = 200,
	RightArray,
	LeftCurly,
	RightCurly,
	LeftParen,
	RightParen,

	IsNotEqual = 300,
	IsEqual,
	IsLessThan,
	IsGreaterThan,
	IsLessThanOrEqual,
	IsGreaterThanOrEqual,

	LogicalAnd = 400,
	LogicalOr,
	LogicalNot,

	OpPlus = 500,
	OpMinus,
	OpMultiply,
	OpDivide,
	OpModulus,

	SemiColon = 600,
	Comma,

	SetEqual = 700,

	MemberOf = 800,

	Number = 900,
	Identifier
};
*/

#endif // LEXER_H
