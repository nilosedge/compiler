//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This header file declares our CodeGen Class to be used in the CodeGen.cpp file, it includes all the functions as detailed down below
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _INODE_VISITOR_CodeGen
#define _INODE_VISITOR_CodeGen

#include "TreeNode.h"
#include "INodeVisitor.h"
#include <iostream>
#include <fstream>
#include "Printer.h"

class CodeGen : public  INodeVisitor
{
	Printer printer;
public:
	int LabelCounter;
	int OffSet;
	int ParamCount;
	CodeGen(const char *file) : printer(new std::ofstream(file, std::ios::out))
	{
		ParamCount = OffSet = LabelCounter = 0;
	}
	CodeGen() : printer(&std::cout)
	{
		ParamCount = OffSet = LabelCounter = 0;
	};
	virtual ~CodeGen()
	{
	};
	void Visit(ProgramNode* Program);
	void Visit(ClassNode* Class);
	void Visit(FieldNode* Field);
	void Visit(MethodNode* Method);
	void Visit(ParameterNode* Parameter);
	void Visit(MethodBodyNode* MethodBody);
	void Visit(VariableNode* Variable);
	void Visit(AssignmentStatementNode* Assignment);
	void Visit(PrintStatementNode* Print);
	void Visit(IfStatementNode* If);
	void Visit(WhileStatementNode* While);
	void Visit(ReturnStatementNode* Return);
	void Visit(MethodCallStatementNode* MethodCall);
	void Visit(MethodCallNode* MethodCall);
	void Visit(NoOpStatementNode* NoOp);
	void Visit(IsEqualNode* IsEqual);
	void Visit(IsNotEqualNode* IsNotEqual);
	void Visit(IsLessThanNode* IsLessThan);
	void Visit(IsGreaterThanNode* IsGreaterThan);
	void Visit(IsLessThanOrEqualNode* IsLessThanOrEqual);
	void Visit(IsGreaterThanOrEqualNode* IsGreaterThanOrEqual);
	void Visit(ConstructorCallNode* ConstructorCall);
	void Visit(ArrayConstructorCallNode* ArrayConsturctorCall);
	void Visit(AdditionNode* Addition);
	void Visit(SubtractionNode* Subtraction);
	void Visit(LogicalOrNode* LogicalOr);
	void Visit(MultiplyNode* Multiply);
	void Visit(DivideNode* Divide);
	void Visit(ModulusNode* Modulus);
	void Visit(LogicalAndNode* LogicalAnd);
	void Visit(ReadNode* Read);
	void Visit(NullNode* Null);
	void Visit(VariableAccessFactorNode* VariableAccess);
	void Visit(NumberNode* Number);
	void Visit(UnaryMinusNode* UnaryMinus);
	void Visit(LogicalNotNode* LogicalNot);
	void Visit(ArgumentNode* Argument);
	void Visit(ThisAccessNode* This);
	void Visit(ForeignMemberAccessNode* MemberAccess);
	void Visit(MemberAccessNode* MemberAccess);
	void Visit(ForeignMemberArrayAccessNode* MemberAccess);
	void Visit(MemberArrayAccessNode* MemberAccess);
	void Visit(TypeNode* Type);
	void Visit(ArrayTypeNode* Array);

	///////////////////////////////////////////////
////////////////////////////////////////////////////
	/// Other functions not dealing with the visitor.
	int GetLabelCount();
	void EmitHeaderCode();
	void EmitExceptionCode();
	void EmitPrintDefinedString(char* stringName); 
	void EmitProcCall(char *);
	void EmitPrintCode();
	void EmitPrint(char* reg);
	void PrintReg(char* reg);
	void EmitPop(char* reg);
	void EmitPush(char* reg);
	void EmitBinaryTermCode(char* command, char* comment, BinaryTermNode* bse);
	void EmitBinarySubExp(char* command, char* comment, BinarySubExpressionNode* bse);
	void EmitBinRelExp(char* command, char* comment, BinaryRelationalExpressionNode* bre);

	void IncOffSet();
	void DecOffSet();
	//void EmitGetStackSpace();
	void EmitAloc(bool SetSize=false);
	
};


#endif // _INODE_VISITOR_CodeGen
