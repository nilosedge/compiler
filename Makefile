#  Makefile for Project 1 
#  Based on a Make File by
#      Rick Halterman
#

LEX=flex
YACC=bison
CC=g++

LEX_FLAGS=
YACC_FLAGS=--verbose --defines
FLAGS=-Wall -g # -fno-implicit-templates
OP=

all: decaf

rungood: decaf
	./decaf < sample_code/good/test.decaf

runbad: decaf
	./decaf < sample_code/bad/test3.decaf

run: decaf
	./decaf < test.decaf

test: decaf
	gdb --command=debug.cmd decaf

decaf: lexer.o parser.o TreeNodePrinting.o ParseTreeBuilder.o TreeNodeConstructors.o main.o Buffer.o TreeNodeAcceptors.o PrintTest.o CodeGen.o VisitorUtility.o AlphaAnalizer.o BetaAnalizer.o GammaAnalizer.o ErrorManager.o
	${CC} ${FLAGS} ${OP} -o decaf lexer.o parser.o TreeNodePrinting.o ParseTreeBuilder.o TreeNodeConstructors.o main.o Buffer.o TreeNodeAcceptors.o PrintTest.o CodeGen.o VisitorUtility.o AlphaAnalizer.o BetaAnalizer.o GammaAnalizer.o ErrorManager.o

#
#  TreeNode Code
#

ParseTreeBuilder.o: ParseTreeBuilder.cpp TreeNode.h
	${CC} ${FLAGS} ${OP} -c ParseTreeBuilder.cpp 

TreeNodePrinting.o: TreeNodePrinting.cpp TreeNode.h
	${CC} ${FLAGS} ${OP} -c TreeNodePrinting.cpp

TreeNodeConstructors.o: TreeNodeConstructors.cpp TreeNode.h INodeVisitor.h
	${CC} ${FLAGS} ${OP} -c TreeNodeConstructors.cpp 

TreeNodeAcceptors.o: TreeNodeAcceptors.cpp INodeVisitor.h
	${CC} ${FLAGS} ${OP} -c TreeNodeAcceptors.cpp 



#
#  Visitor Code
#

PrintTest.o: PrintTest.cpp INodeVisitor.h HashTable.h BinaryTree.h
	${CC} ${FLAGS} ${OP} -c PrintTest.cpp 

AlphaAnalizer.o: AlphaAnalizer.cpp AlphaAnalizer.h INodeVisitor.h HashTable.h BinaryTree.h
	${CC} ${FLAGS} ${OP} -c AlphaAnalizer.cpp 

BetaAnalizer.o: BetaAnalizer.cpp BetaAnalizer.h INodeVisitor.h HashTable.h BinaryTree.h
	${CC} ${FLAGS} ${OP} -c BetaAnalizer.cpp 

GammaAnalizer.o: GammaAnalizer.cpp GammaAnalizer.h INodeVisitor.h HashTable.h BinaryTree.h
	${CC} ${FLAGS} ${OP} -c GammaAnalizer.cpp 

CodeGen.o: CodeGen.cpp INodeVisitor.h HashTable.h BinaryTree.h
	${CC} ${FLAGS} ${OP} -c CodeGen.cpp 




#
#  Utility Code
#

ErrorManager.o: ErrorManager.cpp
	${CC} ${FLAGS} ${OP} -c ErrorManager.cpp

VisitorUtility.o: VisitorUtility.cpp HashTable.h BinaryTree.h
	${CC} ${FLAGS} ${OP} -c VisitorUtility.cpp 

Buffer.o: Buffer.cpp
	${CC} ${FLAGS} ${OP} -c Buffer.cpp 

main.o: main.cpp INodeVisitor.h HashTable.h BinaryTree.h
	${CC} ${FLAGS} ${OP} -c main.cpp 




#
#  Lexer and parser compilation
#

lexer.o: lexer.cpp TreeNode.h
	${CC} ${FLAGS} ${OP} -c lexer.cpp 

parser.o: parser.cpp TreeNode.h
	${CC} ${FLAGS} ${OP} -c parser.cpp 

lexer.cpp: lexer.l parser.cpp
	${LEX} ${LEX_FLAGS} -olexer.cpp lexer.l

parser.cpp: parser.y
	${YACC} ${YACC_FLAGS} -o parser.cpp parser.y
	./copy.pl

edit:
	vim lexer.l

wc: clean
	wc *.cpp *.h *.cpph *.th *.l *.y *.pl *.vim *.cmd Makefile README sample_code/good/*.decaf sample_code/bad/*.decaf sample_code/bad/SemanticChecks/*.decaf   


st: test.asm
	spim -file test.asm

clean: distclean
	/bin/rm -f decaf

distclean:
	/bin/rm -f *.o core lexer.cpp parser.cpp parser.cpp.output temp lexer gmon.out parser.cpp.h parser.output *.asm
