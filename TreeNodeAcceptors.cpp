#include "TreeNode.h"
#include "INodeVisitor.h"


void TypeNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void ArrayTypeNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void NameWithTypeNode::Accept(INodeVisitor* visitor)
{
	// Do nothing,because this class is not used in the AST
	//visitor->Visit(this);
}

void NameWithReturnTypeNode::Accept(INodeVisitor* visitor)
{
	// Do nothing,because this class is not used in the AST
	//visitor->Visit(this);
}

void ForeignMemberAccessNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void MemberAccessNode::Accept(INodeVisitor* visitor)
{
	//visitor->State.PushState();
	
	//Variable = visitor->State.ResolveVariable(name->Value);
	//visitor->State.WorkingVariable = Variable;
	//if (Variable)
		//visitor->State.WorkingClass = Variable->Type;
	//else
		//visitor->State.WorkingClass = 0;
	
	visitor->Visit(this);
	
	//visitor->State.PopState();
}

void ThisAccessNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void ForeignMemberArrayAccessNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void MemberArrayAccessNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void ArgumentNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void ArgumentNodes::Accept(INodeVisitor* visitor)
{
	AcceptNodes(visitor);
}

void ArgumentNodes::AcceptReverse(INodeVisitor* visitor)
{
	AcceptNodesReverse(visitor);
}

void UnaryMinusNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void LogicalNotNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void ParenExpressionNode::Accept(INodeVisitor* visitor)
{
	this->value->Accept(visitor);
	this->ReturnType = value->ReturnType;
}

void ReadNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void MethodCallNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void NullNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void VariableAccessFactorNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void NumberNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void MultiplyNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void DivideNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void ModulusNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void LogicalAndNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void AdditionNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void SubtractionNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void LogicalOrNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void IsEqualNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void IsNotEqualNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void IsLessThanNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void IsGreaterThanNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void IsLessThanOrEqualNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void IsGreaterThanOrEqualNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void ConstructorCallNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void ArrayConstructorCallNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void StatementNode::Accept(INodeVisitor* visitor)
{
	//visitor->Visit(this);
}

void StatementNodes::Accept(INodeVisitor* visitor)
{
	AcceptNodes(visitor);
}

void StatementBlockNode::Accept(INodeVisitor* visitor)
{
	block->Accept(visitor);
}

void AssignmentStatementNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void PrintStatementNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void IfStatementNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void WhileStatementNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void ReturnStatementNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void MethodCallStatementNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void NoOpStatementNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void VariableNode::Accept(INodeVisitor* visitor)
{
	visitor->State.PushState();
	visitor->State.WorkingVariable = this;
	visitor->Visit(this);
	visitor->State.PopState();
}

void VariableNodes::Accept(INodeVisitor* visitor)
{
	AcceptNodes(visitor);
}

void MethodBodyNode::Accept(INodeVisitor* visitor)
{
	visitor->Visit(this);
}

void ParameterNode::Accept(INodeVisitor* visitor)
{
	visitor->State.PushState();
	visitor->State.WorkingVariable = this;
	visitor->Visit(this);
	visitor->State.PopState();
}

void ParameterNodes::Accept(INodeVisitor* visitor)
{
	AcceptNodes(visitor);
}

void FieldNode::Accept(INodeVisitor* visitor)
{
	visitor->State.PushState();
	visitor->State.WorkingVariable = this;
	visitor->Visit(this);
	visitor->State.PopState();
}

void FieldNodes::Accept(INodeVisitor* visitor)
{
	AcceptNodes(visitor);
}

void MethodNode::Accept(INodeVisitor* visitor)
{
	visitor->State.PushState();
	visitor->State.CurrentMethod = this;
	visitor->State.WorkingMethod = this;
	visitor->Visit(this);
	visitor->State.PopState();
}

void MethodNodes::Accept(INodeVisitor* visitor)
{
	AcceptNodes(visitor);
}

void ClassBodyNode::Accept(INodeVisitor* visitor)
{
	//visitor->Visit(this);
}

void ClassNode::Accept(INodeVisitor* visitor)
{
	visitor->State.PushState();
	visitor->State.CurrentClass = this;
	visitor->State.WorkingClass = this;
	visitor->Visit(this);
	visitor->State.PopState();
}

void ClassNodes::Accept(INodeVisitor* visitor)
{
	AcceptNodes(visitor);
}

void ProgramNode::Accept(INodeVisitor* visitor)
{
	visitor->State.Program = this;
	visitor->Visit(this);
}

