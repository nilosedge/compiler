// BetaAnalizer.cpp: 
// Populate all symbol tables.
// After this state all declaration errors for Fields/Methods/Variables should
// be caught. NOTE: This does not extend to methods declared to return one type
// but returning another.  That will be caught in Gamma.

#include "BetaAnalizer.h"


// Make sure that at least one class has a 'main' method.
void BetaAnalizer::Visit(ProgramNode* Program)
{
	Program->classes->Accept(this);
	if(!State.Program->MainClass) {
		State.Errors->PrintError("No \'main\' Method defined in any class.");
	}
}

// Visit fields and methods if they exist
// Also we set up the offset for each class so the CodeGen people are happy
void BetaAnalizer::Visit(ClassNode* Class)
{
	OffSet = 0;
	if (Class->body->fields) Class->body->fields->Accept(this);
	if (Class->body->methods) Class->body->methods->Accept(this);

	if(Class->body->fields) {
		State.CurrentClass->Size = 4 * Class->body->fields->GetLength();
	} else State.CurrentClass->Size = 0;
}

// Discover the type of the field and then add its information
// to the enclosing class.
void BetaAnalizer::Visit(FieldNode* Field)
{
	TypeInfo* type = State.ResolveType(Field->type);
	if(type != NULL) {
		State.WorkingVariable->Type = type;
	}

	State.WorkingVariable->Name = Field->name->Value;

	VariableInfo* FirstDec = NULL;

	if(State.CurrentClass->Fields.Lookup(State.WorkingVariable->Name, &FirstDec)) {
		char message[160];
		snprintf(message, 160, "Duplicate field declaration \'%*s\'",  strlen(State.WorkingVariable->Name),
				 State.WorkingVariable->Name);
		State.Errors->PrintError(message, Field->RowNumber, false);
	} else {
		// Place the correct OffSet into the VariableInfo object for each 
		// class variable.  
		// done by Nilo
		State.WorkingVariable->Offset = OffSet;
		State.CurrentClass->Fields.Add(State.WorkingVariable->Name, 
								State.WorkingVariable);
		OffSet += 4;
	}
}

//	Setup the MethodInfo object with name, unique name
void BetaAnalizer::Visit(MethodNode* Method)
{
	TypeInfo* type = State.ResolveType(Method->type);
	
	State.CurrentMethod->ReturnType = type;

	State.CurrentMethod->Name = Method->name->Value;
	sprintf(State.CurrentMethod->UniqueName, "___%s.%s___", State.CurrentClass->Name, State.CurrentMethod->Name);
		
	if (strcmp("main", State.CurrentMethod->Name) == 0)
	{
		if (State.Program->MainClass)
		{
			State.Errors->PrintError("Cannot have more than one main method");
		}
		else
		{
			State.Program->MainClass = State.CurrentClass;
			if (State.CurrentMethod->ReturnType != State.ResolveType("void"))
				State.Errors->PrintError("main must return type void", Method->RowNumber);
		}
	}

	MethodInfo* FirstDec = NULL;

	if(State.CurrentClass->Methods.Lookup(State.CurrentMethod->Name, &FirstDec)) {
		char message[160];
		snprintf(message, 160, "Duplicate method declaration \'%*s\'",
			strlen(State.CurrentMethod->Name), State.CurrentMethod->Name);
		State.Errors->PrintError(message, Method->RowNumber, false);
	} else {
		State.CurrentClass->Methods.Add(State.CurrentMethod->Name, State.CurrentMethod);
	}

	if(Method->parameters) Method->parameters->Accept(this);
	if(Method->body) Method->body->Accept(this);
}

// Verify the correctness of parameter declarations
// Place them in the Method's list for later use
void BetaAnalizer::Visit(ParameterNode* Parameter)
{
	TypeInfo* type = State.ResolveType(Parameter->type);
	if(type != NULL) {
		State.WorkingVariable->Type = type;
	}

	State.WorkingVariable->Name = Parameter->name->Value;

	VariableInfo* FirstDec = NULL;
	if(State.WorkingMethod->Parameters.Lookup(State.WorkingVariable->Name, &FirstDec)) {
		char message[160];
		snprintf(message, 160, "Duplicate parameter declaration \'%*s\'",
			strlen(State.WorkingVariable->Name), State.WorkingVariable->Name);
		State.Errors->PrintError(message, Parameter->RowNumber, false);
	} else {
		State.WorkingMethod->Parameters.Add(State.WorkingVariable->Name, State.WorkingVariable);
		State.WorkingMethod->ParameterList.push_back(State.WorkingVariable);
	}
}

// Do nothing.. only looking for varibles inside.
void BetaAnalizer::Visit(MethodBodyNode* MethodBody)
{
	if(MethodBody->variables) MethodBody->variables->Accept(this);
}

// Verify the correctness of variable declarations
// Place them in the Method's list for later use
void BetaAnalizer::Visit(VariableNode* Variable)
{
	TypeInfo* type = State.ResolveType(Variable->type);
	if(type != NULL) {
		State.WorkingVariable->Type = type;
	}

	State.WorkingVariable->Name = Variable->name->Value;

	VariableInfo* FirstDec = NULL;
	
	if(State.WorkingMethod->Variables.Lookup(State.WorkingVariable->Name, &FirstDec)) {
		char message[160];
		snprintf(message, 160, "Duplicate variable declaration \'%*s\'",
			strlen(State.WorkingVariable->Name), State.WorkingVariable->Name);
		State.Errors->PrintError(message, Variable->RowNumber, false);
	} else {
		State.WorkingMethod->Variables.Add(State.WorkingVariable->Name, State.WorkingVariable);
	}
}

// That's all folks... The rest of the methods are here for 
// interface implementation only
void BetaAnalizer::Visit(MethodCallNode* MethodCall)
{
	
}

void BetaAnalizer::Visit(AssignmentStatementNode* Assignment)
{
	
}

void BetaAnalizer::Visit(PrintStatementNode* Print)
{
	
}

void BetaAnalizer::Visit(IfStatementNode* If)
{
	
}

void BetaAnalizer::Visit(WhileStatementNode* While)
{
	
}

void BetaAnalizer::Visit(ReturnStatementNode* Return)
{
	
}

void BetaAnalizer::Visit(MethodCallStatementNode* MethodCall)
{
	
}

void BetaAnalizer::Visit(NoOpStatementNode* NoOp)
{
	
}

void BetaAnalizer::Visit(IsEqualNode* IsEqual)
{
	
}

void BetaAnalizer::Visit(IsNotEqualNode* IsNotEqual)
{
	
}

void BetaAnalizer::Visit(IsLessThanNode* IsLessThan)
{
	
}

void BetaAnalizer::Visit(IsGreaterThanNode* IsGreaterThan)
{
	
}

void BetaAnalizer::Visit(IsLessThanOrEqualNode* IsLessThanOrEqual)
{
	
}

void BetaAnalizer::Visit(IsGreaterThanOrEqualNode* IsGreaterThanOrEqual)
{
	
}

void BetaAnalizer::Visit(ConstructorCallNode* ConstructorCall)
{
	
}

void BetaAnalizer::Visit(ArrayConstructorCallNode* ArrayConsturctorCall)
{
	
}

void BetaAnalizer::Visit(AdditionNode* Addition)
{
	
}

void BetaAnalizer::Visit(SubtractionNode* Subtraction)
{
	
}

void BetaAnalizer::Visit(LogicalOrNode* LogicalOr)
{
	
}

void BetaAnalizer::Visit(MultiplyNode* Multiply)
{
	
}

void BetaAnalizer::Visit(DivideNode* Divide)
{
	
}

void BetaAnalizer::Visit(ModulusNode* Modulus)
{
	
}

void BetaAnalizer::Visit(LogicalAndNode* LogicalAnd)
{
	
}

void BetaAnalizer::Visit(ReadNode* Read)
{
	
}

void BetaAnalizer::Visit(NullNode* Null)
{
	
}

void BetaAnalizer::Visit(VariableAccessFactorNode* VariableAccess)
{
	
}

void BetaAnalizer::Visit(NumberNode* Number)
{
	
}

void BetaAnalizer::Visit(UnaryMinusNode* UnaryMinus)
{
	
}

void BetaAnalizer::Visit(LogicalNotNode* LogicalNot)
{
	
}

void BetaAnalizer::Visit(ArgumentNode* Argument)
{
	
}

void BetaAnalizer::Visit(ThisAccessNode* This)
{
	
}

void BetaAnalizer::Visit(ForeignMemberAccessNode* MemberAccess)
{
	
}

void BetaAnalizer::Visit(MemberAccessNode* MemberAccess)
{
	
}

void BetaAnalizer::Visit(ForeignMemberArrayAccessNode* MemberAccess)
{
	
}

void BetaAnalizer::Visit(MemberArrayAccessNode* MemberAccess)
{
	
}

void BetaAnalizer::Visit(TypeNode* Type)
{
	
}

void BetaAnalizer::Visit(ArrayTypeNode* Array)
{
	
}

