
#include "INodeVisitor.h"
#include "StateInfo.h"
#include "TreeNode.h"


//TypeTable TypeTableInstance;
//VariableTable VariableTableInstance;
//MethodTable MethodTableInstance;
//VariableList VariableListInstance;


// Set all internal fields to null initially.  These will be set individually 
// in the TreeNodeAcceptors as the visitor progresses.
VisitorState::VisitorState() : Program(0), CurrentClass(0), CurrentMethod(0), WorkingClass(0), WorkingMethod(0), WorkingVariable(0), Errors(0)
{
}

// Nothing to do here since once were done the stack will
// be empty once again.
VisitorState::~VisitorState()
{
}


// Copy the contents of the State into a RawVisitorState object for
// storing on the stack.
VisitorState::RawVisitorState* VisitorState::Clone()
{
	VisitorState::RawVisitorState* state = new VisitorState::RawVisitorState();

	state->Program = Program;
	state->CurrentClass = CurrentClass;
	state->CurrentMethod = CurrentMethod;

	state->WorkingClass = WorkingClass;
	state->WorkingMethod = WorkingMethod;
	state->WorkingVariable = WorkingVariable;

	return state;
}

// Restore the state's contents from a RawVisitorState object
// popped of the stack.
void VisitorState::Unclone(VisitorState::RawVisitorState* state)
{
	Program = state->Program;
	CurrentClass = state->CurrentClass;
	CurrentMethod = state->CurrentMethod;

	WorkingClass = state->WorkingClass;
	WorkingMethod = state->WorkingMethod;
	WorkingVariable = state->WorkingVariable;

	delete state;
}

// Check to see that this is actually an ArrayType, if it is 
// return the type of things stored in the array.
// If not print an error using the linNumber as a reference for printing out
// code.
TypeInfo* VisitorState::ResolveArrayAccess(const TypeInfo* arrayType, int lineNumber)
{

	if (arrayType->SecondaryType)
	{
    	return arrayType->SecondaryType;
	}
	else
	{
    	char message[180];
    	snprintf(message, 180, "Array access on non arrray of type \'%s\'",
        	arrayType->Name);
    	Errors->PrintError(message, lineNumber);

		return 0;
	}
	
}


// Verifies the correctness of an array declaration and then creates
// a typeinfo object for that array.  We use this to correctly set up
// arrays for CodeGen in the GammaAnalizer phase.
TypeInfo* VisitorState::ResolveArrayType(const TypeNode* typeNode)
{
	TypeInfo* type = 0;
	TypeInfo* accessType = 0;
	if (typeNode->typeID == TypeNode::Array)
	{
      const ArrayTypeNode* arrayTypeNode = dynamic_cast<const ArrayTypeNode*>(typeNode);
      accessType = ResolveType(arrayTypeNode->innerType);
	}
	else
	{
		accessType = ResolveType(typeNode);
	}

            if (accessType)
            {
               char name[80];
               snprintf(name, 80, "%s[]", accessType->Name);

               if (!Program->Classes.Lookup(name, &type))
               {
                  // NOTE: this is a memmory leak at the moment I am not
                  //  sure if this new will get deleted
                  char* heapName = new char[80];
                  strcpy(heapName, name);

                  type = new TypeInfo();
                  type->Name = heapName;

                  char* heapLength = new char[10];
                  strcpy(heapLength, "length");
                  VariableInfo* length = new VariableInfo();
                  length->Name = heapLength;
                  length->Offset = 0;
				  length->Type = ResolveType("int");

                  type->Fields.Add("length", length);

				  type->SecondaryType = accessType;

                  Program->Classes.Add(name, type);
               }
            }
            else
            {
               char message[180];
               snprintf(message, 180, "Cannot have an array of a non-declared type \'%s\'",
                  typeNode->typeName->Value);
               Errors->PrintError(message, typeNode->RowNumber);
            }


	return type;

}

// Resolves a typeNode object to a corresponding typeinfo object
// in the global type table.  Note that we use the ResolveArrayType
// method to handle the additional logic required for arrays
TypeInfo* VisitorState::ResolveType(const TypeNode* typeNode)
{
	TypeInfo* type = 0;

	if (!typeNode)
		return ResolveType("void");

	switch (typeNode->typeID)
	{
		case TypeNode::Void:
			Program->Classes.Lookup("void", &type);
			break;
			
		case TypeNode::Array:
			{
				type = ResolveArrayType(typeNode);
			}
			break;
			
		case TypeNode::New:
			Program->Classes.Lookup(typeNode->typeName->Value, &type);
			break;
			
		case TypeNode::Int:
			Program->Classes.Lookup("int", &type);
			break;
	}

	if (!type)
	{
		char message[80];
		snprintf(message, 80, "Undeclared type \'%s\'", typeNode->typeName->Value);
		Errors->PrintError(message, typeNode->typeName->RowNumber);
	}
	
	return type;
}

// Simple function to look up a type fromt the global type table
TypeInfo* VisitorState::ResolveType(const char* typeName)
{
	TypeInfo* type = 0;
	Program->Classes.Lookup(typeName, &type);
	return type;
}

// Resolve a varible name refernece to a varible declaration.
// Look in the right order to maintain scope.
VariableInfo* VisitorState::ResolveVariable(const char* typeName)
{
	VariableInfo* var = 0;

	bool success = false;
	success = CurrentMethod->Variables.Lookup(typeName, &var);

	if (!success) 
		success = CurrentMethod->Parameters.Lookup(typeName, &var);

	if (!success) 
		success = CurrentClass->Fields.Lookup(typeName, &var);

	return var;
}
   

// Use Clone() to place the internal state of the State object into a Stack.
void VisitorState::PushState()
{
	stateStack.push_back(Clone());
}

// Restor the last State from the stack
void VisitorState::PopState()
{
	Unclone(stateStack.back());
	stateStack.pop_back();
}




// Constructor for ProgramInfo.  Sets up the internal hashtable for global types
// and sets all other variables to null
ProgramInfo::ProgramInfo()
	: Classes(41), MainClass(0)
{
}

// Constructor for TypeInfo.  Sets up the internal defaults
// and sets other variables to null
TypeInfo::TypeInfo()
	: Fields(41), Methods(41), Name(0), Size(0), IsReferenceType(true), SecondaryType(0)
{
}

// Constructor for VariableInfo.  Start will all null values.
VariableInfo::VariableInfo()
	: Type(0), Name(0), Offset(0)
{
}

// Constructor for MethodInfo.  Sets up the internal hashtables
// and sets all other variables to null
MethodInfo::MethodInfo()
	: ReturnType(0), Variables(41), Parameters(41), Name(0)
{
}
