
#include "ParseTreeBuilder.h"
#include "PrintTest.h"
#include "CodeGen.h"
#include "AlphaAnalizer.h"
#include "BetaAnalizer.h"
#include "GammaAnalizer.h"
#include "ErrorManager.h"

int yyparse();
void InitializeLexer(ErrorManager* errorManager, Buffer* buffer);
void InitializeParser(IParseTreeBuilder* treeBuilder);



ProgramNode* GetProgram();

// the main function runs the lexer through the input
//  at the same time the parser generates a tree from
//	input.
int main( int argc, char *argv[ ], char *envp[ ] )
{
	Buffer buffer;

	if (argc == 1)
	{
		buffer.GetStdIn();
	}
	else
	{
		buffer.GetFile(argv[1]);
	}

	ErrorManager* error = new ErrorManager(&(std::cerr), &buffer, 20);

	
	// Create a new ParseTreeBuilder
	IParseTreeBuilder* builder = new ParseTreeBuilder();

	// Print out the copyright notice
	printf("<!-- Decaf compiler, Copyright 2002 -->\n\n");

	// Initialize the lexer and the perser
	InitializeLexer(error, &buffer);
	InitializeParser(builder);

	// run the parser
	yyparse();

	// Get the parsed program
	ProgramNode* program = GetProgram();

	// if the program has been parsed and there
	// were no errors then print out the parse tree
	if (program && !error->GetErrorCount())
	{
			//if (argc >= 1 && (argv[1][0] == 'p'))
			//{
			//	printf("\n\n\n\n\t\t XML Parse Tree\n\n\n\n");
			//	PrintTest test;
			//	test.State.Errors = error;
			//	program->Accept(&test);
			//}
		
		
			if (error->GetErrorCount() == 0)
			{
				//printf("\n\n\n\n\t\t Alpha Analizer\n\n\n\n");
				AlphaAnalizer alpha;
				alpha.State.Errors = error;
				program->Accept(&alpha);

				//printf("\n\n\n\n\t\t Beta Analizer\n\n\n\n");
				BetaAnalizer beta;
				beta.State.Errors = error;
				program->Accept(&beta);

			}
		
			if (error->GetErrorCount() == 0)
			{
				//printf("\n\n\n\n\t\t Gamma Analizer\n\n\n\n");
				GammaAnalizer gamma;
				gamma.State.Errors = error;
				program->Accept(&gamma);
			}
		
			if (error->GetErrorCount() == 0)
			{
				printf("\n\n\n\n\t\t Compiled MIPS Assembly\n\n\n\n");
				const char* output = "test.asm";
				if (argc > 2)
					output = argv[2];
				CodeGen gener(output);
				printf("\t\tSaving output to %s\n\n", output);
				gener.State.Errors = error;
				program->Accept(&gener);
			}

//		Printer* printer = new Printer(&cout);
//		printer->Indent();
//		program->Print(printer);
//		delete printer;

	}

	int err = error->GetErrorCount();
	if (err)
	{
		printf("\nCompilation aborted due to errors\n\n");
	}

	// Clean up everything
	TreeNode::CleanUp();
	Token::CleanUp();
	
	delete error;

	// we're done
	return err;
}
