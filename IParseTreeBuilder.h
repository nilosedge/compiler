
#ifndef _IPARSE_TREE_BUILDER
#define _IPARSE_TREE_BUILDER

#include "TreeNode.h"

#define PURE = 0

//////////////////////////////////
// IParseTreeBuilder
//
//   This interface is used by the Parser to generate output 
//   from the lexer
//
struct IParseTreeBuilder
{
    virtual ProgramNode* CreateProgramNode(ClassNodes* cs) PURE;

    virtual ClassNodes* CreateClassNodes(ClassNode* classnode) PURE;
    virtual ClassNodes* AppendClassNode(ClassNodes* nodes, ClassNode* classnode) PURE;

    virtual ClassNode* CreateClassNode(IdentifierToken* name, ClassBodyNode* body) PURE;

    virtual ClassBodyNode* CreateClassBodyNode() PURE;
    virtual ClassBodyNode* CreateClassBodyNode(MethodNodes* methods) PURE;
    virtual ClassBodyNode* CreateClassBodyNode(FieldNodes* fields) PURE;
    virtual ClassBodyNode* CreateClassBodyNode(FieldNodes* fields, MethodNodes* methods) PURE;

    virtual FieldNodes* CreateFieldNodes(FieldNode* field) PURE;
    virtual FieldNodes* AppendFieldNode(FieldNodes* fields, FieldNode* field) PURE;
    
    virtual FieldNode* CreateFieldNode(NameWithTypeNode* name) PURE;
    
    virtual MethodNodes* CreateMethodNodes(MethodNode* method) PURE;
    virtual MethodNodes* AppendMethodNode(MethodNodes* methods, MethodNode* method) PURE;
    virtual MethodNode* CreateMethodNode(NameWithReturnTypeNode* name, ParameterNodes* parameters, MethodBodyNode* body) PURE;

    virtual NameWithTypeNode* CreateNameWithTypeNode(TypeNode* type, IdentifierToken* name) PURE;

    virtual NameWithReturnTypeNode* CreateNameWithReturnTypeNode(TypeNode* type, IdentifierToken* name) PURE;
    
    virtual ArrayTypeNode* CreateIntArrayTypeNode(Token* type) PURE;
    virtual ArrayTypeNode* CreateArrayTypeNode(IdentifierToken* name) PURE;

    virtual TypeNode* CreateIntTypeNode(Token* type) PURE;
    virtual TypeNode* CreateTypeNode(IdentifierToken* name) PURE;

    virtual ParameterNodes* CreateParameterNodes() PURE;
    virtual ParameterNodes* CreateParameterNodes(ParameterNode* Parameter) PURE;
    virtual ParameterNodes* AppendParameterNode(ParameterNodes* Parameters, ParameterNode* parameter) PURE;

    virtual ParameterNode* CreateParameterNode(NameWithTypeNode* name) PURE;

    virtual MethodBodyNode* CreateMethodBodyNode() PURE;
    virtual MethodBodyNode* CreateMethodBodyNode(StatementNodes* statement) PURE;
    virtual MethodBodyNode* CreateMethodBodyNode(VariableNodes* variables) PURE;
    virtual MethodBodyNode* CreateMethodBodyNode(VariableNodes* variables, StatementNodes* statements) PURE;

    virtual VariableNodes* CreateVariableNodes(VariableNode* variable) PURE;
    virtual VariableNodes* AppendVariableNode(VariableNodes* variables, VariableNode* variable) PURE;

    virtual VariableNode* CreateVariableNode(NameWithTypeNode* name) PURE;

    virtual StatementNodes* CreateStatementNodes(StatementNode* statement) PURE;
    virtual StatementNodes* AppendStatementNode(StatementNodes* statements, StatementNode* statement) PURE;

    virtual StatementBlockNode* CreateStatementBlockNode(StatementNodes* statements) PURE;
    
    virtual StatementNode* CreateNoOpStatementNode(Token* token) PURE;
    virtual StatementNode* CreateAssignmentStatementNode(MemberAccessNode* memberaccess, ExpressionNode* expression) PURE;
    virtual StatementNode* CreateMethodCallStatementNode(MemberAccessNode* memberaccess, ArgumentNodes* args) PURE;
    virtual StatementNode* CreatePrintStatementNode(ArgumentNodes* arguments) PURE;
    virtual StatementNode* CreateWhileStatementNode(ExpressionNode* expression, StatementNode* statement) PURE;
    virtual StatementNode* CreateReturnStatementNode(Token* token) PURE;
    virtual StatementNode* CreateReturnStatementNode(ExpressionNode* expression) PURE;

    virtual MemberAccessNode* CreateMemberAccessNode(MemberAccessNode* memberaccess, ForeignMemberAccessNode* foreignmemberaccess) PURE;

    virtual MemberAccessNode* CreateThisAccessNode(Token* token) PURE;
    virtual MemberAccessNode* CreateMemberAccessNode(IdentifierToken* token) PURE;
    virtual MemberAccessNode* CreateMemberArrayAccessNode(IdentifierToken* name, ExpressionNode* index) PURE;

    virtual ForeignMemberAccessNode* CreateForeignMemberAccessNode(IdentifierToken* token) PURE;
    virtual ForeignMemberAccessNode* CreateForeignMemberArrayAccessNode(IdentifierToken* token, ExpressionNode* expression) PURE;

    virtual ArgumentNodes* CreateArgumentNodes() PURE;

    virtual ArgumentNodes* CreateArgumentNodes(ExpressionNode* expression) PURE;
    virtual ArgumentNodes* AppendArgumentNode(ArgumentNodes* arguments, ExpressionNode* expression) PURE;
    
    virtual IfStatementNode* CreateIfStatementNode(ExpressionNode* expression, StatementNode* statement, StatementNode* elsestatement) PURE;

    virtual ExpressionNode* CreateExpressionNode(RelationalExpressionNode* relationalexpression) PURE;
    virtual ExpressionNode* CreateConstructorCallNode(IdentifierToken* toke) PURE;
    virtual ExpressionNode* CreateArrayConstructorCallNode(TypeNode* type, ExpressionNode* expression) PURE;

    virtual RelationalExpressionNode* CreateRelationalExpressionNode(SubExpressionNode* subexpression) PURE;
    virtual RelationalExpressionNode* CreateIsEqualNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2) PURE;
    virtual RelationalExpressionNode* CreateIsNotEqualNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2) PURE;
    virtual RelationalExpressionNode* CreateIsLessThanNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2) PURE;
    virtual RelationalExpressionNode* CreateIsGreaterThanNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2) PURE;
    virtual RelationalExpressionNode* CreateIsLessThanOrEqualNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2) PURE;
    virtual RelationalExpressionNode* CreateIsGreaterThanOrEqualNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2) PURE;
 
    virtual SubExpressionNode* CreateSubExpressionNode(TermNode* term) PURE;
    virtual SubExpressionNode* CreateAdditionNode(SubExpressionNode* subexpression1, SubExpressionNode* subexpression2) PURE;
    virtual SubExpressionNode* CreateSubtractionNode(SubExpressionNode* subexpression1, SubExpressionNode* subexpression2) PURE;
    virtual SubExpressionNode* CreateLogicalOrNode(SubExpressionNode* subexpression1, SubExpressionNode* subexpression2) PURE;

    virtual TermNode* CreateTermNode(FactorNode* factor) PURE;
    virtual TermNode* CreateMultiplyNode(TermNode* term1, TermNode* term2) PURE;
    virtual TermNode* CreateDivideNode(TermNode* term1, TermNode* term2) PURE;
    virtual TermNode* CreateModulusNode(TermNode* term1, TermNode* term2) PURE;
    virtual TermNode* CreateLogicalAndNode(TermNode* term1, TermNode* term2) PURE;

    virtual FactorNode* CreateVariableAccessFactorNode(MemberAccessNode* memberaccess) PURE;
    virtual FactorNode* CreateNumberNode(NumberToken* token) PURE;
    virtual FactorNode* CreateNullNode(Token* token) PURE;
    virtual FactorNode* CreateMethodCallNode(MemberAccessNode* member, ArgumentNodes* arguments) PURE;
    virtual FactorNode* CreateReadNode(Token* token) PURE;

    virtual FactorNode* CreateParenExpressionNode(ExpressionNode* expression) PURE;
    virtual UnaryOperationNode* CreateUnaryMinusNode(FactorNode* factor) PURE;
    virtual UnaryOperationNode* CreateLogicalNotNode(FactorNode* factor) PURE;


};


#endif // _IPARSE_TREE_BUILDER 

