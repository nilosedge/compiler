" Vim syntax file
" Language:	decaf
" Maintainer:	Olin Blodgett <oblodget@southern.edu>
" URL:	http://oblodget.public.southern.edu
" Last Change:	24th April 2002

if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

" tags
syn keyword decafStatement if while else return this
syn keyword decafDefine new
syn keyword decafStructure class
syn keyword decafType int void
syn keyword decafNumber null
syn keyword decafFunction print read length
syn match decafNumber "-\=\<\d\+\>"
syn match decafOperator	"[!=<>]"
syn match decafBrak    "[({[\]})]"
syn match decafComment "//.*$"



if version >= 508 || !exists("did_decaf_syn_inits")
  if version < 508
    let did_decaf_syn_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif


  HiLink	 decafComment		Comment
  HiLink	 decafDefine		Define
  HiLink     decafBrak			Delimiter
  HiLink     decafType			Type
  HiLink     decafNumber		Number
  HiLink	 decafOperator		Operator
  HiLink	 decafFunction		Function
  HiLink	 decafStatement		Statement
  HiLink	 decafStructure		Structure

  delcommand HiLink
endif

let b:current_syntax = "decaf"

" vim: ts=4
