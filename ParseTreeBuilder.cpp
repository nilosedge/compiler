
#include "TreeNode.h"
#include "ParseTreeBuilder.h"


/////////////////////////////////////////////////////////////////
//  This is the implementation for the ParseTreeBUILder class
//  	These methods almost all either call constructors
//		or call the append method on Collections for they
//		do both.
/////////////////////////////////////////////////////////////////


ProgramNode* ParseTreeBuilder::CreateProgramNode(ClassNodes* cs)
{
	return new ProgramNode(cs);
}

ClassNodes* ParseTreeBuilder::CreateClassNodes(ClassNode* classnode)
{
	ClassNodes* nodes = new ClassNodes();
	nodes->Append(classnode);
	return nodes;
}
ClassNodes* ParseTreeBuilder::AppendClassNode(ClassNodes* nodes, ClassNode* classnode)
{
	nodes->Append(classnode);
	return nodes;
}

ClassNode* ParseTreeBuilder::CreateClassNode(IdentifierToken* name, ClassBodyNode* body)
{
	return new ClassNode(name, body);
}

ClassBodyNode* ParseTreeBuilder::CreateClassBodyNode()
{
	return new ClassBodyNode();
}
ClassBodyNode* ParseTreeBuilder::CreateClassBodyNode(MethodNodes* methods)
{
	return new ClassBodyNode(methods);
}
ClassBodyNode* ParseTreeBuilder::CreateClassBodyNode(FieldNodes* fields)
{
	return new ClassBodyNode(fields);
}
ClassBodyNode* ParseTreeBuilder::CreateClassBodyNode(FieldNodes* fields, MethodNodes* methods)
{
	return new ClassBodyNode(fields, methods);
}

FieldNodes* ParseTreeBuilder::CreateFieldNodes(FieldNode* field)
{
	FieldNodes* nodes = new FieldNodes();
	nodes->Append(field);
	return nodes;
}
FieldNodes* ParseTreeBuilder::AppendFieldNode(FieldNodes* fields, FieldNode* field)
{
	fields->Append(field);
	return fields;
}
    
FieldNode* ParseTreeBuilder::CreateFieldNode(NameWithTypeNode* name)
{
	return new FieldNode(name);
}
    
MethodNodes* ParseTreeBuilder::CreateMethodNodes(MethodNode* method)
{
	MethodNodes* nodes = new MethodNodes();
	nodes->Append(method);
	return nodes;
}
MethodNodes* ParseTreeBuilder::AppendMethodNode(MethodNodes* methods, MethodNode* method)
{
	methods->Append(method);
	return methods;
}
MethodNode* ParseTreeBuilder::CreateMethodNode(NameWithReturnTypeNode* name, ParameterNodes* parameters, MethodBodyNode* body)
{
	return new MethodNode(name, parameters, body);
}

NameWithTypeNode* ParseTreeBuilder::CreateNameWithTypeNode(TypeNode* type, IdentifierToken* name)
{
	return new NameWithTypeNode(type, name);
}

NameWithReturnTypeNode* ParseTreeBuilder::CreateNameWithReturnTypeNode(TypeNode* type, IdentifierToken* name)
{
	return new NameWithReturnTypeNode(type, name);
}
    
ArrayTypeNode* ParseTreeBuilder::CreateIntArrayTypeNode(Token* type)
{
	return new ArrayTypeNode(ArrayTypeNode::Int, type);
}
ArrayTypeNode* ParseTreeBuilder::CreateArrayTypeNode(IdentifierToken* name)
{
	return new ArrayTypeNode(name);
}

TypeNode* ParseTreeBuilder::CreateIntTypeNode(Token* type)
{
	return new TypeNode(TypeNode::Int, type);
}
TypeNode* ParseTreeBuilder::CreateTypeNode(IdentifierToken* name)
{
	return new TypeNode(name);
}

ParameterNodes* ParseTreeBuilder::CreateParameterNodes()
{
	return new ParameterNodes();
}
ParameterNodes* ParseTreeBuilder::CreateParameterNodes(ParameterNode* parameter)
{
	ParameterNodes* nodes = new ParameterNodes();
	nodes->Append(parameter);
	return nodes;
}
ParameterNodes* ParseTreeBuilder::AppendParameterNode(ParameterNodes* parameters, ParameterNode* parameter)
{
	parameters->Append(parameter);
	return parameters;
}

ParameterNode* ParseTreeBuilder::CreateParameterNode(NameWithTypeNode* name)
{
	return new ParameterNode(name);
}

MethodBodyNode* ParseTreeBuilder::CreateMethodBodyNode()
{
	return new MethodBodyNode(0, 0);
}
MethodBodyNode* ParseTreeBuilder::CreateMethodBodyNode(StatementNodes* statement)
{
	return new MethodBodyNode(0, statement);
}
MethodBodyNode* ParseTreeBuilder::CreateMethodBodyNode(VariableNodes* variables)
{
	return new MethodBodyNode(variables, 0);
}
MethodBodyNode* ParseTreeBuilder::CreateMethodBodyNode(VariableNodes* variables, StatementNodes* statements)
{
	return new MethodBodyNode(variables, statements);
}

VariableNodes* ParseTreeBuilder::CreateVariableNodes(VariableNode* variable)
{
	VariableNodes* nodes = new VariableNodes();
	nodes->Append(variable);
	return nodes;
}
VariableNodes* ParseTreeBuilder::AppendVariableNode(VariableNodes* variables, VariableNode* variable)
{
	variables->Append(variable);
	return variables;
}

VariableNode* ParseTreeBuilder::CreateVariableNode(NameWithTypeNode* name)
{
	return new VariableNode(name);
}

StatementNodes* ParseTreeBuilder::CreateStatementNodes(StatementNode* statement)
{
	StatementNodes* nodes = new StatementNodes();
	nodes->Append(statement);
	return nodes;
}
StatementNodes* ParseTreeBuilder::AppendStatementNode(StatementNodes* statements, StatementNode* statement)
{
	statements->Append(statement);
	return statements;
}

StatementBlockNode* ParseTreeBuilder::CreateStatementBlockNode(StatementNodes* statements)
{
	return new StatementBlockNode(statements);
}
    
StatementNode* ParseTreeBuilder::CreateNoOpStatementNode(Token* token)
{
	return new NoOpStatementNode(token);
}
StatementNode* ParseTreeBuilder::CreateAssignmentStatementNode(MemberAccessNode* memberaccess, ExpressionNode* expression)
{
	return new AssignmentStatementNode(memberaccess, expression);
}
StatementNode* ParseTreeBuilder::CreateMethodCallStatementNode(MemberAccessNode* memberaccess, ArgumentNodes* arguments)
{
	return new MethodCallStatementNode(memberaccess, arguments);
}
StatementNode* ParseTreeBuilder::CreatePrintStatementNode(ArgumentNodes* arguments)
{
	return new PrintStatementNode(arguments);
}
StatementNode* ParseTreeBuilder::CreateWhileStatementNode(ExpressionNode* expression, StatementNode* statement)
{
	return new WhileStatementNode(expression, statement);
}
StatementNode* ParseTreeBuilder::CreateReturnStatementNode(Token* token)
{
	return new ReturnStatementNode(token);
}
StatementNode* ParseTreeBuilder::CreateReturnStatementNode(ExpressionNode* expression)
{
	return new ReturnStatementNode(expression);
}

MemberAccessNode* ParseTreeBuilder::CreateMemberAccessNode(MemberAccessNode* memberaccess, ForeignMemberAccessNode* foreignmemberaccess)
{
	memberaccess->SetInnerAccess(foreignmemberaccess);
	return memberaccess;
}

MemberAccessNode* ParseTreeBuilder::CreateThisAccessNode(Token* token)
{
	return new ThisAccessNode(token);
}
MemberAccessNode* ParseTreeBuilder::CreateMemberAccessNode(IdentifierToken* token)
{
	return new MemberAccessNode(token);
}
MemberAccessNode* ParseTreeBuilder::CreateMemberArrayAccessNode(IdentifierToken* name, ExpressionNode* index)
{
	return new MemberArrayAccessNode(name, index);
}

ForeignMemberAccessNode* ParseTreeBuilder::CreateForeignMemberAccessNode(IdentifierToken* token)
{
	return new ForeignMemberAccessNode(token);
}
ForeignMemberAccessNode* ParseTreeBuilder::CreateForeignMemberArrayAccessNode(IdentifierToken* token, ExpressionNode* expression)
{
	return new ForeignMemberArrayAccessNode(token, expression);
}

ArgumentNodes* ParseTreeBuilder::CreateArgumentNodes()
{
	return new ArgumentNodes();
}

ArgumentNodes* ParseTreeBuilder::CreateArgumentNodes(ExpressionNode* expression)
{
	ArgumentNodes* nodes = new ArgumentNodes();
	nodes->Append(expression);
	return nodes;
}
ArgumentNodes* ParseTreeBuilder::AppendArgumentNode(ArgumentNodes* arguments, ExpressionNode* expression)
{
	arguments->Append(expression);
	return arguments;
}
    
IfStatementNode* ParseTreeBuilder::CreateIfStatementNode(ExpressionNode* expression, StatementNode* statement, StatementNode* elsestatement)
{
	return new IfStatementNode(expression, statement, elsestatement);
}

ExpressionNode* ParseTreeBuilder::CreateExpressionNode(RelationalExpressionNode* relationalexpression)
{
	//return new ExpressionNode(relationalexpression);
	return relationalexpression;
}
ExpressionNode* ParseTreeBuilder::CreateConstructorCallNode(IdentifierToken* token)
{
	TypeNode* type = new TypeNode(token);
	return new ConstructorCallNode(type);
}
ExpressionNode* ParseTreeBuilder::CreateArrayConstructorCallNode(TypeNode* type, ExpressionNode* expression)
{
	return new ArrayConstructorCallNode(type, expression);
}

RelationalExpressionNode* ParseTreeBuilder::CreateRelationalExpressionNode(SubExpressionNode* subexpression)
{
	//return new RelationalExpressionNode(subexpression);
	return subexpression;
}
RelationalExpressionNode* ParseTreeBuilder::CreateIsEqualNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2)
{
	return new IsEqualNode(relationalexpression1, relationalexpression2);
}
RelationalExpressionNode* ParseTreeBuilder::CreateIsNotEqualNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2)
{
	return new IsNotEqualNode(relationalexpression1, relationalexpression2);
}
RelationalExpressionNode* ParseTreeBuilder::CreateIsLessThanNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2)
{
	return new IsLessThanNode(relationalexpression1, relationalexpression2);
}
RelationalExpressionNode* ParseTreeBuilder::CreateIsGreaterThanNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2)
{
	return new IsGreaterThanNode(relationalexpression1, relationalexpression2);
}
RelationalExpressionNode* ParseTreeBuilder::CreateIsLessThanOrEqualNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2)
{
	return new IsLessThanOrEqualNode(relationalexpression1, relationalexpression2);
}
RelationalExpressionNode* ParseTreeBuilder::CreateIsGreaterThanOrEqualNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2)
{
	return new IsGreaterThanOrEqualNode(relationalexpression1, relationalexpression2);
}
SubExpressionNode* ParseTreeBuilder::CreateSubExpressionNode(TermNode* term)
{
	//return new SubExpressionNode(term);
	return term;
}
SubExpressionNode* ParseTreeBuilder::CreateAdditionNode(SubExpressionNode* subexpression1, SubExpressionNode* subexpression2)
{
	return new AdditionNode(subexpression1, subexpression2);
}
SubExpressionNode* ParseTreeBuilder::CreateSubtractionNode(SubExpressionNode* subexpression1, SubExpressionNode* subexpression2)
{
	return new SubtractionNode(subexpression1, subexpression2);
}
SubExpressionNode* ParseTreeBuilder::CreateLogicalOrNode(SubExpressionNode* subexpression1, SubExpressionNode* subexpression2)
{
	return new LogicalOrNode(subexpression1, subexpression2);
}

TermNode* ParseTreeBuilder::CreateTermNode(FactorNode* factor)
{
	//return new TermNode(factor);
	return factor;
}
TermNode* ParseTreeBuilder::CreateMultiplyNode(TermNode* term1, TermNode* term2)
{
	return new MultiplyNode(term1, term2);
}
TermNode* ParseTreeBuilder::CreateDivideNode(TermNode* term1, TermNode* term2)
{
	return new DivideNode(term1, term2);
}
TermNode* ParseTreeBuilder::CreateModulusNode(TermNode* term1, TermNode* term2)
{
	return new ModulusNode(term1, term2);
}
TermNode* ParseTreeBuilder::CreateLogicalAndNode(TermNode* term1, TermNode* term2)
{
	return new LogicalAndNode(term1, term2);
}

FactorNode* ParseTreeBuilder::CreateVariableAccessFactorNode(MemberAccessNode* memberaccess)
{
	return new VariableAccessFactorNode(memberaccess);
}
FactorNode* ParseTreeBuilder::CreateNumberNode(NumberToken* token)
{
	return new NumberNode(token);
}
FactorNode* ParseTreeBuilder::CreateNullNode(Token* token)
{
	return new NullNode(token);
}
FactorNode* ParseTreeBuilder::CreateMethodCallNode(MemberAccessNode* member, ArgumentNodes* arguments)
{
	return new MethodCallNode(member, arguments);
}
FactorNode* ParseTreeBuilder::CreateReadNode(Token* token)
{
	return new ReadNode(token);
}

FactorNode* ParseTreeBuilder::CreateParenExpressionNode(ExpressionNode* expression)
{
	return new ParenExpressionNode(expression);
}
UnaryOperationNode* ParseTreeBuilder::CreateUnaryMinusNode(FactorNode* factor)
{
	return new UnaryMinusNode(factor);
}
UnaryOperationNode* ParseTreeBuilder::CreateLogicalNotNode(FactorNode* factor)
{
	return new LogicalNotNode(factor);
}

