%{
/////////////////////////////////////
// Parser
//
//	This is the yacc definition for decaf
//	it creates a parser for the language


#include "TreeNode.h"
#include "IParseTreeBuilder.h"

IParseTreeBuilder* TreeBuilder;
ProgramNode* program;

int yyerror(const char* msg);
int yylex();

// InitializeParser
//  sets up the parser
// Parameters
//	treeBuilder is a class that implements the IParseTreeBuilder interface
void InitializeParser(IParseTreeBuilder* treeBuilder)
{
	TreeBuilder = treeBuilder;
	program = 0;
}

// GetProgram
//	returns the program node of the parse
// returns
//	the program node or NULL if the parse was unsuccessful
ProgramNode* GetProgram()
{
	return program;
}

// this is defined to meke the parser give mostly usful error messages
#define YYERROR_VERBOSE 10

///////////////////////////////////
// Note on errors
//
//  Most errors use the verbose error generation ability of the parser
//  More often then not, the parser has an idea of what it is expecting
//  All error messages are currently printed using the yyerror function
//  our implementation of yyerror prints the line number and the source
//  for the error.
//
//  most error rules do nothing, but let the parser print out an error
//	this is because the error rule only provides a place for the parser
//	to resume from.

%}

%union
{
	Token* token;
	NumberToken* number;
	IdentifierToken* identifier;

	ProgramNode* programNode;
	
	ClassNodes* classNodes;
	ClassNode* classNode;
	ClassBodyNode* classBodyNode;

	FieldNodes* fieldNodes;
	FieldNode* fieldNode;

	MethodNodes* methodNodes;
	MethodNode* methodNode;
	
	NameWithTypeNode* nameWithTypeNode;
	NameWithReturnTypeNode* nameWithReturnTypeNode;
	
	TypeNode* typeNode;
	
	ParameterNodes* parameterNodes;
	ParameterNode* parameterNode;
	
	MethodBodyNode* methodBodyNode;
	
	VariableNodes* variableNodes;
	VariableNode* variableNode;
	
	StatementNodes* statementNodes;
	StatementNode* statementNode;
	MemberAccessNode* memberAccessNode;
	ForeignMemberAccessNode* foreignMemberAccessNode;
	
	ArgumentNodes* argumentNodes;
	
	ExpressionNode* expressionNode;
	RelationalExpressionNode* relationalExpressionNode;
	SubExpressionNode* subExpressionNode;
	TermNode* termNode;
	FactorNode* factorNode;
}

%token <token> _class _else _if _int _new _null _print _readfun _return _this 
%token <token> _void _while LeftArray RightArray LeftCurly RightCurly 
%token <token> LeftParen RightParen IsNotEqual IsEqual IsLessThan 
%token <token> IsGreaterThan IsLessThanOrEqual IsGreaterThanOrEqual 
%token <token> LogicalAnd LogicalOr LogicalNot OpPlus OpMinus OpMultiply 
%token <token> OpDivide OpModulus SemiColon Comma SetEqual MemberOf 

%token <number> Number 
%token <identifier> Identifier 

%type <programNode> PROGRAM
%type <classNodes> CLASS_DECLARATION_LIST
%type <classNode> CLASS_DECLARATION
%type <classBodyNode> CLASS_BODY
%type <fieldNodes> FIELD_DECLARATION_LIST
%type <fieldNode> FIELD_DECLARATION
%type <methodNodes> METHOD_DECLARATION_LIST
%type <methodNode> METHOD_DECLARATION
%type <nameWithTypeNode> NAME_WITH_TYPE
%type <nameWithReturnTypeNode> NAME_WITH_RETURN_TYPE
%type <typeNode> TYPE ARRAY_TYPE TYPE_NAME
%type <identifier> IDENTIFIER_WITH_LEFT_ARRAY
%type <parameterNodes> PARAMETER_LIST PARAMETER_LIST2
%type <parameterNode> PARAMETER
%type <methodBodyNode> METHOD_BODY
%type <variableNodes> LOCAL_VAR_DECLARATION_LIST
%type <variableNode> LOCAL_VAR_DECLARATION
%type <statementNodes> STATEMENT_LIST
%type <statementNode> STATEMENT STATEMENT_BLOCK ELSE_PHRASE IF_STATEMENT
%type <memberAccessNode> NAME VARIABLE_ACCESS
%type <foreignMemberAccessNode> FIELD_ACCESS
%type <argumentNodes> ARG_LIST ARG_LIST2
%type <expressionNode> EXPRESSION
%type <relationalExpressionNode> RELATIONAL_EXPRESSION
%type <subExpressionNode> SUB_EXPRESSION
%type <termNode> TERM
%type <factorNode> FACTOR UNARY_OP_FACTOR

%left IsEqual IsNotEqual IsLessThan IsGreaterThan IsLessThanOrEqual IsGreaterThanOrEqual
%left OpPlus OpMinus LogicalOr
%left OpMultiply OpDivide OpModulus LogicalAnd

%nonassoc UOpPlus UOpMinus  

%nonassoc LessThanElse
%nonassoc _else

%%
  
PROGRAM:
	CLASS_DECLARATION_LIST
					   { $$ = program = TreeBuilder->CreateProgramNode($1); }
	;
 

CLASS_DECLARATION_LIST:
	CLASS_DECLARATION  { $$ = TreeBuilder->CreateClassNodes($1); }
	| CLASS_DECLARATION_LIST CLASS_DECLARATION
					   { $$ = TreeBuilder->AppendClassNode($1, $2); }
	;

CLASS_DECLARATION:
	_class Identifier CLASS_BODY
					   { $$ = TreeBuilder->CreateClassNode($2, $3); }
	;

CLASS_BODY:
	LeftCurly RightCurly
					   { $$ = TreeBuilder->CreateClassBodyNode(); }
	| LeftCurly METHOD_DECLARATION_LIST RightCurly
					   { $$ = TreeBuilder->CreateClassBodyNode($2); }
	| LeftCurly FIELD_DECLARATION_LIST RightCurly
					   { $$ = TreeBuilder->CreateClassBodyNode($2); }
	| LeftCurly FIELD_DECLARATION_LIST METHOD_DECLARATION_LIST RightCurly
					   { $$ = TreeBuilder->CreateClassBodyNode($2, $3); }
	;

FIELD_DECLARATION_LIST:
	FIELD_DECLARATION  { $$ = TreeBuilder->CreateFieldNodes($1); }
	| FIELD_DECLARATION_LIST FIELD_DECLARATION
					   { $$ = TreeBuilder->AppendFieldNode($1, $2); }
	;

FIELD_DECLARATION:
	NAME_WITH_TYPE SEMI_COLON
					   { $$ = TreeBuilder->CreateFieldNode($1); }
	;

METHOD_DECLARATION_LIST:
	METHOD_DECLARATION { $$ = TreeBuilder->CreateMethodNodes($1); }
	| METHOD_DECLARATION_LIST METHOD_DECLARATION
					   { $$ = TreeBuilder->AppendMethodNode($1, $2); }
	;
	
METHOD_DECLARATION:
	NAME_WITH_RETURN_TYPE LeftParen PARAMETER_LIST RightParen METHOD_BODY
					   { $$ = TreeBuilder->CreateMethodNode($1, $3, $5); }
	;

NAME_WITH_TYPE:
	TYPE Identifier	   { $$ = TreeBuilder->CreateNameWithTypeNode($1, $2); }
	;

NAME_WITH_RETURN_TYPE:
	TYPE Identifier	   { $$ = TreeBuilder->CreateNameWithReturnTypeNode($1, $2); }
	| _void Identifier { $$ = TreeBuilder->CreateNameWithReturnTypeNode( 0, $2); }
	;

TYPE:
	TYPE_NAME		   { $$ = ($1); }
	| ARRAY_TYPE	   { $$ = ($1); }
	;

ARRAY_TYPE:
	_int LeftArray RightArray
					   { $$ = TreeBuilder->CreateIntArrayTypeNode($1); }
	| IDENTIFIER_WITH_LEFT_ARRAY RightArray
					   { $$ = TreeBuilder->CreateArrayTypeNode($1); }
	;

IDENTIFIER_WITH_LEFT_ARRAY:
	Identifier LeftArray
	;

TYPE_NAME:
	_int			   { $$ = TreeBuilder->CreateIntTypeNode($1); }
	| Identifier	   { $$ = TreeBuilder->CreateTypeNode($1); }
	;

PARAMETER_LIST:
	PARAMETER_LIST2	   { $$ = $1; }
	|				   { $$ = TreeBuilder->CreateParameterNodes(); }
	;

PARAMETER_LIST2:
	PARAMETER		   { $$ = TreeBuilder->CreateParameterNodes($1); }
	| PARAMETER_LIST2 Comma PARAMETER
					   { $$ = TreeBuilder->AppendParameterNode($1, $3); }
	;

PARAMETER:
	NAME_WITH_TYPE	   { $$ = TreeBuilder->CreateParameterNode($1); }
	;

METHOD_BODY:
	LeftCurly RightCurly
					   { $$ = TreeBuilder->CreateMethodBodyNode(); }
	| LeftCurly STATEMENT_LIST RightCurly
					   { $$ = TreeBuilder->CreateMethodBodyNode($2); }
	| LeftCurly LOCAL_VAR_DECLARATION_LIST RightCurly
					   { $$ = TreeBuilder->CreateMethodBodyNode($2); }
	| LeftCurly LOCAL_VAR_DECLARATION_LIST STATEMENT_LIST RightCurly
					   { $$ = TreeBuilder->CreateMethodBodyNode($2, $3); }
	;

LOCAL_VAR_DECLARATION_LIST:
	LOCAL_VAR_DECLARATION
					   { $$ = TreeBuilder->CreateVariableNodes($1); }
	| LOCAL_VAR_DECLARATION_LIST LOCAL_VAR_DECLARATION
					   { $$ = TreeBuilder->AppendVariableNode($1, $2); }
	;

LOCAL_VAR_DECLARATION:
	NAME_WITH_TYPE SEMI_COLON
					   { $$ = TreeBuilder->CreateVariableNode($1); }
	;

STATEMENT_LIST:
	STATEMENT_BLOCK	   { $$ = TreeBuilder->CreateStatementNodes($1); }
	| STATEMENT_LIST STATEMENT_BLOCK
					   { $$ = TreeBuilder->AppendStatementNode($1, $2); }
	;

STATEMENT_BLOCK:
	STATEMENT		   { $$ = $1; }
	| LeftCurly STATEMENT_LIST RightCurly
					   { $$ = TreeBuilder->CreateStatementBlockNode($2); }
	;

STATEMENT:
	SemiColon		   { $$ = TreeBuilder->CreateNoOpStatementNode($1); }
	| NAME SetEqual EXPRESSION SEMI_COLON
					   { $$ = TreeBuilder->CreateAssignmentStatementNode($1, $3); }
	| NAME LeftParen ARG_LIST RightParen SEMI_COLON
					   { $$ = TreeBuilder->CreateMethodCallStatementNode($1, $3); }
	| _print LeftParen ARG_LIST RightParen SEMI_COLON
					   { $$ = TreeBuilder->CreatePrintStatementNode($3); }
	| IF_STATEMENT	   { $$ = $1; }
	| _while LeftParen EXPRESSION RightParen STATEMENT_BLOCK
					   { $$ = TreeBuilder->CreateWhileStatementNode($3, $5); }
	| _return SEMI_COLON
					   { $$ = TreeBuilder->CreateReturnStatementNode($1); }
	| _return EXPRESSION SEMI_COLON
					   { $$ = TreeBuilder->CreateReturnStatementNode($2); }
	;

NAME:
	VARIABLE_ACCESS	   { $$ = $1; }
	| VARIABLE_ACCESS FIELD_ACCESS
					   { $$ = TreeBuilder->CreateMemberAccessNode($1, $2); }
	;

VARIABLE_ACCESS:
	_this			   { $$ = TreeBuilder->CreateThisAccessNode($1); }
	| Identifier	   { $$ = TreeBuilder->CreateMemberAccessNode($1); }
	| IDENTIFIER_WITH_LEFT_ARRAY EXPRESSION RightArray
					   { $$ = TreeBuilder->CreateMemberArrayAccessNode($1, $2); }
	;

FIELD_ACCESS:
	MemberOf Identifier 
					   { $$ = TreeBuilder->CreateForeignMemberAccessNode($2); }
	| MemberOf IDENTIFIER_WITH_LEFT_ARRAY EXPRESSION RightArray
					   { $$ = TreeBuilder->CreateForeignMemberArrayAccessNode($2, $3); }
	;

ARG_LIST:
	ARG_LIST2		   { $$ = $1; }
	|				   { $$ = TreeBuilder->CreateArgumentNodes(); }
	;

ARG_LIST2:
	EXPRESSION		   { $$ = TreeBuilder->CreateArgumentNodes($1); }
	| ARG_LIST2 Comma EXPRESSION
					   { $$ = TreeBuilder->AppendArgumentNode($1, $3); }
	;

IF_STATEMENT:
	_if LeftParen EXPRESSION RightParen STATEMENT_BLOCK ELSE_PHRASE
					   { $$ = TreeBuilder->CreateIfStatementNode($3, $5, $6); }
	;

ELSE_PHRASE:
	_else STATEMENT_BLOCK 
					   { $$ = $2; }
	| %prec LessThanElse
					   { $$ = 0; }
	;

EXPRESSION:
	RELATIONAL_EXPRESSION 
					   { $$ = $1; }
					   //{ $$ = TreeBuilder->CreateExpressionNode($1); }
	| _new Identifier LeftParen RightParen
					   { $$ = TreeBuilder->CreateConstructorCallNode($2); }
	| _new TYPE_NAME LeftArray EXPRESSION RightArray
					   { $$ = TreeBuilder->CreateArrayConstructorCallNode($2, $4); }
	;

RELATIONAL_EXPRESSION:
	SUB_EXPRESSION
					   { $$ = $1; }
					   //{ $$ = TreeBuilder->CreateRelationalExpressionNode($1); }
	| RELATIONAL_EXPRESSION IsEqual RELATIONAL_EXPRESSION
					   { $$ = TreeBuilder->CreateIsEqualNode($1, $3); }
	| RELATIONAL_EXPRESSION IsNotEqual  RELATIONAL_EXPRESSION
					   { $$ = TreeBuilder->CreateIsNotEqualNode($1, $3); }
					   
	| RELATIONAL_EXPRESSION IsLessThan  RELATIONAL_EXPRESSION
					   { $$ = TreeBuilder->CreateIsLessThanNode($1, $3); }
	| RELATIONAL_EXPRESSION IsGreaterThan  RELATIONAL_EXPRESSION
					   { $$ = TreeBuilder->CreateIsGreaterThanNode($1, $3); }
					   
	| RELATIONAL_EXPRESSION IsLessThanOrEqual  RELATIONAL_EXPRESSION
					   { $$ = TreeBuilder->CreateIsLessThanOrEqualNode($1, $3); }
	| RELATIONAL_EXPRESSION IsGreaterThanOrEqual  RELATIONAL_EXPRESSION
					   { $$ = TreeBuilder->CreateIsGreaterThanOrEqualNode($1, $3); }
	;

SUB_EXPRESSION:
	TERM
					   { $$ = $1; }
					   //{ $$ = TreeBuilder->CreateSubExpressionNode($1); }
	| SUB_EXPRESSION OpPlus  SUB_EXPRESSION
					   { $$ = TreeBuilder->CreateAdditionNode($1, $3); }
	| SUB_EXPRESSION OpMinus  SUB_EXPRESSION
					   { $$ = TreeBuilder->CreateSubtractionNode($1, $3); }
	| SUB_EXPRESSION LogicalOr SUB_EXPRESSION
					   { $$ = TreeBuilder->CreateLogicalOrNode($1, $3); }
	;

TERM:
	FACTOR
					   { $$ = $1; }
					   //{ $$ = TreeBuilder->CreateTermNode($1); }
	| TERM OpMultiply TERM
					   { $$ = TreeBuilder->CreateMultiplyNode($1, $3); }
	| TERM OpDivide TERM
					   { $$ = TreeBuilder->CreateDivideNode($1, $3); }
	| TERM OpModulus TERM
					   { $$ = TreeBuilder->CreateModulusNode($1, $3); }
	| TERM LogicalAnd TERM
					   { $$ = TreeBuilder->CreateLogicalAndNode($1, $3); }
	;

FACTOR:
	NAME
					   { $$ = TreeBuilder->CreateVariableAccessFactorNode($1); }
	| Number
					   { $$ = TreeBuilder->CreateNumberNode($1); }
	| _null
					   { $$ = TreeBuilder->CreateNullNode($1); }
	| NAME LeftParen ARG_LIST RightParen
					   { $$ = TreeBuilder->CreateMethodCallNode($1, $3); }
	| _readfun LeftParen RightParen
					   { $$ = TreeBuilder->CreateReadNode($1); }
	| LeftParen EXPRESSION RightParen
					   { $$ = TreeBuilder->CreateParenExpressionNode($2); }
	| UNARY_OP_FACTOR
					   { $$ = $1; }
	;

UNARY_OP_FACTOR:
	OpPlus FACTOR  %prec UOpPlus
					   { $$ = $2; }
	| OpMinus FACTOR  %prec UOpMinus
					   { $$ = TreeBuilder->CreateUnaryMinusNode($2); }
	| LogicalNot FACTOR 
					   { $$ = TreeBuilder->CreateLogicalNotNode($2); }
 	;

SEMI_COLON:
	SemiColon {}
	;
%%

