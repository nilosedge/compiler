
#ifndef _INODE_VISITOR_GammaAnalizer
#define _INODE_VISITOR_GammaAnalizer

#include "TreeNode.h"
#include "INodeVisitor.h"

class GammaAnalizer : public INodeVisitor
{
protected:
	TypeInfo* GetMemberAccessType(MemberAccessNode* member);
	TypeInfo* VisitMethodCall(MethodCall* methodCall);
	void AssertType(TreeNode* node, const char* typeName);
	void AssertType(TreeNode* node, TypeInfo* type);
	bool CompareTypes(TypeInfo*, TypeInfo*, bool);
	void VisitBinaryTermNode(BinaryTermNode* BinaryTerm);
	void VisitBinarySubExpressionNode(BinarySubExpressionNode* BinarySubExpression);
	void VisitBinaryRelationalExpressionNode(BinaryRelationalExpressionNode* BinaryRelationalExpression, bool allowNull = false);
public:
	void Visit(ProgramNode* Program);
	void Visit(ClassNode* Class);
	void Visit(FieldNode* Field);
	void Visit(MethodNode* Method);
	void Visit(ParameterNode* Parameter);
	void Visit(MethodBodyNode* MethodBody);
	void Visit(VariableNode* Variable);
	void Visit(AssignmentStatementNode* Assignment);
	void Visit(PrintStatementNode* Print);
	void Visit(IfStatementNode* If);
	void Visit(WhileStatementNode* While);
	void Visit(ReturnStatementNode* Return);
	void Visit(MethodCallStatementNode* MethodCall);
	void Visit(MethodCallNode* MethodCall);
	void Visit(NoOpStatementNode* NoOp);
	void Visit(IsEqualNode* IsEqual);
	void Visit(IsNotEqualNode* IsNotEqual);
	void Visit(IsLessThanNode* IsLessThan);
	void Visit(IsGreaterThanNode* IsGreaterThan);
	void Visit(IsLessThanOrEqualNode* IsLessThanOrEqual);
	void Visit(IsGreaterThanOrEqualNode* IsGreaterThanOrEqual);
	void Visit(ConstructorCallNode* ConstructorCall);
	void Visit(ArrayConstructorCallNode* ArrayConsturctorCall);
	void Visit(AdditionNode* Addition);
	void Visit(SubtractionNode* Subtraction);
	void Visit(LogicalOrNode* LogicalOr);
	void Visit(MultiplyNode* Multiply);
	void Visit(DivideNode* Divide);
	void Visit(ModulusNode* Modulus);
	void Visit(LogicalAndNode* LogicalAnd);
	void Visit(ReadNode* Read);
	void Visit(NullNode* Null);
	void Visit(VariableAccessFactorNode* VariableAccess);
	void Visit(NumberNode* Number);
	void Visit(UnaryMinusNode* UnaryMinus);
	void Visit(LogicalNotNode* LogicalNot);
	void Visit(ArgumentNode* Argument);
	void Visit(ThisAccessNode* This);
	void Visit(ForeignMemberAccessNode* MemberAccess);
	void Visit(MemberAccessNode* MemberAccess);
	void Visit(ForeignMemberArrayAccessNode* MemberAccess);
	void Visit(MemberArrayAccessNode* MemberAccess);
	void Visit(TypeNode* Type);
	void Visit(ArrayTypeNode* Array);
};


#endif // _INODE_VISITOR_GammaAnalizer
