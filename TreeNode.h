
#ifndef _TREE_NODE_H
#define _TREE_NODE_H

#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include "Printer.h"
#include "StateInfo.h"

///
//
//Memmory Management Node:
// this file contains the declarations for all Node classes, and the token
// classes.  Both the token and node classes put themselves into a collection
// of all tokens or all nodes, the TreeNode class and the Token class
// have a CleanUp method that removes all nodes from memmory.
// 


class Token
{
public://protected:
	static std::vector<Token*> allTokens;
public:
	const char* Value;
	int RowNumber;
	int ColNumber;
	int TokenID;

	Token(int tokenID, int colNumber, int rowNumber)
		: Value(""), RowNumber(rowNumber), ColNumber(colNumber), TokenID(tokenID)
	{
		allTokens.push_back(this);
	}
	virtual ~Token() {};
	static void CleanUp();
};

class IdentifierToken : public Token
{
public:

	IdentifierToken(const char* value, int tokenID, int colNumber, int rowNumber)
		: Token(tokenID, colNumber, rowNumber)
	{
		char* newValue = new char[strlen(value)+5];
		Value = strcpy(newValue, value);
	}

	~IdentifierToken()
	{
		delete Value;
	}
};

class NumberToken : public Token
{
public:

	NumberToken(const char* value, int tokenID, int colNumber, int rowNumber)
		: Token(tokenID, colNumber, rowNumber)
	{
		char* newValue = new char[strlen(value)+5];
		Value = strcpy(newValue, value);
	}

	~NumberToken()
	{
		delete Value;
	}
};


class MemberAccessNode;
class ArgumentNodes;

////////////////////////////
// MethodCall classes that inherit from this one
// this class will contain the logic for emiting
// code for a method call
class MethodCall
{
public://protected:
	MemberAccessNode* member;
	ArgumentNodes* arguments;
public:
	MethodCall(MemberAccessNode* member, ArgumentNodes* arguments);
	virtual ~MethodCall();
	virtual void PrintMethodCallChildren(Printer* printer);
};


struct INodeVisitor;

////////////////////////////
// TreeNode:
// this is the base class for every class represents
// a node in the AST
class TreeNode
{
public://protected:
	static std::vector<TreeNode*> allNodes;
	const char* nodeName;
public:
	int RowNumber;
	int ColNumber;
	TypeInfo* ReturnType;
	
	TreeNode(const char* name);
	virtual ~TreeNode();
	static void CleanUp();

	void Print(Printer* printer)
	{
		BeginPrintNode(printer);
		printer->Indent();
		PrintChildren(printer);
		printer->Unindent();
		EndPrintNode(printer);
	}
	virtual void BeginPrintNode(Printer* printer)
	{
		printer->Print("<%s>\n", nodeName);
	}
	virtual void PrintChildren(Printer* printer)
	{
	}
	virtual void EndPrintNode(Printer* printer)
	{
		printer->Print("</%s>\n", nodeName);
	}

	void LoadTokenInfo(Token* info)
	{
		if (!info) return;
		RowNumber = info->RowNumber;
		ColNumber = info->ColNumber;
	}

	void LoadTokenInfo(TreeNode* info)
	{
		if (!info) return;
		RowNumber = info->RowNumber;
		ColNumber = info->ColNumber;
	}

	virtual void Accept(INodeVisitor* visitor) = 0;

};

///////////////////////////
// Nodes:
// this class provides functionality for AST nodes
// that represent a set of nodes
class Nodes
{
public://protected:
	typedef std::vector<TreeNode*> NodeCollection;
	NodeCollection nodes;
public:
	Nodes();
	virtual ~Nodes();
	void Append(TreeNode* node)
	{
		if (node)
			nodes.push_back(node);
	}
	void PrintNodes(Printer* printer)
	{
		NodeCollection::iterator it;
		it = nodes.begin();
		while (it != nodes.end())
		{
			(*it)->Print(printer);
			it++;
		}
	}

	int GetLength() {
		return nodes.size();
	}
	
	inline void AcceptNodes(INodeVisitor* visitor)
	{
		NodeCollection::iterator it;
		it = nodes.begin();
		while (it != nodes.end())
		{
			(*it)->Accept(visitor);
			it++;
		}
	}

	// Added by Nilo out of bordom... just kidding
	// Reading header files is cool... just kidding again.
	// Ok I really needed this function so i did what i had
	// to to get it.
	inline void AcceptNodesReverse(INodeVisitor* visitor)
	{
		NodeCollection::reverse_iterator rit;
		rit = nodes.rbegin();
		while (rit != nodes.rend())
		{
			(*rit)->Accept(visitor);
			rit++;
		}
	}

};


class ExpressionNode : public TreeNode
{
public://protected:
public:
	ExpressionNode(const char* name)
		: TreeNode(name)
	{
	}
};

class RelationalExpressionNode : public ExpressionNode
{
public://protected:
public:
	RelationalExpressionNode(const char* name)
		: ExpressionNode(name)
	{
	}
};

class SubExpressionNode : public RelationalExpressionNode
{
public://protected:
public:
	SubExpressionNode(const char* name)
		: RelationalExpressionNode(name)
	{
	}
};

class TermNode : public SubExpressionNode
{
public://protected:
public:
	TermNode(const char* name)
		: SubExpressionNode(name)
	{
	}
};

class FactorNode : public TermNode
{
public://protected:
public:
	FactorNode(const char* name)
		: TermNode(name)
	{
	}
};



class TypeNode : public TreeNode
{
public://protected:
	IdentifierToken* typeName;
	int typeID;
public:
	enum InternalType
	{
		Void,
		Int,
		Array,
		New
	};
	TypeNode(IdentifierToken* name);
	TypeNode(InternalType type, Token* info);
	virtual void BeginPrintNode(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class ArrayTypeNode : public TypeNode
{
public://protected:
	TypeNode* innerType;
public:
	ArrayTypeNode(IdentifierToken* n);
	ArrayTypeNode(InternalType type, Token* i);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class NameWithTypeNode : public TreeNode
{
public:
	TypeNode* type;
	IdentifierToken* name;
public:
	NameWithTypeNode(TypeNode* t, IdentifierToken* n);
	virtual void BeginPrintNode(Printer* printer);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class NameWithReturnTypeNode: public TreeNode
{
public:
	TypeNode* type;
	IdentifierToken* name;
public:
	NameWithReturnTypeNode(TypeNode* t, IdentifierToken* n);
	virtual void BeginPrintNode(Printer* printer);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};



class ForeignMemberAccessNode : public TreeNode
{
public://protected:
	IdentifierToken* name;
	VariableInfo* Variable;
public:
	ForeignMemberAccessNode(IdentifierToken* n);
	virtual void BeginPrintNode(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class MemberAccessNode : public TreeNode
{
public://protected:
	IdentifierToken* name;
	ForeignMemberAccessNode* innerAccess;
	VariableInfo* Variable;
	
	MemberAccessNode(Token* n);
public:
	MemberAccessNode(IdentifierToken* n);
	void SetInnerAccess(ForeignMemberAccessNode* inner)
	{
		innerAccess = inner;
	}
	virtual void BeginPrintNode(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class ThisAccessNode : public MemberAccessNode
{
public://protected:
public:
	ThisAccessNode(Token* token);
	virtual void Accept(INodeVisitor* visitor);
};


class ForeignMemberArrayAccessNode : public ForeignMemberAccessNode
{
public://protected:
	ExpressionNode* index;
public:
	ForeignMemberArrayAccessNode(IdentifierToken* n, ExpressionNode* i);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class MemberArrayAccessNode : public MemberAccessNode
{
public://protected:
	ExpressionNode* index;
public:
	MemberArrayAccessNode(IdentifierToken* n, ExpressionNode* i);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};



class ArgumentNode : public TreeNode
{
public://protected:
	ExpressionNode* value;
public:
	ArgumentNode(ExpressionNode* expression);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class ArgumentNodes : public TreeNode, public Nodes
{
public://protected:
public:
	ArgumentNodes();
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
	virtual void AcceptReverse(INodeVisitor* visitor);
};



class UnaryOperationNode : public FactorNode
{
public://protected:
	FactorNode* value;
public:
	UnaryOperationNode(const char* nodeName, FactorNode* v)
		: FactorNode(nodeName), value(v)
	{
	}
	virtual void PrintChildren(Printer* printer);
};

class UnaryMinusNode : public UnaryOperationNode
{
public://protected:
public:
	UnaryMinusNode(FactorNode* factor);
	virtual void Accept(INodeVisitor* visitor);
};

class LogicalNotNode : public UnaryOperationNode
{
public://protected:
public:
	LogicalNotNode(FactorNode* factor);
	virtual void Accept(INodeVisitor* visitor);
};



class ParenExpressionNode : public FactorNode
{
public://protected:
	ExpressionNode* value;
public:
	ParenExpressionNode(ExpressionNode* expression);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class ReadNode : public FactorNode
{
public://protected:
public:
	ReadNode(Token* token);
	virtual void Accept(INodeVisitor* visitor);
};

class MethodCallNode : public FactorNode, public MethodCall
{
public://protected:
public:
	MethodCallNode(MemberAccessNode* method, ArgumentNodes* args);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class NullNode : public FactorNode
{
public://protected:
public:
	NullNode(Token* token);
	virtual void Accept(INodeVisitor* visitor);
};

class VariableAccessFactorNode : public FactorNode
{
public://protected:
	MemberAccessNode* variable;
public:
	VariableAccessFactorNode(MemberAccessNode* member);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class NumberNode : public FactorNode
{
public://protected:
	NumberToken* value;
public:
	NumberNode(NumberToken* number);
	virtual void BeginPrintNode(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};



class BinaryTermNode : public TermNode
{
public://protected:
	TermNode* left, *right;
public:
	BinaryTermNode(TermNode* l, TermNode* r);
	virtual void PrintChildren(Printer* printer);
	//virtual void Accept(INodeVisitor* visitor);
};



class MultiplyNode : public BinaryTermNode
{
public://protected:
public:
	MultiplyNode(TermNode* l, TermNode* r);
	virtual void Accept(INodeVisitor* visitor);
};

class DivideNode : public BinaryTermNode
{
public://protected:
public:
	DivideNode(TermNode* l, TermNode* r);
	virtual void Accept(INodeVisitor* visitor);
};

class ModulusNode : public BinaryTermNode
{
public://protected:
public:
	ModulusNode(TermNode* l, TermNode* r);
	virtual void Accept(INodeVisitor* visitor);
};

class LogicalAndNode : public BinaryTermNode
{
public://protected:
public:
	LogicalAndNode(TermNode* l, TermNode* r);
	virtual void Accept(INodeVisitor* visitor);
};



class BinarySubExpressionNode : public SubExpressionNode
{
public://protected:
	SubExpressionNode* left, *right;
public:
	BinarySubExpressionNode(SubExpressionNode* l, SubExpressionNode* r);
	virtual void PrintChildren(Printer* printer);
	//virtual void Accept(INodeVisitor* visitor);
};



class AdditionNode : public BinarySubExpressionNode
{
public://protected:
public:
	AdditionNode(SubExpressionNode* l, SubExpressionNode* r);
	virtual void Accept(INodeVisitor* visitor);
};

class SubtractionNode : public BinarySubExpressionNode
{
public://protected:
public:
	SubtractionNode(SubExpressionNode* l, SubExpressionNode* r);
	virtual void Accept(INodeVisitor* visitor);
};

class LogicalOrNode : public BinarySubExpressionNode
{
public://protected:
public:
	LogicalOrNode(SubExpressionNode* l, SubExpressionNode* r);
	virtual void Accept(INodeVisitor* visitor);
};


class BinaryRelationalExpressionNode : public RelationalExpressionNode 
{
public://protected:
	RelationalExpressionNode* left, *right;
public:
	BinaryRelationalExpressionNode(RelationalExpressionNode* l, RelationalExpressionNode* r);
	virtual void PrintChildren(Printer* printer);
	//virtual void Accept(INodeVisitor* visitor);
};



class IsEqualNode : public BinaryRelationalExpressionNode 
{
public://protected:
public:
	IsEqualNode(RelationalExpressionNode* l, RelationalExpressionNode* r);
	virtual void Accept(INodeVisitor* visitor);
};

class IsNotEqualNode : public BinaryRelationalExpressionNode 
{
public://protected:
public:
	IsNotEqualNode(RelationalExpressionNode* l, RelationalExpressionNode* r);
	virtual void Accept(INodeVisitor* visitor);
};

class IsLessThanNode : public BinaryRelationalExpressionNode 
{
public://protected:
public:
	IsLessThanNode(RelationalExpressionNode* l, RelationalExpressionNode* r);
	virtual void Accept(INodeVisitor* visitor);
};

class IsGreaterThanNode : public BinaryRelationalExpressionNode 
{
public://protected:
public:
	IsGreaterThanNode(RelationalExpressionNode* l, RelationalExpressionNode* r);
	virtual void Accept(INodeVisitor* visitor);
};

class IsLessThanOrEqualNode : public BinaryRelationalExpressionNode 
{
public://protected:
public:
	IsLessThanOrEqualNode(RelationalExpressionNode* l, RelationalExpressionNode* r);
	virtual void Accept(INodeVisitor* visitor);
};

class IsGreaterThanOrEqualNode : public BinaryRelationalExpressionNode 
{
public://protected:
public:
	IsGreaterThanOrEqualNode(RelationalExpressionNode* l, RelationalExpressionNode* r);
	virtual void Accept(INodeVisitor* visitor);
};



class ConstructorCallNode : public ExpressionNode
{
public://protected:
	TypeNode* type;
public:
	ConstructorCallNode(TypeNode* t);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class ArrayConstructorCallNode : public ConstructorCallNode
{
public://protected:
	ExpressionNode* count;
public:
	ArrayConstructorCallNode(TypeNode* t, ExpressionNode* expression);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};



class StatementNode : public TreeNode
{
public://protected:
public:
	StatementNode(const char* nodeName)
		: TreeNode(nodeName)
	{
	}
	virtual void Accept(INodeVisitor* visitor);
};

class StatementNodes : public TreeNode, public Nodes
{
public://protected:
public:
	StatementNodes();
	virtual void BeginPrintNode(Printer* printer);
	virtual void PrintChildren(Printer* printer);
	virtual void EndPrintNode(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class StatementBlockNode : public StatementNode
{
public://protected:
	StatementNodes* block;
public:
	StatementBlockNode(StatementNodes* statements);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};



class AssignmentStatementNode : public StatementNode
{
public://protected:
	MemberAccessNode* member;
	ExpressionNode* value;
public:
	AssignmentStatementNode(MemberAccessNode* m, ExpressionNode* expression);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class PrintStatementNode : public StatementNode
{
public://protected:
	ArgumentNodes* arguments;
public:
	PrintStatementNode(ArgumentNodes* args);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class IfStatementNode : public StatementNode
{
public://protected:
	ExpressionNode* booleanExpression;
	StatementNode* trueCase;
	StatementNode* falseCase;
public:
	IfStatementNode(ExpressionNode* exp, StatementNode* tcase, StatementNode* fcase);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class WhileStatementNode : public StatementNode
{
public://protected:
	ExpressionNode* value;
	StatementNode* loop;
public:
	WhileStatementNode(ExpressionNode* exp, StatementNode* statement);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class ReturnStatementNode : public StatementNode
{
public://protected:
	ExpressionNode* returnValue;
public:
	ReturnStatementNode(Token* token);
	ReturnStatementNode(ExpressionNode* exp);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class MethodCallStatementNode : public StatementNode, public MethodCall
{
public://protected:
public:
	MethodCallStatementNode(MemberAccessNode* method, ArgumentNodes* args);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class NoOpStatementNode : public StatementNode
{
public://protected:
public:
	NoOpStatementNode(Token* token);
	virtual void Accept(INodeVisitor* visitor);
};



class VariableNode : public TreeNode, public VariableInfo
{
public://protected:
	//NameWithTypeNode* variable;
	TypeNode* type;
	IdentifierToken* name;
public:
	VariableNode(NameWithTypeNode* var);
	virtual void BeginPrintNode(Printer* printer);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class VariableNodes : public TreeNode, public Nodes
{
public://protected:
public:
	VariableNodes();
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};



class MethodBodyNode : public TreeNode
{
public://protected:
	VariableNodes* variables;
	StatementNodes* statements;
public:
	MethodBodyNode(VariableNodes* vars, StatementNodes* stms);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};



class ParameterNode : public TreeNode, public VariableInfo
{
public://protected:
	//NameWithTypeNode* parameter;
	TypeNode* type;
	IdentifierToken* name;
public:
	ParameterNode(NameWithTypeNode* name);
	virtual void BeginPrintNode(Printer* printer);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class ParameterNodes : public TreeNode, public Nodes
{
public://protected:
public:
	ParameterNodes();
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class FieldNode : public TreeNode, public VariableInfo
{
public://protected:
	//NameWithTypeNode* field;
	TypeNode* type;
	IdentifierToken* name;
public:
	FieldNode(NameWithTypeNode* name);
	virtual void BeginPrintNode(Printer* printer);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class FieldNodes : public TreeNode, public Nodes
{
public://protected:
public:
	FieldNodes();
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class MethodNode : public TreeNode, public MethodInfo
{
public://protected:
	//NameWithReturnTypeNode* name;
	TypeNode* type;
	IdentifierToken* name;
	MethodBodyNode* body;
	ParameterNodes* parameters;
public:
	MethodNode(NameWithReturnTypeNode* n, ParameterNodes* params, MethodBodyNode* b);
	virtual void BeginPrintNode(Printer* printer);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class MethodNodes : public TreeNode, public Nodes
{
public://protected:
public:
	MethodNodes();
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};



class ClassBodyNode : public TreeNode
{
public://protected:
	FieldNodes* fields;
	MethodNodes* methods;
public:
	ClassBodyNode();
	ClassBodyNode(FieldNodes* fs);
	ClassBodyNode(MethodNodes* ms);
	ClassBodyNode(FieldNodes* fs, MethodNodes* ms);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};



class ClassNode : public TreeNode, public TypeInfo
{
public://protected:
	IdentifierToken* name;
	ClassBodyNode* body;
public:
	ClassNode(IdentifierToken* n, ClassBodyNode* b);
	virtual void BeginPrintNode(Printer* printer);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class ClassNodes : public TreeNode, public Nodes
{
public://protected:
public:
	ClassNodes();
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

class ProgramNode : public TreeNode, public ProgramInfo
{
public://protected:
	ClassNodes* classes;
public:
	ProgramNode(ClassNodes* cs);
	virtual void PrintChildren(Printer* printer);
	virtual void Accept(INodeVisitor* visitor);
};

#endif // _TREE_NODE_H
