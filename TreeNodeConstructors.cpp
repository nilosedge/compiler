#include "TreeNode.h"


// vector instanciation
std::vector<TreeNode*> declaration;

// definiations of the all collections
std::vector<TreeNode*> TreeNode::allNodes;
std::vector<Token*> Token::allTokens;



TreeNode::TreeNode(const char* name)
    : nodeName(name), ReturnType(0)
{
	allNodes.push_back(this);
}
TreeNode::~TreeNode()
{
}


Nodes::Nodes()
{
}
Nodes::~Nodes()
{
}
	
MethodCall::MethodCall(MemberAccessNode* member, ArgumentNodes* arguments)
	: member(member), arguments(arguments)
{
}
MethodCall::~MethodCall()
{
}

void Token::CleanUp()
{
	std::vector<Token*>::iterator it;
	for (it = allTokens.begin(); it != allTokens.end(); it++)
		delete *it;
}

void TreeNode::CleanUp()
{
	std::vector<TreeNode*>::iterator it;
	for (it = allNodes.begin(); it != allNodes.end(); it++)
		delete *it;
}

TypeNode::TypeNode(IdentifierToken* Name) : TreeNode("Type"), typeName(Name), typeID(New)
{
	LoadTokenInfo(Name);
}
TypeNode::TypeNode(InternalType Type, Token* info) : TreeNode("Type"), typeName(0), typeID(Type)
{
	LoadTokenInfo(info);
}
ArrayTypeNode::ArrayTypeNode(IdentifierToken* n) : TypeNode(Array, n)
{
	innerType = new TypeNode(n);
	nodeName = "ArrayType";
	LoadTokenInfo(n);
}
ArrayTypeNode::ArrayTypeNode(InternalType type, Token* i) : TypeNode(Array, i) 
{
	innerType = new TypeNode(type, i);
	nodeName= "ArrayType";
	LoadTokenInfo(i);
}
NameWithTypeNode::NameWithTypeNode(TypeNode* t, IdentifierToken* n) : TreeNode("NameWithType"), type(t), name(n)
{
	LoadTokenInfo(t);
}
NameWithReturnTypeNode::NameWithReturnTypeNode(TypeNode* t, IdentifierToken* n) : TreeNode("NameWithReturnType"), type(t), name(n) 
{
	LoadTokenInfo(n);
	if (!t)
	{
		type = new TypeNode(TypeNode::Void, n);
	}
}
ForeignMemberAccessNode::ForeignMemberAccessNode(IdentifierToken* n) : TreeNode("ForeignMemberAccess"), name(n)
{
	LoadTokenInfo(n);
}
// check from right here..................................*************
MemberAccessNode::MemberAccessNode(Token* n) : TreeNode("MemberAccess"), name(0), innerAccess(0)
{
	LoadTokenInfo(n);
}

MemberAccessNode::MemberAccessNode(IdentifierToken* n) : TreeNode("MemberAccess"), name(n), innerAccess(0)
{
	LoadTokenInfo(n);
}

ThisAccessNode::ThisAccessNode(Token* token) : MemberAccessNode(token)
{
	nodeName = "This";
}

ForeignMemberArrayAccessNode::ForeignMemberArrayAccessNode(IdentifierToken* n, ExpressionNode* i) : ForeignMemberAccessNode(n), index(i)
{
	nodeName = "ForeignMemberArrayAccess";
	LoadTokenInfo(n);
}
////////////////////////////////////////////
MemberArrayAccessNode::MemberArrayAccessNode(IdentifierToken* n, ExpressionNode* i) : MemberAccessNode(n), index(i)
{
	nodeName="MemberArrayAccess";
	LoadTokenInfo(n);
}
ArgumentNode::ArgumentNode(ExpressionNode* expression) : TreeNode("Argument"), value(expression)
{
	LoadTokenInfo(expression);
}
ArgumentNodes::ArgumentNodes() : TreeNode("Arguments"), Nodes()
{
}
// Have to implement this one yet
//////////////////////////////////
UnaryMinusNode::UnaryMinusNode(FactorNode* factor) : UnaryOperationNode("UnaryMinus", factor)
{
	LoadTokenInfo(factor);
}
LogicalNotNode::LogicalNotNode(FactorNode* factor) : UnaryOperationNode("LogicalNot", factor)
{
	LoadTokenInfo(factor);
}
ParenExpressionNode::ParenExpressionNode(ExpressionNode* expression) : FactorNode("ParenExpression"), value(expression) 
{
	LoadTokenInfo(expression);
}
ReadNode::ReadNode(Token* token) : FactorNode("Read")
{
	LoadTokenInfo(token);
}
MethodCallNode::MethodCallNode(MemberAccessNode* method, ArgumentNodes* args)
	: FactorNode("MethodCall"), MethodCall(method, args) 
{
	LoadTokenInfo(method);
}
NullNode::NullNode(Token* token) : FactorNode("Null")
{
	LoadTokenInfo(token);
}
VariableAccessFactorNode::VariableAccessFactorNode(MemberAccessNode* member) : FactorNode("VariableAccess"), variable(member)
{
	LoadTokenInfo(member);
}
NumberNode::NumberNode(NumberToken* number) : FactorNode("Number"), value(number)
{
	LoadTokenInfo(number);
}
BinaryTermNode::BinaryTermNode(TermNode* l, TermNode* r) : TermNode("BinaryTerm"), left(l), right(r)
{
	LoadTokenInfo(l);
}
MultiplyNode::MultiplyNode(TermNode* l, TermNode* r) : BinaryTermNode(l, r)
{
	nodeName = "Multiply";
}
DivideNode::DivideNode(TermNode* l, TermNode* r) : BinaryTermNode(l,r)
{
	nodeName = "Divide";
}
ModulusNode::ModulusNode(TermNode* l, TermNode* r) : BinaryTermNode(l,r)
{
	nodeName = "Modulus";
}
LogicalAndNode::LogicalAndNode(TermNode* l, TermNode* r) : BinaryTermNode(l,r)
{
	nodeName = "LogicalAnd";
}
BinarySubExpressionNode::BinarySubExpressionNode(SubExpressionNode* l, SubExpressionNode* r) : SubExpressionNode("BinarySub"), left(l), right(r) 
{
	LoadTokenInfo(l); 
}
AdditionNode::AdditionNode(SubExpressionNode* l, SubExpressionNode* r) : BinarySubExpressionNode(l,r)
{
	nodeName = "Addition";
}
SubtractionNode::SubtractionNode(SubExpressionNode* l, SubExpressionNode* r) :BinarySubExpressionNode(l,r)
{
	nodeName = "Subtraction";
}
LogicalOrNode::LogicalOrNode(SubExpressionNode* l, SubExpressionNode* r) : BinarySubExpressionNode(l,r)
{
	nodeName = "LogicalOr";
}
BinaryRelationalExpressionNode::BinaryRelationalExpressionNode(RelationalExpressionNode* i, RelationalExpressionNode* r) : RelationalExpressionNode("BinaryRelational"), left(i), right(r)
{
	LoadTokenInfo(i);
}
IsEqualNode::IsEqualNode(RelationalExpressionNode* l, RelationalExpressionNode* r) : BinaryRelationalExpressionNode(l,r)
{
	nodeName = "IsEqual";
}
IsNotEqualNode::IsNotEqualNode(RelationalExpressionNode* l, RelationalExpressionNode* r) : BinaryRelationalExpressionNode(l,r)
{
	nodeName = "IsNotEqual";
}
IsLessThanNode::IsLessThanNode(RelationalExpressionNode* l, RelationalExpressionNode* r) : BinaryRelationalExpressionNode(l,r)
{
	nodeName = "IsLessThanNode";
}
IsGreaterThanNode::IsGreaterThanNode(RelationalExpressionNode* l, RelationalExpressionNode* r) : BinaryRelationalExpressionNode(l,r)
{
	nodeName = "IsGreaterThanNode";
}
IsLessThanOrEqualNode::IsLessThanOrEqualNode(RelationalExpressionNode* l, RelationalExpressionNode* r) : BinaryRelationalExpressionNode(l,r)
{
	nodeName = "IsLessThanOrEqual";
}
IsGreaterThanOrEqualNode::IsGreaterThanOrEqualNode(RelationalExpressionNode* l, RelationalExpressionNode* r) : BinaryRelationalExpressionNode(l,r)
{
	nodeName = "IsGreaterThanOrEqualNode";
}
ConstructorCallNode::ConstructorCallNode(TypeNode* t) : ExpressionNode("ConstructorCall"), type(t)
{
	LoadTokenInfo(t);
}

ArrayConstructorCallNode::ArrayConstructorCallNode(TypeNode* t, ExpressionNode* expression) : ConstructorCallNode(t), count(expression)
{
	nodeName = "ArrayConstructorCall";
///////////////// ?
}
StatementNodes::StatementNodes() :TreeNode("Statements") // ?
{
	nodeName = "Statements";
}
StatementBlockNode::StatementBlockNode(StatementNodes* statements) : StatementNode("StatementBlock"), block(statements)
{
	LoadTokenInfo(statements);
}
AssignmentStatementNode::AssignmentStatementNode(MemberAccessNode* m, ExpressionNode* expression) : StatementNode("AssignmentStatement"), member(m), value(expression)
{
	LoadTokenInfo(expression);
}
PrintStatementNode::PrintStatementNode(ArgumentNodes* args) : StatementNode("Print"), arguments(args)  //?
{
	LoadTokenInfo(args);
}
IfStatementNode::IfStatementNode(ExpressionNode* exp, StatementNode* tcase, StatementNode* fcase) : StatementNode("IfStatement"), booleanExpression(exp), trueCase(tcase), falseCase(fcase)
{
	LoadTokenInfo(exp);
}
WhileStatementNode::WhileStatementNode(ExpressionNode* exp, StatementNode* statement) : StatementNode("WhileStatement"), value(exp), loop(statement)
{
	LoadTokenInfo(exp);
}
ReturnStatementNode::ReturnStatementNode(Token* token) : StatementNode("ReturnStatement"), returnValue(0)
{
	LoadTokenInfo(token);
}
ReturnStatementNode::ReturnStatementNode(ExpressionNode* exp) : StatementNode("ReturnStatement"), returnValue(exp)
{
	LoadTokenInfo(exp);
}
MethodCallStatementNode::MethodCallStatementNode(MemberAccessNode* method, ArgumentNodes* args)
	: StatementNode("MethodCall"), MethodCall(method, args)
{
	LoadTokenInfo(method);
}
NoOpStatementNode::NoOpStatementNode(Token* token) : StatementNode("NoOp")
{
	LoadTokenInfo(token);
}
VariableNode::VariableNode(NameWithTypeNode* var) : TreeNode("Variable"), 
		type(var->type), name(var->name)
{
	LoadTokenInfo(var);
}
VariableNodes::VariableNodes() : TreeNode("Variables") // ???????????????????????????
{
		
}
MethodBodyNode::MethodBodyNode(VariableNodes* vars, StatementNodes* stms)
	: TreeNode("MethodBody"),  variables(vars), statements(stms)
{
	LoadTokenInfo(stms);
}
ParameterNode::ParameterNode(NameWithTypeNode* name) : TreeNode("Parameter"),
		type(name->type), name(name->name)
{
	LoadTokenInfo(name);
}
ParameterNodes::ParameterNodes() : TreeNode("Parameters") // ????????????????????????
{
}
FieldNode::FieldNode(NameWithTypeNode* name) : TreeNode("Field"), 
		type(name->type), name(name->name)
{
	LoadTokenInfo(name);
}
FieldNodes::FieldNodes() : TreeNode("Fields")
{
}
MethodNode::MethodNode(NameWithReturnTypeNode* n, ParameterNodes* parameters, MethodBodyNode* body)
	: TreeNode("Method"), type(n->type), name(n->name), body(body), parameters(parameters)
{
	LoadTokenInfo(n);
}
MethodNodes::MethodNodes() : TreeNode("Methods") //?
{
}
ClassBodyNode::ClassBodyNode()
	: TreeNode("ClassBody"), fields(0), methods(0)
{
}
ClassBodyNode::ClassBodyNode(FieldNodes* fields)
	: TreeNode("ClassBody"), fields(fields), methods(0)
{
}
ClassBodyNode::ClassBodyNode(MethodNodes* methods)
	: TreeNode("ClassBody"), fields(0), methods(methods)
{
}
ClassBodyNode::ClassBodyNode(FieldNodes* fields, MethodNodes* methods)
	: TreeNode("ClassBody"), fields(fields), methods(methods)
{
}
ClassNode::ClassNode(IdentifierToken* n, ClassBodyNode* b)
	: TreeNode("Class"), name(n), body(b)
{
	LoadTokenInfo(n);
}
ClassNodes::ClassNodes() : TreeNode("Classes") // ?
{
}

ProgramNode::ProgramNode(ClassNodes* cs) : TreeNode("Program"), classes(cs)
{
	LoadTokenInfo(cs);
}



