
#ifndef _PARSE_TREE_BUILDER
#define _PARSE_TREE_BUILDER

#include "TreeNode.h"
#include "IParseTreeBuilder.h"

///
// The ParseTreeBuilder class has but one perupose, that is to
// 	implement the IParseTreeBuilder interface
class ParseTreeBuilder : public IParseTreeBuilder
{
public:
    virtual ProgramNode* CreateProgramNode(ClassNodes* cs);

    virtual ClassNodes* CreateClassNodes(ClassNode* classnode);
    virtual ClassNodes* AppendClassNode(ClassNodes* nodes, ClassNode* classnode);

    virtual ClassNode* CreateClassNode(IdentifierToken* name, ClassBodyNode* body);

    virtual ClassBodyNode* CreateClassBodyNode();
    virtual ClassBodyNode* CreateClassBodyNode(MethodNodes* methods);
    virtual ClassBodyNode* CreateClassBodyNode(FieldNodes* fields);
    virtual ClassBodyNode* CreateClassBodyNode(FieldNodes* fields, MethodNodes* methods);

    virtual FieldNodes* CreateFieldNodes(FieldNode* field);
    virtual FieldNodes* AppendFieldNode(FieldNodes* fields, FieldNode* field);
    
    virtual FieldNode* CreateFieldNode(NameWithTypeNode* name);
    
    virtual MethodNodes* CreateMethodNodes(MethodNode* method);
    virtual MethodNodes* AppendMethodNode(MethodNodes* methods, MethodNode* method);
    virtual MethodNode* CreateMethodNode(NameWithReturnTypeNode* name, ParameterNodes* parameters, MethodBodyNode* body);

    virtual NameWithTypeNode* CreateNameWithTypeNode(TypeNode* type, IdentifierToken* name);

    virtual NameWithReturnTypeNode* CreateNameWithReturnTypeNode(TypeNode* type, IdentifierToken* name);
    
    virtual ArrayTypeNode* CreateIntArrayTypeNode(Token* type);
    virtual ArrayTypeNode* CreateArrayTypeNode(IdentifierToken* name);

    virtual TypeNode* CreateIntTypeNode(Token* type);
    virtual TypeNode* CreateTypeNode(IdentifierToken* name);

    virtual ParameterNodes* CreateParameterNodes();
    virtual ParameterNodes* CreateParameterNodes(ParameterNode* parameter);
    virtual ParameterNodes* AppendParameterNode(ParameterNodes* parameters, ParameterNode* param);

    virtual ParameterNode* CreateParameterNode(NameWithTypeNode* name);

    virtual MethodBodyNode* CreateMethodBodyNode();
    virtual MethodBodyNode* CreateMethodBodyNode(StatementNodes* statement);
    virtual MethodBodyNode* CreateMethodBodyNode(VariableNodes* variables);
    virtual MethodBodyNode* CreateMethodBodyNode(VariableNodes* variables, StatementNodes* statements);

    virtual VariableNodes* CreateVariableNodes(VariableNode* variable);
    virtual VariableNodes* AppendVariableNode(VariableNodes* variables, VariableNode* variable);

    virtual VariableNode* CreateVariableNode(NameWithTypeNode* name);

    virtual StatementNodes* CreateStatementNodes(StatementNode* statement);
    virtual StatementNodes* AppendStatementNode(StatementNodes* statements, StatementNode* statement);

    virtual StatementBlockNode* CreateStatementBlockNode(StatementNodes* statements);
    
    virtual StatementNode* CreateNoOpStatementNode(Token* token);
    virtual StatementNode* CreateAssignmentStatementNode(MemberAccessNode* memberaccess, ExpressionNode* expression);
    virtual StatementNode* CreateMethodCallStatementNode(MemberAccessNode* memberaccess, ArgumentNodes* args);
    virtual StatementNode* CreatePrintStatementNode(ArgumentNodes* arguments);
    virtual StatementNode* CreateWhileStatementNode(ExpressionNode* expression, StatementNode* statement);
    virtual StatementNode* CreateReturnStatementNode(Token* token);
    virtual StatementNode* CreateReturnStatementNode(ExpressionNode* expression);

    virtual MemberAccessNode* CreateMemberAccessNode(MemberAccessNode* memberaccess, ForeignMemberAccessNode* foreignmemberaccess);

    virtual MemberAccessNode* CreateThisAccessNode(Token* token);
    virtual MemberAccessNode* CreateMemberAccessNode(IdentifierToken* token);
    virtual MemberAccessNode* CreateMemberArrayAccessNode(IdentifierToken* name, ExpressionNode* index);

    virtual ForeignMemberAccessNode* CreateForeignMemberAccessNode(IdentifierToken* token);
    virtual ForeignMemberAccessNode* CreateForeignMemberArrayAccessNode(IdentifierToken* token, ExpressionNode* expression);

    virtual ArgumentNodes* CreateArgumentNodes();

    virtual ArgumentNodes* CreateArgumentNodes(ExpressionNode* expression);
    virtual ArgumentNodes* AppendArgumentNode(ArgumentNodes* arguments, ExpressionNode* expression);
    
    virtual IfStatementNode* CreateIfStatementNode(ExpressionNode* expression, StatementNode* statement, StatementNode* elsestatement);

    virtual ExpressionNode* CreateExpressionNode(RelationalExpressionNode* relationalexpression);
    virtual ExpressionNode* CreateConstructorCallNode(IdentifierToken* token);
    virtual ExpressionNode* CreateArrayConstructorCallNode(TypeNode* type, ExpressionNode* expression);

    virtual RelationalExpressionNode* CreateRelationalExpressionNode(SubExpressionNode* subexpression);
    virtual RelationalExpressionNode* CreateIsEqualNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2);
    virtual RelationalExpressionNode* CreateIsNotEqualNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2);
    virtual RelationalExpressionNode* CreateIsLessThanNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2);
    virtual RelationalExpressionNode* CreateIsGreaterThanNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2);
    virtual RelationalExpressionNode* CreateIsLessThanOrEqualNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2);
    virtual RelationalExpressionNode* CreateIsGreaterThanOrEqualNode(RelationalExpressionNode* relationalexpression1, RelationalExpressionNode* relationalexpression2);
 
    virtual SubExpressionNode* CreateSubExpressionNode(TermNode* term);
    virtual SubExpressionNode* CreateAdditionNode(SubExpressionNode* subexpression1, SubExpressionNode* subexpression2);
    virtual SubExpressionNode* CreateSubtractionNode(SubExpressionNode* subexpression1, SubExpressionNode* subexpression2);
    virtual SubExpressionNode* CreateLogicalOrNode(SubExpressionNode* subexpression1, SubExpressionNode* subexpression2);

    virtual TermNode* CreateTermNode(FactorNode* factor);
    virtual TermNode* CreateMultiplyNode(TermNode* term1, TermNode* term2);
    virtual TermNode* CreateDivideNode(TermNode* term1, TermNode* term2);
    virtual TermNode* CreateModulusNode(TermNode* term1, TermNode* term2);
    virtual TermNode* CreateLogicalAndNode(TermNode* term1, TermNode* term2);

    virtual FactorNode* CreateVariableAccessFactorNode(MemberAccessNode* memberaccess);
    virtual FactorNode* CreateNumberNode(NumberToken* token);
    virtual FactorNode* CreateNullNode(Token* token);
    virtual FactorNode* CreateMethodCallNode(MemberAccessNode* member, ArgumentNodes* arguments);
    virtual FactorNode* CreateReadNode(Token* token);

    virtual FactorNode* CreateParenExpressionNode(ExpressionNode* expression);
    virtual UnaryOperationNode* CreateUnaryMinusNode(FactorNode* factor);
    virtual UnaryOperationNode* CreateLogicalNotNode(FactorNode* factor);

};


#endif // _PARSE_TREE_BUILDER 

