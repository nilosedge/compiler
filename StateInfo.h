
#ifndef ___STATE_INFO_H
#define ___STATE_INFO_H


#include "HashTable.h"


// Predeclare the Info classes
class ProgramInfo;
class TypeInfo;
class VariableInfo;
class MethodInfo;


// This Tables and lists are used in the Info classes
typedef Hashing::HashTable<TypeInfo*> TypeTable;
typedef Hashing::HashTable<VariableInfo*> VariableTable;
typedef Hashing::HashTable<MethodInfo*> MethodTable;
typedef std::vector<VariableInfo*> VariableVector;


// The Program info class contains all the symbolic information
class ProgramInfo
{
public:
	ProgramInfo();
    TypeTable Classes;
	// The MainClass points to the TypeInfo object that contains
	// the main method
    TypeInfo* MainClass;
};

// Each type in the system is described by a TypeInfo Object
class TypeInfo
{
public:
	TypeInfo();
    VariableTable Fields;
    MethodTable Methods;
    const char* Name;
	int Size;
	bool IsReferenceType;
	TypeInfo* SecondaryType;
};

// Each Parameter, Argument, and local Variable is described 
// by a VariableInfo object
class VariableInfo
{
public:
	VariableInfo();
    TypeInfo* Type;
    const char* Name;
    int Offset;
};

// Each Method is described by a MethodInfo Object
class MethodInfo
{
public:
	MethodInfo();
	 TypeInfo* ReturnType;
    VariableTable Variables;
    VariableTable Parameters;
    VariableVector ParameterList;
    const char* Name;
    int numParams;
	// UniqueName is used in codegeneration for jump labels
    char UniqueName[128];
};


#endif // ___STATE_INFO_H




