#!/usr/bin/perl -w
# A test suite for the Decaf compiler.
#

# The location of the decaf compiler
my $DECAF = "decaf";

# The location of the test suite config file
my $CONFIG = "testsuite.conf";

# Directory to write logs to.
my $LOG_DIR = "logs";

# Check for a correct compiler
if(!(-e $DECAF)) {
	die "Error: couldn't find $DECAF\nDo you need to 'make all'?";
} elsif(!(-f $DECAF)) {
	die "Error: $DECAF is corrupted";
} elsif(!(-x $DECAF)) {
	die "Error: $DECAF is not executable";
}

# Make sure we can write logs.
if(!(-e $LOG_DIR)) {
	print "Error: couldn't find $LOG_DIR\nCreating...\n\n";
	exec ("mkdir logs");
} elsif(!(-d $LOG_DIR)) {
	die "Error: $LOG_DIR is not a directory";
} elsif(!(-w $LOG_DIR)) {
	die "Error: Can't write to $LOG_DIR";
}

open(CONFIG, "<", $CONFIG)
	or die "Error: couldn't find ./$CONFIG.\n";


my %files;
my $tests = 0;
my $failed = 0;
my $pwd = $ENV{'PWD'};
$pwd = $pwd."/";

# Read in the config file
while(<CONFIG>) {
	chomp;
	@input = split(/:/);
	$files{$input[0]} = $input[1];
}

# Walk through the tests
foreach $file (keys %files) {
	if(-r $file) {
		&test($file, $files{$file});
		$tests++;
	} else {
		die "Error: $file doesn't exist or isn't readable";
	}
}

if($failed > 0) {
	print "\e[31m\e[1mTest Suite: $failed out of $tests tests failed\e[0m\n\n";
} else  {
	print "\e[34m\e[1mTest Suite: $failed out of $tests tests failed\e[0m\n\n";
}

sub test {
	my $file_string = shift;
	my $expected_errors = shift;
	my @file_path = split(/\//, $file_string);
	my $file_name = $file_path[$#file_path];
	my @args = ($pwd.$DECAF, " $pwd$file_string", " 2>$LOG_DIR/$file_name.err", "1>/dev/null");
	print "\e[34m\e[1mTesting: $file_name...\e[0m\n";
	system "@args";
	my $actual_errors = $? >> 8;
	if($actual_errors != $expected_errors) {
		print "\e[31m\e[1mError: $file_name failed test\e[0m\n";
		print "\e[31m\e[1m\texpected $expected_errors errors found $actual_errors\e[0m\n";
		print "\e[31m\e[1m\tsee $LOG_DIR/$file_name.err for details.\e[0m\n\n";
		$failed++;
	} else {
		print "\e[34m\e[1m$file_name passed.\e[0m\n\n";
	}
}
